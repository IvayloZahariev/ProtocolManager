/**
 * 
 */
package com.izahariev.projects.pm.services.interfaces;

import com.izahariev.projects.pm.beans.ProtocolBean;

/**
 * Interface declaring the methods used by the DisplaySingleProtocolServlet.
 * 
 * @author Ivaylo Zahariev
 *
 */
public interface DisaplySingleProtocolServicesInterface {

    /**
     * Saves the given protocol bean data to the database.
     * 
     * @param protocolBean
     *            The protocol bean which data will be used.
     */
    void addProtocol(ProtocolBean protocolBean);

    /**
     * Updates the old protocol with the given new values.
     * 
     * @param oldBean
     *            Bean having the old values of the protocol fields.
     * @param newBean
     *            bean having the new values of the protocol fields.
     */
    void updateProtocol(ProtocolBean oldBean, ProtocolBean newBean);

    /**
     * Removes the given protocol from the system.
     * 
     * @param bean
     *            The protocol bean which data will be removed from the
     *            database.
     */
    void deleteProtocol(ProtocolBean bean);
}
