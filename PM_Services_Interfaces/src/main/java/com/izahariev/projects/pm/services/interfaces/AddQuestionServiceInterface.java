/**
 * 
 */
package com.izahariev.projects.pm.services.interfaces;

import java.util.List;

/**
 * Interface declaring the methods used to add questions to protocol.
 * 
 * @author Ivaylo Zahariev
 *
 */
public interface AddQuestionServiceInterface {

    /**
     * Gets the name of question types in the system.
     * 
     * @return A List of the names of the question types.
     */
    List<String> getQuestionTypes();
}
