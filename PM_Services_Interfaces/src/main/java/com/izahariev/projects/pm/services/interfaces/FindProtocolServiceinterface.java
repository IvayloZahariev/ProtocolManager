/**
 * 
 */
package com.izahariev.projects.pm.services.interfaces;

import java.util.List;

import com.izahariev.projects.pm.beans.ProtocolBean;

/**
 * Interface declaring the methods used to find a protocol.
 * 
 * @author Ivaylo Zahariev
 *
 */
public interface FindProtocolServiceinterface {

    /**
     * Gets the names of all faculties in the system.
     * 
     * @return List containing the names of all faculties.
     */
    List<String> getFaculties();

    /**
     * Gets the names of all departments in the system.
     * 
     * @return List containing the names of all departments.
     */
    List<String> getDepartments();

    /**
     * Gets the names of all people on the teaching stuff who are not members of
     * the academic council.
     * 
     * @return List containing the names of all people who are not part of the
     *         academic council.
     */
    List<String> getNonAcademicCouncilMembers();

    /**
     * Gets all protocols with the given parameters.
     * 
     * @param protocolType
     *            The type of the protocol.
     * @param protocolNumber
     *            The number of the protocol.
     * @param councilDate
     *            The date of the council meeting. Must be in format yyyy-mm-dd.
     * @param faculty
     *            The protocol faculty.
     * @param department
     *            The protocol department.
     * @param reporter
     *            The protocol reporter.
     * @param questionSpecification
     *            Parts of the specification of one or more questions in the
     *            required protocols.
     * @param questionDecision
     *            Parts of the decision of one or more questions in the required
     *            protocols.
     * @return List of protocol beans with the protocols meeting the given
     *         requirements.
     */
    List<ProtocolBean> getProtocols(String protocolType, String protocolNumber, String councilDate, String faculty,
            String department, String reporter, String questionSpecification, String questionDecision);
}
