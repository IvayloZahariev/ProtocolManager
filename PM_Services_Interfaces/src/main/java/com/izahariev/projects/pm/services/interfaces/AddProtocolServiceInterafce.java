/**
 * 
 */
package com.izahariev.projects.pm.services.interfaces;

import java.util.List;

/**
 * Interface declaring the methods used to add a new protocol to the Protocol
 * Manager.
 * 
 * @author Ivaylo Zahariev
 *
 */
public interface AddProtocolServiceInterafce {

    /**
     * Method used to check if there is a council member with the given first
     * name and last name.
     * 
     * @param firstName
     *            The first name of the council member.
     * @param lastName
     *            The last name of the council member.
     * @return True if a council member with the given names exist, otherwise
     *         false.
     */
    boolean checkCouncilMember(String firstName, String lastName);

    /**
     * Method used to check if the given department is part of the given
     * faculty.
     * 
     * @param faculty
     *            The name of the faculty.
     * @param department
     *            The name of the department.
     * @return True if the department is part of the faculty, otherwise false.
     */
    boolean checkFaculty(String faculty, String department);

    /**
     * Gets the name of every member of the academic council.
     * 
     * @return A List of the names of the academic council members.
     */
    List<String> getAcademicCouncilMembers();

    /**
     * Gets the name of every department which are part of the selected faculty.
     * 
     * @return A List of the names of the departments.
     */
    List<String> getDepartments(String facultyName);

    /**
     * Gets the name of every faculty in the system.
     * 
     * @return A List of the names of the faculties.
     */
    List<String> getFaculties();

    /**
     * Gets the names of all people on the teaching stuff who are not members of
     * the academic council.
     * 
     * @return List containing the names of all people who are not part of the
     *         academic council.
     */
    List<String> getNonAcademicCouncilMembers();

    /**
     * Gets the name of the director of the given department.
     * 
     * @param departmentName
     *            The name of the department which director is needed.
     * @return The name of the director.
     */
    String getDepartmentDirectorName(String departmentName);
}
