/**
 * 
 */
package com.izahariev.projects.pm.services.interfaces;

/**
 * Interface defining the methods provided by the login services.
 * 
 * @author Ivaylo Zahariev
 *
 */
public interface LoginServiceInterface {

    /**
     * Checks if the user has entered correct username and password.
     * 
     * @param username
     *            The given username.
     * @param password
     *            The given password.
     * @return True if the username and password are correct, otherwise false.
     */
    boolean checkUser(String username, String password);
}
