/**
 * 
 */
package com.izahariev.projects.pm.factories.services;

import com.izahariev.projects.pm.services.AddProtocolServices;
import com.izahariev.projects.pm.services.AddQuestionServices;
import com.izahariev.projects.pm.services.DisplaySingleProtocolServices;
import com.izahariev.projects.pm.services.FindProtocolServices;
import com.izahariev.projects.pm.services.LoginServices;

/**
 * Class used to provide implementation for the different kinds of services.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings("PMD.UseUtilityClass")
public class ServicesFactory {

    /**
     * Method providing implementation for the LoginServices.
     * 
     * @return The created LoginServices object.
     */
    public static LoginServices getLoginServices() {
        return new LoginServices();
    }

    /**
     * Method providing implementation for the AddProtocolServices.
     * 
     * @return The created AddProtocolServices object.
     */
    public static AddProtocolServices getAddProtocolServices() {
        return new AddProtocolServices();
    }

    /**
     * Method providing implementation for the AddQuestionServices.
     * 
     * @return The created AddQuestionServices object.
     */
    public static AddQuestionServices getAddQuestionServices() {
        return new AddQuestionServices();
    }

    /**
     * Method providing implementation for the FindProtocolServices.
     * 
     * @return The created FindProtocolServices object.
     */
    public static FindProtocolServices getFindProtocolServices() {
        return new FindProtocolServices();
    }

    /**
     * Method providing implementation for the DisplaySingleProtocolServices.
     * 
     * @return The created DisplaySingleProtocolServices object.
     */
    public static DisplaySingleProtocolServices getDisplaySingleProtocolServices() {
        return new DisplaySingleProtocolServices();
    }
}
