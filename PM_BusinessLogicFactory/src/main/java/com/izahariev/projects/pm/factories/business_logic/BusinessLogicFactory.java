/**
 * 
 */
package com.izahariev.projects.pm.factories.business_logic;

import com.izahariev.projects.pm.business_logic.requests.DepartmentRequests;
import com.izahariev.projects.pm.business_logic.requests.FacultyRequests;
import com.izahariev.projects.pm.business_logic.requests.ProtocolRequests;
import com.izahariev.projects.pm.business_logic.requests.QuestionTypeRequests;
import com.izahariev.projects.pm.business_logic.requests.TeachingStuffRequests;
import com.izahariev.projects.pm.business_logic.requests.UserRequests;

/**
 * Class used to provide implementations for the Business Logic interfaces.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings("PMD.UseUtilityClass")
public class BusinessLogicFactory {

    /**
     * Creates a new instance of the {@link UserRequests} class.
     * 
     * @return The created instance of the {@link UserRequests} class.
     */
    public static UserRequests getUserRequests() {
        return new UserRequests();
    }

    /**
     * Creates a new instance of the {@link TeachingStuffRequests} class.
     * 
     * @return The created instance of the {@link TeachingStuffRequests} class.
     */
    public static TeachingStuffRequests getTeachingStuffRequests() {
        return new TeachingStuffRequests();
    }

    /**
     * Creates a new instance of the {@link DepartmentRequests} class.
     * 
     * @return The created instance of the {@link DepartmentRequests} class.
     */
    public static DepartmentRequests getDepartmentRequests() {
        return new DepartmentRequests();
    }

    /**
     * Creates a new instance of the {@link FacultyRequests} class.
     * 
     * @return The created instance of the {@link FacultyRequests} class.
     */
    public static FacultyRequests getFacultyRequests() {
        return new FacultyRequests();
    }

    /**
     * Creates a new instance of the {@link QuestionTypeRequests} class.
     * 
     * @return The created instance of the {@link QuestionTypeRequests} class.
     */
    public static QuestionTypeRequests getQuestionTypeRequests() {
        return new QuestionTypeRequests();
    }

    /**
     * Creates a new instance of the {@link ProtocolRequests} class.
     * 
     * @return The created instance of the {@link ProtocolRequests} class.
     */
    public static ProtocolRequests getProtocolRequests() {
        return new ProtocolRequests();
    }
}
