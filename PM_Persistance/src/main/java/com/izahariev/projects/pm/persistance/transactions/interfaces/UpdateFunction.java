/**
 * 
 */
package com.izahariev.projects.pm.persistance.transactions.interfaces;

/**
 * Interface used for update functions.
 * 
 * @author Ivaylo Zahariev
 *
 */
@FunctionalInterface
public interface UpdateFunction {

    /**
     * Method used to execute a given update lambda expression.
     */
    void execute();
}
