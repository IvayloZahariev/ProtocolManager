/**
 * 
 */
package com.izahariev.projects.pm.persistance.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity class representing the QUESTIONTYPE table in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
@Entity
@Table(name = "QUESTIONTYPE")
public class QuestionType {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QUESTIONTYPE_ID")
    @SequenceGenerator(name = "QUESTIONTYPE_ID", sequenceName = "QUESTIONTYPE_ID", allocationSize = 1)
    @Column(name = "id")
    @SuppressWarnings("PMD.ShortVariable")
    private int id;

    @Column(name = "name")
    private String name;

    /**
     * Default constructor required by Hibernate.
     */
    public QuestionType() {

    }

    /**
     * Constructor initializing the name field.
     * 
     * @param name
     *            The question type name.
     */
    public QuestionType(final String name) {
        this.name = name;
    }

    /**
     * Gets the id of the question type.
     * 
     * @return the id The id of the question type.
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the name of the question type.
     * 
     * @return the name The name of the question type.
     */
    public String getName() {
        return name;
    }

}
