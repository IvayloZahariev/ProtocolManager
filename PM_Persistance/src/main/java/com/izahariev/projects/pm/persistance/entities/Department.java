/**
 * 
 */
package com.izahariev.projects.pm.persistance.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity class representing the DEPARTMENT table in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
@Entity
@Table(name = "DEPARTMENT")
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DEPARTMENT_ID")
    @SequenceGenerator(name = "DEPARTMENT_ID", sequenceName = "DEPARTMENT_ID", allocationSize = 1)
    @Column(name = "id")
    @SuppressWarnings("PMD.ShortVariable")
    private int id;

    @Column(name = "NAME")
    private String name;

    @OneToOne
    @JoinColumn(name = "FK_DEPARTMENT_TEACHINGSTUFF")
    private TeachingStuff director;

    @ManyToOne
    @JoinColumn(name = "FK_DEPARTMENT_FACULTY")
    private Faculty faculty;

    /**
     * Default constructor required by Hibernate.
     */
    public Department() {

    }

    /**
     * Constructor initializing the class fields.
     * 
     * @param name
     *            The name of the department.
     * @param faculty
     *            The faculty , which this department is part of.
     */
    public Department(final String name, final Faculty faculty) {
        this.name = name;
        this.faculty = faculty;
    }

    /**
     * Gets the name of the department.
     * 
     * @return The name of the department.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the department.
     * 
     * @param name
     *            The new name of the department.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the director of the department.
     * 
     * @return The director of the department.
     */
    public TeachingStuff getDirector() {
        return director;
    }

    /**
     * Sets the director of the department.
     * 
     * @param director
     *            The new director of the department.
     */
    public void setDirector(final TeachingStuff director) {
        this.director = director;
    }

    /**
     * Gets the faculty of the department.
     * 
     * @return The faculty of the department.
     */
    public Faculty getFaculty() {
        return faculty;
    }

    /**
     * Sets the faculty of the department.
     * 
     * @param faculty
     *            The new faculty of the department.
     */
    public void setFaculty(final Faculty faculty) {
        this.faculty = faculty;
    }

    /**
     * Gets the id of the department.
     * 
     * @return The id of the department.
     */
    public int getId() {
        return id;
    }
}
