/**
 * 
 */
package com.izahariev.projects.pm.persistance.transactions.interfaces;

import java.util.List;

/**
 * Interface used for insert functions.
 * 
 * @author Ivaylo Zahariev
 *
 */
@FunctionalInterface
public interface InsertFunction {

    /**
     * Method used to execute a given insert lambda expression.
     * 
     * @return The created entity objects.
     */
    List<Object> execute();
}
