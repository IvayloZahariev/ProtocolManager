/**
 * 
 */
package com.izahariev.projects.pm.persistance.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * Entity class representing the QUESTION table in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
@Entity
@Table(name = "QUESTION")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QUESTION_ID")
    @SequenceGenerator(name = "QUESTION_ID", sequenceName = "QUESTION_ID", allocationSize = 1)
    @Column(name = "id")
    @SuppressWarnings("PMD.ShortVariable")
    private int id;

    @Column(name = "QUESTION_NUMBER")
    private float number;

    @ManyToOne
    @JoinColumn(name = "FK_QUESTION_QUESTIONTYPE")
    private QuestionType type;

    @Column(name = "SPECIFICATION")
    private String specification;

    @ManyToOne
    @JoinColumn(name = "FK_QUESTION_PROTOCOL")
    private Protocol protocol;

    @Column(name = "DECISION")
    private String decision;

    @OneToMany(mappedBy = "question")
    @NotFound(action = NotFoundAction.IGNORE)
    private List<Vote> votes;

    @OneToMany(mappedBy = "question")
    @NotFound(action = NotFoundAction.IGNORE)
    private List<QuestionComment> comments;

    /**
     * Default constructor required by Hibernate.
     */
    public Question() {

    }

    /**
     * Constructor initializing the class variables.
     * 
     * @param number
     *            The number of the question in the protocol.
     * @param type
     *            The type of the question.
     * @param specification
     *            The specification of the question.
     * @param protocol
     *            The protocol of which this question is part of.
     * @param decision
     *            The approved decision for the question specification.
     */
    public Question(final float number, final QuestionType type, final String specification, final Protocol protocol,
            final String decision) {
        this.number = number;
        this.type = type;
        this.specification = specification;
        this.protocol = protocol;
        this.decision = decision;
    }

    /**
     * Gets the comments for this question.
     * 
     * @return The comments for this question.
     */
    public List<QuestionComment> getComments() {
        return comments;
    }

    /**
     * Sets the comments for this question.
     * 
     * @param comments
     *            The comments for this question.
     */
    public void setComments(final List<QuestionComment> comments) {
        this.comments = comments;
    }

    /**
     * Gets the number of the question in the protocol.
     * 
     * @return The number of the question in the protocol.
     */
    public float getNumber() {
        return number;
    }

    /**
     * Sets a new value for the number of the question in the protocol.
     * 
     * @param number
     *            The new number of the question to be set.
     */
    public void setNumber(final float number) {
        this.number = number;
    }

    /**
     * Gets the type of the question.
     * 
     * @return The type of the question.
     */
    public QuestionType getType() {
        return type;
    }

    /**
     * Sets a new value for the type of the question.
     * 
     * @param type
     *            The new value for the type of the question.
     */
    public void setType(final QuestionType type) {
        this.type = type;
    }

    /**
     * gets the specification of the question.
     * 
     * @return The specification of the question.
     */
    public String getSpecification() {
        return specification;
    }

    /**
     * Sets a new value for the specification of the question.
     * 
     * @param specification
     *            The new value for the specification of the question.
     */
    public void setSpecification(final String specification) {
        this.specification = specification;
    }

    /**
     * Gets the protocol this question is part of.
     * 
     * @return The protocol this question is part of.
     */
    public Protocol getProtocol() {
        return protocol;
    }

    /**
     * Sets a new value for the protocol of the question.
     * 
     * @param protocol
     *            The new value for the protocol of the question.
     */
    public void setProtocol(final Protocol protocol) {
        this.protocol = protocol;
    }

    /**
     * Gets the approved decision of the question specification.
     * 
     * @return The approved decision of the question specification.
     */
    public String getDecision() {
        return decision;
    }

    /**
     * Sets a new value for the decision of the question specification.
     * 
     * @param decision
     *            The new value for the decision of the question specification.
     */
    public void setDecision(final String decision) {
        this.decision = decision;
    }

    /**
     * Gets the id of the question.
     * 
     * @return The id of the question.
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the votes for this question.
     * 
     * @return The votes for this question.
     */
    public List<Vote> getVotes() {
        return votes;
    }

    /**
     * Sets the votes for this question.
     * 
     * @param votes
     *            The new votes for this question.
     */
    public void setVotes(final List<Vote> votes) {
        this.votes = votes;
    }

}
