/**
 * 
 */
package com.izahariev.projects.pm.persistance.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity class representing the VOTETYPE table in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
@Entity
@Table(name = "VOTETYPE")
public class VoteType {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VOTETYPE_ID")
    @SequenceGenerator(name = "VOTETYPE_ID", sequenceName = "VOTETYPE_ID", allocationSize = 1)
    @Column(name = "id")
    @SuppressWarnings("PMD.ShortVariable")
    private int id;

    @Column(name = "NAME")
    private String name;

    /**
     * Default constructor required by Hibernate.
     */
    public VoteType() {

    }

    /**
     * Constructor initializing the vote type name.
     * 
     * @param name
     *            The name of the vote type.
     */
    public VoteType(final String name) {
        this.name = name;
    }

    /**
     * Gets the name of the vote type.
     * 
     * @return The name of the vote type.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the vote type.
     * 
     * @param name
     *            The new name of the vote type.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the id of the vote type.
     * 
     * @return The id of the vote type.
     */
    public int getId() {
        return id;
    }

}
