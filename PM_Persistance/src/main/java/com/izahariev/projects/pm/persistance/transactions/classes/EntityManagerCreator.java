/**
 * 
 */
package com.izahariev.projects.pm.persistance.transactions.classes;

import javax.persistence.EntityManager;

/**
 * Class used to create entity manager objects.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings("PMD.UseUtilityClass")
public class EntityManagerCreator {

    /**
     * Creates an entity manager object.
     * 
     * @return The created entity manager.
     */
    public static EntityManager create() {
        return EntityManagerFactoryCreator.getEntityManagerFactory().createEntityManager();
    }
}
