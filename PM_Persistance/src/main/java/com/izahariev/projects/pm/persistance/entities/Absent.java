/**
 * 
 */
package com.izahariev.projects.pm.persistance.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity class representing the ABSENT table in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
@Entity
@Table(name = "ABSENT")
public class Absent {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ABSENT_ID")
    @SequenceGenerator(name = "ABSENT_ID", sequenceName = "ABSENT_ID", allocationSize = 1)
    @Column(name = "id")
    @SuppressWarnings("PMD.ShortVariable")
    private int id;

    @ManyToOne
    @JoinColumn(name = "FK_ABSENT_PROTOCOL")
    private Protocol protocol;

    @ManyToOne
    @JoinColumn(name = "FK_ABSENT_TEACHINGSTUFF")
    private TeachingStuff person;

    @Column(name = "REASON")
    private String reason;

    @Column(name = "PERSON_NAME")
    private String personName;

    /**
     * Default constructor required by Hibernate.
     */
    public Absent() {

    }

    /**
     * Constructor initializing the class fields.
     * 
     * @param protocol
     *            The protocol from which this person is absent.
     * @param person
     *            The person who is absent.
     * @param reason
     *            The reason for the absence.
     */
    public Absent(final Protocol protocol, final TeachingStuff person, final String reason) {
        this.protocol = protocol;
        this.person = person;
        this.reason = reason;
    }

    /**
     * Gets the protocol form which the person is absent.
     * 
     * @return The protocol form which the person is absent.
     */
    public Protocol getProtocol() {
        return protocol;
    }

    /**
     * Sets the protocol form which the person is absent.
     * 
     * @param protocol
     *            The new value for the protocol.
     */
    public void setProtocol(final Protocol protocol) {
        this.protocol = protocol;
    }

    /**
     * Gets the person who is absent.
     * 
     * @return The person who is absent.
     */
    public TeachingStuff getPerson() {
        return person;
    }

    /**
     * Sets the person who is absent.
     * 
     * @param person
     *            The new value for the absent person.
     */
    public void setPerson(final TeachingStuff person) {
        this.person = person;
    }

    /**
     * Gets the reason for the absence.
     * 
     * @return The reason for the absence.
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the reason for the absence.
     * 
     * @param reason
     *            The new reason for the absence.
     */
    public void setReason(final String reason) {
        this.reason = reason;
    }

    /**
     * Gets the id of the absence.
     * 
     * @return The id of the absence.
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the value of the personName property.
     * 
     * @return The value of the personName property.
     */
    public String getPersonName() {
        return personName;
    }

    /**
     * Sets a new value for the personName property.
     * 
     * @param personName
     *            The new value of the personName property.
     */
    public void setPersonName(final String personName) {
        this.personName = personName;
    }
}
