/**
 * 
 */
package com.izahariev.projects.pm.persistance.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity class representing the VOTE table in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
@Entity
@Table(name = "VOTE")
@SuppressWarnings("PMD.ShortClassName")
public class Vote {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VOTE_ID")
    @SequenceGenerator(name = "VOTE_ID", sequenceName = "VOTE_ID", allocationSize = 1)
    @Column(name = "id")
    @SuppressWarnings("PMD.ShortVariable")
    private int id;

    @Column(name = "NUMBER_YES")
    private int numberYes;

    @Column(name = "NUMBER_NO")
    private int numberNo;

    @Column(name = "NUMBER_WHITE")
    private int numberWhite;

    @ManyToOne
    @JoinColumn(name = "MODERATOR")
    private TeachingStuff moderator;

    @ManyToOne
    @JoinColumn(name = "MEMBER1")
    private TeachingStuff member1;

    @ManyToOne
    @JoinColumn(name = "MEMBER2")
    private TeachingStuff member2;

    @ManyToOne
    @JoinColumn(name = "FK_VOTE_QUESTION")
    private Question question;

    @ManyToOne
    @JoinColumn(name = "FK_VOTE_VOTETYPE")
    private VoteType type;

    @Column(name = "VOTE_SUBJECT")
    private String voteSubject;

    /**
     * Default constructor required by Hibernate.
     */
    public Vote() {

    }

    /**
     * Constructor initializing the class fields.
     * 
     * @param numberYes
     *            The number of YES votes.
     * @param numberNo
     *            The number of NO votes.
     * @param numberWhite
     *            The number of WHITE votes.
     * @param moderator
     *            The moderator of the vote commission.
     * @param member1
     *            The first member of the vote commission.
     * @param member2
     *            The second member of the vote commission.
     * @param question
     *            The question, this vote is part of.
     * @param type
     *            The type of the vote.
     */
    public Vote(final int numberYes, final int numberNo, final int numberWhite, final TeachingStuff moderator,
            final TeachingStuff member1, final TeachingStuff member2, final Question question, final VoteType type,
            final String voteSubject) {
        this.numberYes = numberYes;
        this.numberNo = numberNo;
        this.numberWhite = numberWhite;
        this.moderator = moderator;
        this.member1 = member1;
        this.member2 = member2;
        this.question = question;
        this.type = type;
        this.voteSubject = voteSubject;
    }

    /**
     * Gets the number of YES votes.
     * 
     * @return The number of YES votes.
     */
    public int getNumberYes() {
        return numberYes;
    }

    /**
     * Sets the number of YES votes.
     * 
     * @param numberYes
     *            The new number of YES votes.
     */
    public void setNumberYes(final int numberYes) {
        this.numberYes = numberYes;
    }

    /**
     * Gets the number of NO votes.
     * 
     * @return The number of NO votes.
     */
    public int getNumberNo() {
        return numberNo;
    }

    /**
     * Sets the number of NO votes.
     * 
     * @param numberNo
     *            The new number of NO votes.
     */
    public void setNumberNo(final int numberNo) {
        this.numberNo = numberNo;
    }

    /**
     * Gets the number of WHITE votes.
     * 
     * @return The number of WHITE votes.
     */
    public int getNumberWhite() {
        return numberWhite;
    }

    /**
     * Sets the number of WHITE votes.
     * 
     * @param numberWhite
     *            The new number of WHITE votes.
     */
    public void setNumberWhite(final int numberWhite) {
        this.numberWhite = numberWhite;
    }

    /**
     * Gets the person who is this vote's commission moderator.
     * 
     * @return The moderator of the vote commission.
     */
    public TeachingStuff getModerator() {
        return moderator;
    }

    /**
     * Sets the person who is this vote's commission moderator.
     * 
     * @param moderator
     *            The new commission moderator.
     */
    public void setModerator(final TeachingStuff moderator) {
        this.moderator = moderator;
    }

    /**
     * Gets the first member of this vote's commission.
     * 
     * @return The first member of this vote's commission.
     */
    public TeachingStuff getMember1() {
        return member1;
    }

    /**
     * Sets the first member of this vote's commission.
     * 
     * @param member1
     *            The new first member of this vote's commission.
     */
    public void setMember1(final TeachingStuff member1) {
        this.member1 = member1;
    }

    /**
     * Gets the second member of this vote's commission.
     * 
     * @return The second member of this vote's commission.
     */
    public TeachingStuff getMember2() {
        return member2;
    }

    /**
     * Sets the second member of this vote's commission.
     * 
     * @param member2
     *            The new second member of this vote's commission.
     */
    public void setMember2(final TeachingStuff member2) {
        this.member2 = member2;
    }

    /**
     * Gets the question, this vote is part of.
     * 
     * @return The question, this vote is part of.
     */
    public Question getQuestion() {
        return question;
    }

    /**
     * Sets a new value for the question, this vote is part of.
     * 
     * @param question
     *            The new qustion.
     */
    public void setQuestion(final Question question) {
        this.question = question;
    }

    /**
     * Gets the type of the vote.
     * 
     * @return The type of the vote.
     */
    public VoteType getType() {
        return type;
    }

    /**
     * Sets the type of the vote.
     * 
     * @param type
     *            The new type of the vote.
     */
    public void setType(final VoteType type) {
        this.type = type;
    }

    /**
     * Gets the subject of the vote.
     * 
     * @return The subject of the vote.
     */
    public String getVoteSubject() {
        return voteSubject;
    }

    /**
     * Sets the subject of the vote.
     * 
     * @param type
     *            The new subject of the vote.
     */
    public void setVoteSubject(final String voteSubject) {
        this.voteSubject = voteSubject;
    }

    /**
     * Gets the id of the question.
     * 
     * @return The id of the question.
     */
    public int getId() {
        return id;
    }

}
