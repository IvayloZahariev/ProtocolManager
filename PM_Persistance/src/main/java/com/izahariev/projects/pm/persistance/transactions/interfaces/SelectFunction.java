/**
 * 
 */
package com.izahariev.projects.pm.persistance.transactions.interfaces;

import java.util.List;

/**
 * Interface used for select functions.
 * 
 * @author Ivaylo Zahariev
 *
 */
@FunctionalInterface
public interface SelectFunction<T> {

    /**
     * Method used to execute a given select lambda expression.
     * 
     * @return A list containing the result of the select.
     * 
     */
    List<T> execute();
}
