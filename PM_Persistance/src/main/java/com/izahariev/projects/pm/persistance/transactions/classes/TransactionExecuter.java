/**
 * 
 */
package com.izahariev.projects.pm.persistance.transactions.classes;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.izahariev.projects.pm.persistance.transactions.interfaces.InsertFunction;
import com.izahariev.projects.pm.persistance.transactions.interfaces.SelectFunction;
import com.izahariev.projects.pm.persistance.transactions.interfaces.UpdateFunction;

/**
 * Class used to execute transactions.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings("PMD.UseUtilityClass")
public class TransactionExecuter {

    /**
     * Executes the given insert function. If an error occurs during the
     * insertion, a roll back is executed.
     * 
     * @param entityManager
     *            The entity manager used in the transactions.
     * @param entity
     *            The entity object which data will be inserted in the database.
     * @throws IllegalArgumentException
     *             Thrown when the given object is not an entity.
     */
    public static void insert(final EntityManager entityManager, final InsertFunction function)
            throws IllegalArgumentException {

        final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            boolean errorOccured = true;
            try {
                for (final Object entity : function.execute()) {
                    entityManager.persist(entity);
                }
                transaction.commit();
                errorOccured = false;
            }
            finally {
                if (errorOccured) {
                    transaction.rollback();
                }
            }
        }
        finally {
            entityManager.close();
        }
    }

    /**
     * Executes the given select function.
     * 
     * @param entityManager
     *            The entity manager used in the function.
     * @param function
     *            The function which will be executed.
     * @return A list containing the result from the select function.
     */
    public static <T> List<T> select(final EntityManager entityManager, final SelectFunction<T> function) {

        try {
            return function.execute();
        }
        finally {
            entityManager.close();
        }
    }

    /**
     * Executes the given select function. DOES NOT CLOSES ENTITY MANAGER! Used
     * ONLY to get an entity from the database and use the same entity manager
     * to update it. For a normal select operation use
     * {@link TransactionExecuter#select(EntityManager, SelectFunction)} method.
     * 
     * @param entityManager
     *            The entity manager used in the function.
     * @param function
     *            The function which will be executed.
     * @return A list containing the result from the select function.
     */
    public static <T> List<T> selectForUpdate(final EntityManager entityManager, final SelectFunction<T> function) {
        return function.execute();
    }

    /**
     * Executes the given update function.
     * 
     * @param entityManager
     *            The entity manager used in the function.
     * @param function
     *            The function which will be executed.
     */
    public static void update(final EntityManager entityManager, final UpdateFunction function) {

        final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            boolean errorOccured = true;
            try {
                function.execute();
                transaction.commit();
                errorOccured = false;
            }
            finally {
                if (errorOccured) {
                    transaction.rollback();
                }
            }
        }
        finally {
            entityManager.close();
        }
    }

    /**
     * Deletes the given entity from the database.
     * 
     * @param entityManager
     *            The entity manager used for the transaction.
     * @param entity
     *            The entity object which will be removed from the database.
     * @throws IllegalArgumentException
     *             Thrown when the given object is not an entity.
     */
    public static void delete(final EntityManager entityManager, final Object entity) throws IllegalArgumentException {
        final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            boolean errorOccured = true;
            try {
                entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
                transaction.commit();
                errorOccured = false;
            }
            finally {
                if (errorOccured) {
                    transaction.rollback();
                }
            }
        }
        finally {
            entityManager.close();
        }
    }

    /**
     * Insets the given entity in the database.
     * 
     * @param entityManager
     *            The entity manager used for the transaction.
     * @param entity
     *            The entity object which will be inserted in the database.
     */
    public static void persistEntity(final EntityManager entityManager, final Object entity) {
        final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            boolean errorOccured = true;
            try {
                entityManager.persist(entity);
                transaction.commit();
                errorOccured = false;
            }
            finally {
                if (errorOccured) {
                    transaction.rollback();
                }
            }
        }
        finally {
            entityManager.close();
        }
    }

    /**
     * Used to insert entities which contain other entities as fields, which are
     * detached.
     * 
     * @param entityManager
     *            The entity manager used for the transaction.
     * @param entity
     *            The entity object which will be inserted in the database.
     */
    public static void mergeEntity(final EntityManager entityManager, final Object entity) {
        final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            boolean errorOccured = true;
            try {
                entityManager.merge(entity);
                transaction.commit();
                errorOccured = false;
            }
            finally {
                if (errorOccured) {
                    transaction.rollback();
                }
            }
        }
        finally {
            entityManager.close();
        }
    }
}
