/**
 * 
 */
package com.izahariev.projects.pm.persistance.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity class representing the QUESTIONCOMMENT table in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
@Entity
@Table(name = "QUESTIONCOMMENT")
public class QuestionComment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QUESTIONCOMMENT_ID")
    @SequenceGenerator(name = "QUESTIONCOMMENT_ID", sequenceName = "QUESTIONCOMMENT_ID", allocationSize = 1)
    @Column(name = "ID")
    @SuppressWarnings("PMD.ShortVariable")
    private int id;

    @ManyToOne
    @JoinColumn(name = "FK_COMMENT_QUESTION")
    private Question question;

    @OneToOne
    @JoinColumn(name = "FK_COMMENT_TEACHINGSTUFF")
    private TeachingStuff commentOwner;

    @Column(name = "SPECIFICATION")
    private String specification;

    @Column(name = "OTHER_COMMENT_OWNER")
    private String otherCommentOwner;

    /**
     * Default constructor required by Hibernate.
     */
    public QuestionComment() {

    }

    /**
     * Constructor initializing the class fields.
     * 
     * @param question
     *            The question, this comment is addressed to.
     * @param commentOwner
     *            The owner of the comment.
     * @param specification
     *            The specification of the comment.
     */
    public QuestionComment(final Question question, final TeachingStuff commentOwner, final String specification) {
        super();
        this.question = question;
        this.commentOwner = commentOwner;
        this.specification = specification;
    }

    /**
     * Gets the question, this comment is addressed to.
     * 
     * @return The question, this comment is addressed to.
     */
    public Question getQuestion() {
        return question;
    }

    /**
     * Sets a new value for the question of the comment.
     * 
     * @param question
     *            The new value for the question of the comment.
     */
    public void setQuestion(final Question question) {
        this.question = question;
    }

    /**
     * Gets the person who gave the comment.
     * 
     * @return The person who gave the comment.
     */
    public TeachingStuff getCommentOwner() {
        return commentOwner;
    }

    /**
     * Sets the person who gave the comment.
     * 
     * @param commentOwner
     *            The new owner of the comment.
     */
    public void setCommentOwner(final TeachingStuff commentOwner) {
        this.commentOwner = commentOwner;
    }

    /**
     * Gets the specification of the comment.
     * 
     * @return the specification
     */
    public String getSpecification() {
        return specification;
    }

    /**
     * Sets a new value for the specification of the comment.
     * 
     * @param specification
     *            The new value for the specification of the comment.
     */
    public void setSpecification(final String specification) {
        this.specification = specification;
    }

    /**
     * Gets the id of the comment.
     * 
     * @return The id of the comment.
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the value of the otherCommentOwner property.
     * 
     * @return The value of the otherCommentOwner property.
     */
    public String getOtherCommentOwner() {
        return otherCommentOwner;
    }

    /**
     * Sets a new value for the otherCommentOwner property.
     * 
     * @param otherCommentOwner
     *            The new value of the otherCommentOwner property.
     */
    public void setOtherCommentOwner(final String otherCommentOwner) {
        this.otherCommentOwner = otherCommentOwner;
    }
}
