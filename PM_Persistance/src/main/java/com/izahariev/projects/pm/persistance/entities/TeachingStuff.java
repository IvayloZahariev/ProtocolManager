/**
 * 
 */
package com.izahariev.projects.pm.persistance.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity class representing the TEACHINGSTUFF table in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
@Entity
@Table(name = "TEACHINGSTUFF")
public class TeachingStuff {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEACHINGSTUFF_ID")
    @SequenceGenerator(name = "TEACHINGSTUFF_ID", sequenceName = "TEACHINGSTUFF_ID", allocationSize = 1)
    @Column(name = "id")
    @SuppressWarnings("PMD.ShortVariable")
    private int id;

    @Column(name = "NAME")
    private String name;

    @ManyToOne
    @JoinColumn(name = "FK_TEACHINGSTUFF_DEPARTMENT")
    private Department department;

    @Column(name = "ACADEMIC_COUNCIL_MEMBER")
    private int isAcademicCouncilMember;

    /**
     * Default constructor required by Hibernate.
     */
    public TeachingStuff() {

    }

    /**
     * Constructor initializing the class fields.
     * 
     * @param name
     *            The name of the person.
     * @param department
     *            The department of the person.
     */
    public TeachingStuff(final String name, final Department department) {
        this.name = name;
        this.department = department;
    }

    /**
     * Gets the name of the person.
     * 
     * @return The name of the person.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets a new value for the person's name.
     * 
     * @param name
     *            The new person name.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the department of the person.
     * 
     * @return The department of the person.
     */
    public Department getDepartment() {
        return department;
    }

    /**
     * Sets a new value for the department of the person.
     * 
     * @param department
     *            The new value for the department of the person.
     */
    public void setDepartment(final Department department) {
        this.department = department;
    }

    /**
     * Gets the value of the isAcademicCouncilMember property.
     * 
     * @return The value of the isAcademicCouncilMember property.
     */
    public int getIsAcademicCouncilMember() {
        return isAcademicCouncilMember;
    }

    /**
     * Sets a new value for the isAcademicCouncilMember property.
     * 
     * @param isAcademicCouncilMember
     *            The new value of the isAcademicCouncilMember property.
     */
    public void setIsAcademicCouncilMember(final int isAcademicCouncilMember) {
        this.isAcademicCouncilMember = isAcademicCouncilMember;
    }

    /**
     * Gets the id of the person.
     * 
     * @return The id of the person.
     */
    public int getId() {
        return id;
    }

}
