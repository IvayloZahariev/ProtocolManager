/**
 * 
 */
package com.izahariev.projects.pm.persistance.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity class representing the FACULTY table in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
@Entity
@Table(name = "FACULTY")
public class Faculty {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FACULTY_ID")
    @SequenceGenerator(name = "FACULTY_ID", sequenceName = "FACULTY_ID", allocationSize = 1)
    @Column(name = "id")
    @SuppressWarnings("PMD.ShortVariable")
    private int id;

    @Column(name = "NAME")
    private String name;

    /**
     * Default constructor required by Hibernate.
     */
    public Faculty() {

    }

    /**
     * Constructor initializing the class fields.
     * 
     * @param name
     *            The name of the faculty.
     */
    public Faculty(final String name) {
        this.name = name;
    }

    /**
     * Gets the name of the faculty.
     * 
     * @return the name The name of the faculty.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the faculty.
     * 
     * @param name
     *            The new name to be set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the id of the faculty.
     * 
     * @return the id The id of the faculty.
     */
    public int getId() {
        return id;
    }

}
