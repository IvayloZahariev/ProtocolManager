/**
 * 
 */
package com.izahariev.projects.pm.persistance.entities;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * Entity class representing the PROTOCOL table in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
@Entity
@Table(name = "PROTOCOL")
public class Protocol {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROTOCOL_ID")
    @SequenceGenerator(name = "PROTOCOL_ID", sequenceName = "PROTOCOL_ID", allocationSize = 1)
    @Column(name = "id")
    @SuppressWarnings("PMD.ShortVariable")
    private int id;

    @OneToOne
    @JoinColumn(name = "FK_PROTOCOL_DEPARTMENT")
    private Department department;

    @Column(name = "PROTOCOL_NUMBER")
    private int protocolNumber;

    @Column(name = "PROTOCOL_DATE")
    private Date date;

    @OneToOne
    @JoinColumn(name = "FK_PROTOCOL_TEACHINGSTUFF")
    private TeachingStuff reporter;

    @ManyToOne
    @JoinColumn(name = "FK_PROTOCOL_PROTOCOLTYPE")
    private ProtocolType type;

    @OneToMany(mappedBy = "protocol")
    @NotFound(action = NotFoundAction.IGNORE)
    private List<ProtocolAgenda> protocolAgenda;

    @OneToMany(mappedBy = "protocol")
    @NotFound(action = NotFoundAction.IGNORE)
    private List<Question> questions;

    @Column(name = "ALL_MEMBERS")
    private int allMembers;

    @Column(name = "REDUCED_MEMBERS")
    private int reducedMembers;

    @Column(name = "QUORUM")
    private int quorum;

    @Column(name = "PRESENT_MEMBERS")
    private int presentMembers;

    /**
     * Default constructor required by Hibernate.
     */
    public Protocol() {

    }

    /**
     * Constructor initializing the class fields.
     * 
     * @param department
     *            The department of the protocol.
     * @param protocolNumber
     *            The number of the protocol.
     * @param date
     *            The date of the protocol.
     * @param reporter
     *            The reporter of the protocol.
     * @param type
     *            The type of the protocol.
     */
    public Protocol(final Department department, final int protocolNumber, final java.util.Date date,
            final TeachingStuff reporter, final ProtocolType type) {

        this.department = department;
        this.protocolNumber = protocolNumber;
        this.date = new Date(date.getTime());
        this.reporter = reporter;
        this.type = type;
    }

    /**
     * Gets the department of the protocol.
     * 
     * @return The department of the protocol.
     */
    public Department getDepartment() {
        return department;
    }

    /**
     * Sets the department of the protocol.
     * 
     * @param department
     *            The new value of the department of the protocol.
     */
    public void setDepartment(final Department department) {
        this.department = department;
    }

    /**
     * Gets the protocol number.
     * 
     * @return The protocol number.
     */
    public int getProtocolNumber() {
        return protocolNumber;
    }

    /**
     * Sets the protocol number.
     * 
     * @param protocolNumber
     *            The new value of the protocol number.
     */
    public void setProtocolNumber(final int protocolNumber) {
        this.protocolNumber = protocolNumber;
    }

    /**
     * Gets the date of the protocol.
     * 
     * @return The date of the protocol.
     */
    public java.util.Date getDate() {
        return date;
    }

    /**
     * Sets the date of the protocol.
     * 
     * @param date
     *            The new the date of the protocol.
     */
    public void setDate(final java.util.Date date) {
        this.date = new Date(date.getTime());
    }

    /**
     * Gets the reporter of the protocol.
     * 
     * @return The reporter of the protocol.
     */
    public TeachingStuff getReporter() {
        return reporter;
    }

    /**
     * Sets the reporter of the protocol.
     * 
     * @param reporter
     *            The new reporter of the protocol.
     */
    public void setReporter(final TeachingStuff reporter) {
        this.reporter = reporter;
    }

    /**
     * Gets the type of the protocol.
     * 
     * @return The type of the protocol.
     */
    public ProtocolType getType() {
        return type;
    }

    /**
     * Sets the type of the protocol.
     * 
     * @param type
     *            The new type of the protocol.
     */
    public void setType(final ProtocolType type) {
        this.type = type;
    }

    /**
     * Gets the protocol agenda.
     * 
     * @return The protocol agenda.
     */
    public List<ProtocolAgenda> getProtocolAgenda() {
        return protocolAgenda;
    }

    /**
     * Sets the protocol agenda.
     * 
     * @param protocolAgenda
     *            The new protocol agenda.
     */
    public void setProtocolAgenda(final List<ProtocolAgenda> protocolAgenda) {
        this.protocolAgenda = protocolAgenda;
    }

    /**
     * Gets the questions of the protocol.
     * 
     * @return The questions of the protocol.
     */
    public List<Question> getQuestions() {
        return questions;
    }

    /**
     * Sets the questions of the protocol.
     * 
     * @param questions
     *            The new questions of the protocol.
     */
    public void setQuestions(final List<Question> questions) {
        this.questions = questions;
    }

    /**
     * Gets the reduced number of members of the protocol.
     * 
     * @return The reduced number of members of the protocol.
     */
    public int getReducedMembers() {
        return reducedMembers;
    }

    /**
     * Sets the reduced number of members of the protocol.
     * 
     * @param reducedMembers
     *            The new reduced number of members of the protocol.
     */
    public void setReducedMembers(final int reducedMembers) {
        this.reducedMembers = reducedMembers;
    }

    /**
     * Gets the quorum of the protocol.
     * 
     * @return The quorum of the protocol.
     */
    public int getQuorum() {
        return quorum;
    }

    /**
     * Sets the quorum of the protocol.
     * 
     * @param quorum
     *            The new quorum of the protocol.
     */
    public void setQuorum(final int quorum) {
        this.quorum = quorum;
    }

    /**
     * Gets the number of present members of the protocol.
     * 
     * @return The number of present members of the protocol.
     */
    public int getPresentMembers() {
        return presentMembers;
    }

    /**
     * Sets the number of present members of the protocol.
     * 
     * @param presentMembers
     *            The new number of present members of the protocol.
     */
    public void setPresentMembers(final int presentMembers) {
        this.presentMembers = presentMembers;
    }

    /**
     * Gets the id of the protocol.
     * 
     * @return The id of the protocol.
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the value of the allMembers property.
     * 
     * @return The value of the allMembers property.
     */
    public int getAllMembers() {
        return allMembers;
    }

    /**
     * Sets a new value for the allMembers property.
     * 
     * @param allMembers
     *            The new value of the allMembers property.
     */
    public void setAllMembers(final int allMembers) {
        this.allMembers = allMembers;
    }
}
