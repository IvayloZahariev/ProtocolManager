/**
 * 
 */
package com.izahariev.projects.pm.persistance.transactions.classes;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Singleton class used to ensure that the {@link EntityManagerFactory} will be
 * created only once.
 * 
 * @author Ivaylo Zahariev
 *
 */
public final class EntityManagerFactoryCreator {

    private EntityManagerFactoryCreator() {

    }

    /**
     * Method returning a singleton instance of {@link EntityManagerFactory}. If
     * the instance is not yet created, the method creates a new instance and
     * returns it. If an instance is already created, the method returns that
     * instance.
     * 
     * @return An instance of {@link EntityManagerFactory}.
     */
    public static EntityManagerFactory getEntityManagerFactory() {
        return Holder.INSTANCE;
    }

    /**
     * Class used to make the getEntityManagerFactory method thread safe.
     * 
     * @author Ivaylo Zahariev
     *
     */
    private static class Holder {
        @SuppressWarnings("nls")
        public static final EntityManagerFactory INSTANCE = Persistence.createEntityManagerFactory("ProtocolManagerPU");
    }
}
