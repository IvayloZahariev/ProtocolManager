/**
 * 
 */
package com.izahariev.projects.pm.persistance.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity class representing the PROTOCOLTYPE table in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
@Entity
@Table(name = "PROTOCOLTYPE")
public class ProtocolType {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROTOCOLTYPE_ID")
    @SequenceGenerator(name = "PROTOCOLTYPE_ID", sequenceName = "PROTOCOLTYPE_ID", allocationSize = 1)
    @Column(name = "id")
    @SuppressWarnings("PMD.ShortVariable")
    private int id;

    @Column(name = "NAME")
    private String name;

    /**
     * Default constructor required by Hibernate.
     */
    public ProtocolType() {

    }

    /**
     * Construct initializing the name field.
     * 
     * @param name
     */
    public ProtocolType(final String name) {
        this.name = name;
    }

    /**
     * Gets the name of the protocol type.
     * 
     * @return The name of the protocol type.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets a new name for the protocol type.
     * 
     * @param name
     *            The new name for the protocol type.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the id of the protocol type.
     * 
     * @return The id of the protocol type.
     */
    public int getId() {
        return id;
    }

}
