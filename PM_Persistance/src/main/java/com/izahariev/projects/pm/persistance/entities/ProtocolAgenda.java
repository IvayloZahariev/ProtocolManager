/**
 * 
 */
package com.izahariev.projects.pm.persistance.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity class representing the PROTOCOLAGENDA table in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
@Entity
@Table(name = "PROTOCOLAGENDA")
public class ProtocolAgenda {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROTOCOLAGENDA_ID")
    @SequenceGenerator(name = "PROTOCOLAGENDA_ID", sequenceName = "PROTOCOLAGENDA_ID", allocationSize = 1)
    @Column(name = "id")
    @SuppressWarnings("PMD.ShortVariable")
    private int id;

    @ManyToOne
    @JoinColumn(name = "FK_PROTOCOLAGENDA_PROTOCOL")
    private Protocol protocol;

    @OneToOne
    @JoinColumn(name = "FK_PROTOCOLAGENDA_QUESTIONTYPE")
    private QuestionType questionType;

    /**
     * Default constructor required by Hibernate.
     */
    public ProtocolAgenda() {

    }

    /**
     * Constructor initializing the class fields.
     * 
     * @param protocol
     *            The protocol, this agenda is part of.
     * @param questionType
     *            The question type.
     */
    public ProtocolAgenda(final Protocol protocol, final QuestionType questionType) {
        this.protocol = protocol;
        this.questionType = questionType;
    }

    /**
     * Gets the protocol, this agenda is part of.
     * 
     * @return The protocol, this agenda is part of.
     */
    public Protocol getProtocol() {
        return protocol;
    }

    /**
     * Sets the protocol, this agenda is part of.
     * 
     * @param protocol
     *            The new value of the protocol, this agenda is part of.
     */
    public void setProtocol(final Protocol protocol) {
        this.protocol = protocol;
    }

    /**
     * Gets the question type.
     * 
     * @return The question type.
     */
    public QuestionType getQuestionType() {
        return questionType;
    }

    /**
     * Sets the question type.
     * 
     * @param questionType
     *            The new question type.
     */
    public void setQuestionType(final QuestionType questionType) {
        this.questionType = questionType;
    }

    /**
     * Gets the id of the question type.
     * 
     * @return The id of the question type.
     */
    public int getId() {
        return id;
    }
}
