/**
 * 
 */
package com.izahariev.projects.pm.persistance.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity class representing the USERS table in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
@Entity
@Table(name = "USERS")
@SuppressWarnings("PMD.ShortClassName")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USERS_ID")
    @SequenceGenerator(name = "USERS_ID", sequenceName = "USERS_ID", allocationSize = 1)
    @Column(name = "id")
    @SuppressWarnings("PMD.ShortVariable")
    private int id;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "ADD_FILES")
    private int addFiles;

    @Column(name = "REMOVE_FILES")
    private int removeFiles;

    @Column(name = "EDIT_FILES")
    private int editFiles;

    /**
     * Default constructor required by Hibernate.
     */
    public User() {

    }

    /**
     * Constructor initializing the class properties.
     * 
     * @param username
     *            The username of the user.
     * @param password
     *            The password of the user.
     * @param addFiles
     *            Flag showing if the user can add files. 1 is true, 0 is false.
     * @param removeFiles
     *            Flag showing if the user can remove files. 1 is true, 0 is
     *            false.
     * @param editFiles
     *            Flag showing if the user can edit files. 1 is true, 0 is
     *            false.
     */
    public User(final String username, final String password, final int addFiles, final int removeFiles,
            final int editFiles) {
        this.username = username;
        this.password = password;
        this.addFiles = addFiles;
        this.removeFiles = removeFiles;
        this.editFiles = editFiles;
    }

    /**
     * Gets the value of the username property.
     * 
     * @return The value of the username property.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets a new value for the username property.
     * 
     * @param username
     *            The new value of the username property.
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return The value of the password property.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets a new value for the password property.
     * 
     * @param password
     *            The new value of the password property.
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Gets the value of the addFiles property.
     * 
     * @return The value of the addFiles property.
     */
    public int getAddFiles() {
        return addFiles;
    }

    /**
     * Sets a new value for the addFiles property.
     * 
     * @param addFiles
     *            The new value of the addFiles property.
     */
    public void setAddFiles(final int addFiles) {
        this.addFiles = addFiles;
    }

    /**
     * Gets the value of the removeFiles property.
     * 
     * @return The value of the removeFiles property.
     */
    public int getRemoveFiles() {
        return removeFiles;
    }

    /**
     * Sets a new value for the removeFiles property.
     * 
     * @param removeFiles
     *            The new value of the removeFiles property.
     */
    public void setRemoveFiles(final int removeFiles) {
        this.removeFiles = removeFiles;
    }

    /**
     * Gets the value of the editFiles property.
     * 
     * @return The value of the editFiles property.
     */
    public int getEditFiles() {
        return editFiles;
    }

    /**
     * Sets a new value for the editFiles property.
     * 
     * @param editFiles
     *            The new value of the editFiles property.
     */
    public void setEditFiles(final int editFiles) {
        this.editFiles = editFiles;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return The value of the id property.
     */
    public int getId() {
        return id;
    }

}
