/**
 * 
 */
package com.izahariev.projects.pm.persistance.transaction_executer_tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.Test;

import com.izahariev.projects.pm.persistance.entities.Absent;
import com.izahariev.projects.pm.persistance.entities.Department;
import com.izahariev.projects.pm.persistance.entities.Faculty;
import com.izahariev.projects.pm.persistance.entities.QuestionType;
import com.izahariev.projects.pm.persistance.entities.VoteType;
import com.izahariev.projects.pm.persistance.transactions.classes.EntityManagerCreator;
import com.izahariev.projects.pm.persistance.transactions.classes.TransactionExecuter;

/**
 * Class containing unit tests for the {@link TransactionExecuter} select
 * method.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings({ "nls", "static-method" })
public class SelectMethodTests {

    /**
     * Test for the behavior of the select method when selecting all data from a
     * table with one row.
     */
    @Test
    public void selectShouldReturnListWithOneElementWhenGettingDataFromTableWithOneRow() {
        final QuestionType questionType = new QuestionType("Kadrovi");
        TransactionExecuter.insert(EntityManagerCreator.create(), () -> {
            final List<Object> entities = new ArrayList<>();
            entities.add(questionType);
            return entities;
        });

        final EntityManager entityManager = EntityManagerCreator.create();
        final List<QuestionType> questionTypes = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<QuestionType> typedQuery = entityManager.createQuery("SELECT qt FROM QuestionType qt",
                    QuestionType.class);
            return typedQuery.getResultList();
        });

        assertEquals("The number of elemets in the returned list should be 1.", 1, questionTypes.size());
        assertEquals("The name of the question type should be \"Kadrovi\".", "Kadrovi", questionTypes.get(0).getName());

        clearDatabase(questionType);
    }

    /**
     * Test for the behavior of the select method when selecting all data from a
     * table with three rows.
     */
    @Test
    public void selectShouldReturnListWithThreeElementsWhenGettingDataFromTableWithThreeRows() {
        final VoteType vt1 = new VoteType("1");
        final VoteType vt2 = new VoteType("2");
        final VoteType vt3 = new VoteType("3");
        TransactionExecuter.insert(EntityManagerCreator.create(), () -> {
            final List<Object> entities = new ArrayList<>();
            entities.add(vt1);
            entities.add(vt2);
            entities.add(vt3);
            return entities;
        });

        final EntityManager entityManager = EntityManagerCreator.create();
        final List<VoteType> questionTypes = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<VoteType> typedQuery = entityManager.createQuery("SELECT vt FROM VoteType vt",
                    VoteType.class);
            return typedQuery.getResultList();
        });

        assertEquals("The length of the returned list should be 3.", 3, questionTypes.size());

        clearDatabase(vt1, vt2, vt3);
    }

    /**
     * Test for the behavior of the select method when the table contains only
     * one row covering the select requirements.
     */
    @Test
    @SuppressWarnings("PMD.ShortVariable")
    public void selectShouldReturnOneElementWhenGettingDataFromTableContaingOneRowCoveringTheSelectRequirements() {
        final Faculty f1 = new Faculty("F1");
        final Faculty f2 = new Faculty("F2");
        final Faculty f3 = new Faculty("F3");
        final Department d1 = new Department("D1", f1);
        final Department d2 = new Department("D2", f2);
        final Department d3 = new Department("D3", f3);
        TransactionExecuter.insert(EntityManagerCreator.create(), () -> {
            final List<Object> entities = new ArrayList<>();
            entities.add(f1);
            entities.add(f2);
            entities.add(f3);
            entities.add(d1);
            entities.add(d2);
            entities.add(d3);

            return entities;
        });

        final EntityManager entityManager = EntityManagerCreator.create();
        final List<Department> departments = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<Department> typedQuery = entityManager
                    .createQuery("SELECT d FROM Department d WHERE d.name=:name", Department.class);
            typedQuery.setParameter("name", "D2");
            return typedQuery.getResultList();
        });

        assertEquals("The lentght of the returned list should be 1.", 1, departments.size());
        assertEquals("The name of the first element in departments should be D2.", "D2", departments.get(0).getName());

        clearDatabase(d3, d2, d1, f3, f2, f1);
    }

    /**
     * Test for the behavior of the select method when the table contains two
     * row covering the select requirements.
     */
    @Test
    @SuppressWarnings("PMD.ShortVariable")
    public void selectShouldReturnOneElementWhenGettingDataFromTableContaingTwoRowsCoveringTheSelectRequirements() {
        final Faculty f1 = new Faculty("F1");
        final Faculty f2 = new Faculty("F2");
        final Faculty f3 = new Faculty("F3");
        final Department d1 = new Department("D1", f1);
        final Department d2 = new Department("D2", f2);
        final Department d3 = new Department("D3", f3);
        TransactionExecuter.insert(EntityManagerCreator.create(), () -> {
            final List<Object> entities = new ArrayList<>();
            entities.add(f1);
            entities.add(f2);
            entities.add(f3);
            entities.add(d1);
            entities.add(d2);
            entities.add(d3);

            return entities;
        });

        final EntityManager entityManager = EntityManagerCreator.create();
        final List<Department> departments = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<Department> typedQuery = entityManager
                    .createQuery("SELECT d FROM Department d WHERE d.name IN (:name1, :name2)", Department.class);
            typedQuery.setParameter("name1", "D1");
            typedQuery.setParameter("name2", "D2");
            return typedQuery.getResultList();
        });

        assertEquals("The lentght of the returned list should be 2.", 2, departments.size());
        assertEquals("The name of the first element in departments should be D1.", "D1", departments.get(0).getName());
        assertEquals("The name of the second element in departments should be D2.", "D2", departments.get(1).getName());

        clearDatabase(d3, d2, d1, f3, f2, f1);
    }

    /**
     * Test for the behavior of the select method when selecting all data from
     * an empty table.
     */
    @Test
    public void selectShouldReturnEpmtyListWhenGettingDataFromEmptyTable() {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<Absent> absents = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<Absent> typedQuery = entityManager.createQuery("SELECT a FROM Absent a", Absent.class);
            return typedQuery.getResultList();
        });

        assertEquals("The length of the absents list should be 0.", 0, absents.size());
    }

    /**
     * Test for the behavior of the select method when selecting data which is
     * not contained in the used table.
     */
    @SuppressWarnings("PMD.ShortVariable")
    @Test
    public void selectShouldReturnEmptyListWhenTheTableDoesNotContainTheNeededData() {
        final Faculty f1 = new Faculty("Faculty 1");
        final Faculty f2 = new Faculty("Faculty 2");
        TransactionExecuter.insert(EntityManagerCreator.create(), () -> {
            final List<Object> entities = new ArrayList<>();
            entities.add(f1);
            entities.add(f2);
            return entities;
        });

        final EntityManager entityManager = EntityManagerCreator.create();
        final List<Faculty> faculties = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<Faculty> typedQuery = entityManager
                    .createQuery("SELECT f FROM Faculty f WHERE f.name=:name", Faculty.class);
            typedQuery.setParameter("name", "f3");
            return typedQuery.getResultList();
        });

        assertEquals("The length of the faculties list should be 0.", 0, faculties.size());
        clearDatabase(f1, f2);
    }

    private static void clearDatabase(final Object... objects) {
        for (final Object object : objects) {
            TransactionExecuter.delete(EntityManagerCreator.create(), object);
        }
    }
}
