/**
 * 
 */
package com.izahariev.projects.pm.persistance.transaction_executer_tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.izahariev.projects.pm.persistance.entities.Department;
import com.izahariev.projects.pm.persistance.entities.Faculty;
import com.izahariev.projects.pm.persistance.entities.Protocol;
import com.izahariev.projects.pm.persistance.entities.ProtocolType;
import com.izahariev.projects.pm.persistance.entities.TeachingStuff;
import com.izahariev.projects.pm.persistance.transactions.classes.EntityManagerCreator;
import com.izahariev.projects.pm.persistance.transactions.classes.TransactionExecuter;

/**
 * Class containing unit tests for the update method in the
 * {@link TransactionExecuter} class.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings({ "nls", "static-method", "PMD.AvoidDuplicateLiterals" })
public class UpdateMethodTests {

    private static final List<Object> dbData = new ArrayList<>();

    /**
     * Method used to insert data in the database which will be used in the
     * test.
     */
    @BeforeClass
    @SuppressWarnings("PMD.ShortVariable")
    public static void addDataToDB() {
        final Faculty f1 = new Faculty("F 1");
        final Faculty f2 = new Faculty("F 2");
        final Department d1 = new Department("D 1", f1);
        final Department d2 = new Department("D 2", f2);
        final TeachingStuff ts1 = new TeachingStuff("TS 1", d1);
        final TeachingStuff ts2 = new TeachingStuff("TS 2", d2);
        final ProtocolType pt1 = new ProtocolType("PT 1");
        final ProtocolType pt2 = new ProtocolType("PT 2");
        final Protocol p1 = new Protocol(d1, 1, Calendar.getInstance().getTime(), ts1, pt1);
        final Protocol p2 = new Protocol(d2, 2, Calendar.getInstance().getTime(), ts2, pt2);
        dbData.add(f1);
        dbData.add(f2);
        dbData.add(d1);
        dbData.add(d2);
        dbData.add(ts1);
        dbData.add(ts2);
        dbData.add(pt1);
        dbData.add(pt2);
        dbData.add(p1);
        dbData.add(p2);
        TransactionExecuter.insert(EntityManagerCreator.create(), () -> {
            return dbData;
        });
    }

    /**
     * Test method for the behavior of the update method when updating the name
     * of a faculty.
     */
    @Test
    public void updateShouldChangeTheValueOfTheUpdatedFacultyNameInTheDatabase() {
        final EntityManager entityManager1 = EntityManagerCreator.create();
        final List<Faculty> faculties = TransactionExecuter.selectForUpdate(entityManager1, () -> {
            final TypedQuery<Faculty> typedQuery = entityManager1
                    .createQuery("SELECT f FROM Faculty f WHERE name=:name", Faculty.class);
            typedQuery.setParameter("name", "F 1");
            return typedQuery.getResultList();
        });

        TransactionExecuter.update(entityManager1, () -> {
            faculties.get(0).setName("FKSU");
        });

        final EntityManager entityManager2 = EntityManagerCreator.create();
        final List<Faculty> result = TransactionExecuter.select(EntityManagerCreator.create(), () -> {
            final TypedQuery<Faculty> typedQuery = entityManager2
                    .createQuery("SELECT f FROM Faculty f WHERE name=:name", Faculty.class);
            typedQuery.setParameter("name", "FKSU");
            return typedQuery.getResultList();
        });

        assertEquals("The name of the faculty should be FKSU.", "FKSU", result.get(0).getName());
    }

    /**
     * Test method for the behavior of the update method when updating an entity
     * property which value is null.
     */
    @Test
    public void updateShouldWorkWhenUpdateingNullProperty() {
        final EntityManager entityManager1 = EntityManagerCreator.create();
        final List<Department> departments = TransactionExecuter.selectForUpdate(entityManager1, () -> {
            final TypedQuery<Department> typedQuery = entityManager1
                    .createQuery("SELECT d FROM Department d WHERE name=:name", Department.class);
            typedQuery.setParameter("name", "D 1");
            return typedQuery.getResultList();
        });

        TransactionExecuter.update(entityManager1, () -> {
            departments.get(0).setDirector((TeachingStuff) dbData.get(4));
        });

        final EntityManager entityManager2 = EntityManagerCreator.create();
        final List<Department> result = TransactionExecuter.select(EntityManagerCreator.create(), () -> {
            final TypedQuery<Department> typedQuery = entityManager2
                    .createQuery("SELECT d FROM Department d WHERE name=:name", Department.class);
            typedQuery.setParameter("name", "D 1");
            return typedQuery.getResultList();
        });

        assertEquals("Dhe name of the direcotor of the D 1 department should be TS 1.", "TS 1",
                result.get(0).getDirector().getName());
    }

    /**
     * Test method for the behavior of the update method when updating an entity
     * property which is also an entity.
     */
    @Test
    public void updateSHouldChangeTheDepartmentFaculty() {
        final EntityManager entityManager1 = EntityManagerCreator.create();
        final List<Department> departments = TransactionExecuter.selectForUpdate(entityManager1, () -> {
            final TypedQuery<Department> typedQuery = entityManager1
                    .createQuery("SELECT d FROM Department d WHERE name=:name", Department.class);
            typedQuery.setParameter("name", "D 1");
            return typedQuery.getResultList();
        });

        TransactionExecuter.update(entityManager1, () -> {
            departments.get(0).setFaculty((Faculty) dbData.get(1));
        });

        final EntityManager entityManager2 = EntityManagerCreator.create();
        final List<Department> result = TransactionExecuter.select(EntityManagerCreator.create(), () -> {
            final TypedQuery<Department> typedQuery = entityManager2
                    .createQuery("SELECT d FROM Department d WHERE name=:name", Department.class);
            typedQuery.setParameter("name", "D 1");
            return typedQuery.getResultList();
        });

        assertEquals("The name of the faculty of the D 1 department should be F 2.", "F 2",
                result.get(0).getFaculty().getName());
    }

    /**
     * Method used to empty the database after the tests are finished.
     */
    @AfterClass
    @SuppressWarnings("PMD.UnusedPrivateMethod")
    public static void removeDataFromDB() {
        final List<Object> entities = new ArrayList<>();
        for (int i = dbData.size() - 1; i >= 0; i--) {
            entities.add(dbData.get(i));
        }
        clearDatabase(entities.toArray());
    }

    private static void clearDatabase(final Object... objects) {
        for (final Object object : objects) {
            TransactionExecuter.delete(EntityManagerCreator.create(), object);
        }
    }

}
