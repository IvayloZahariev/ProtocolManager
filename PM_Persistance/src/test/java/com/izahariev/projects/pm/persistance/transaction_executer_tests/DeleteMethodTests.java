package com.izahariev.projects.pm.persistance.transaction_executer_tests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.Test;

import com.izahariev.projects.pm.persistance.entities.Faculty;
import com.izahariev.projects.pm.persistance.transactions.classes.EntityManagerCreator;
import com.izahariev.projects.pm.persistance.transactions.classes.TransactionExecuter;

/**
 * Method used to test the delete method in the {@link TransactionExecuter}
 * class.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings({ "static-method", "PMD.ShortVariable", "nls", "PMD.AtLeastOneConstructor" })
public class DeleteMethodTests {

    /**
     * Method used to test if the delete method works correctly.
     */
    @Test
    public void deleteShouldRemoveARowFromTheDatabase() {
        final Faculty f1 = new Faculty("F 1");
        TransactionExecuter.insert(EntityManagerCreator.create(), () -> {
            final ArrayList<Object> entities = new ArrayList<>();
            entities.add(f1);
            return entities;
        });

        TransactionExecuter.delete(EntityManagerCreator.create(), f1);

        final EntityManager entityManager = EntityManagerCreator.create();
        final List<Faculty> faculties = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<Faculty> typedQuery = entityManager.createQuery("SELECT f FROM Faculty f", Faculty.class);
            return typedQuery.getResultList();
        });

        assertTrue("The faculties list should be empty.", faculties.isEmpty());
    }

    /**
     * Test method for the behavior of the delete method when given an object
     * which is not an entity.
     */
    @Test(expected = IllegalArgumentException.class)
    public void deleteShouldThrowIllegalArgumentExceptionWhenGivenAnObjectWhichIsNotAnEntity() {
        final Object object = new Object();
        TransactionExecuter.delete(EntityManagerCreator.create(), object);
    }
}
