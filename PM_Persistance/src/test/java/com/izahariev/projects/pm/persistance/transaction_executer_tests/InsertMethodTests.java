/**
 * 
 */
package com.izahariev.projects.pm.persistance.transaction_executer_tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.Test;

import com.izahariev.projects.pm.persistance.entities.Department;
import com.izahariev.projects.pm.persistance.entities.Faculty;
import com.izahariev.projects.pm.persistance.entities.Protocol;
import com.izahariev.projects.pm.persistance.entities.ProtocolType;
import com.izahariev.projects.pm.persistance.entities.TeachingStuff;
import com.izahariev.projects.pm.persistance.transactions.classes.EntityManagerCreator;
import com.izahariev.projects.pm.persistance.transactions.classes.TransactionExecuter;

/**
 * Class containing unit tests for the {@link TransactionExecuter} insert
 * method.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings({ "nls", "static-method" })
public class InsertMethodTests {
    /**
     * Test method for the behavior of the insert method, when inserting an
     * object with no foreign keys.
     */
    @Test
    public void insertShouldAddNewRowInTheFacultyTable() {
        final Faculty faculty = new Faculty("TestFaculty");
        TransactionExecuter.insert(EntityManagerCreator.create(), () -> {
            final List<Object> entities = new ArrayList<>();
            entities.add(faculty);
            return entities;
        });

        final List<Faculty> result = getEntities("SELECT f FROM Faculty f", Faculty.class);

        assertEquals("The number of faculty entities in the database after the insert should be 1.", 1, result.size());
        assertEquals("The name of the added entity should be \"TestFaculty\".", "TestFaculty", result.get(0).getName());

        clearDatabase(faculty);
    }

    /**
     * Test method for the behavior of the insert method, when inserting an
     * object with one foreign key.
     */
    @Test
    public void insertShouldAddNewRowInTheDepartmentTable() {
        final Faculty faculty = new Faculty("FKSU");
        final Department department = new Department("TestDepartment", faculty);
        TransactionExecuter.insert(EntityManagerCreator.create(), () -> {
            final List<Object> entities = new ArrayList<>();
            entities.add(faculty);
            entities.add(department);
            return entities;
        });

        final List<Department> departments = getEntities("SELECT d FROM Department d", Department.class);

        assertEquals("The number of department entities in the database should be 1.", 1, departments.size());
        assertEquals("The name of the inserted department should be \"TestDepartment\".", "TestDepartment",
                departments.get(0).getName());
        assertEquals("The faculty's name of the department should be \"FKSU\".", "FKSU",
                departments.get(0).getFaculty().getName());

        clearDatabase(department, faculty);
    }

    /**
     * Test method for the behavior of the insert method, when inserting an
     * object with three foreign keys.
     */
    @Test
    @SuppressWarnings("PMD.JUnitTestContainsTooManyAsserts")
    public void insertShouldAddNewRowInProtocolTable() {
        final Faculty faculty = new Faculty("Random Name");
        final Department department = new Department("Department 2", faculty);
        final TeachingStuff reporter = new TeachingStuff("Ivo", department);
        final ProtocolType type = new ProtocolType("Fakulteten");
        final Date protocolDate = Calendar.getInstance().getTime();
        final Protocol protocol = new Protocol(department, 1, protocolDate, reporter, type);
        protocol.setReducedMembers(5);
        protocol.setQuorum(3);
        protocol.setPresentMembers(4);

        TransactionExecuter.insert(EntityManagerCreator.create(), () -> {
            final List<Object> entities = new ArrayList<>();
            entities.add(faculty);
            entities.add(department);
            entities.add(reporter);
            entities.add(type);
            entities.add(protocol);

            return entities;
        });

        final List<Protocol> protocols = getEntities("SELECT p FROM Protocol p", Protocol.class);

        assertEquals("The number of protocols in the database should be 1.", 1, protocols.size());
        assertEquals("The department of the protocol should have name \"Department 2\".", "Department 2",
                protocol.getDepartment().getName());
        assertEquals("The name of the reporter of the protocol should be \"Ivo\".", "Ivo",
                protocol.getReporter().getName());
        assertEquals("The name of the protocol type should be \"Fakulteten\".", "Fakulteten",
                protocol.getType().getName());
        assertEquals("The number of the protocol should be 1.", 1, protocol.getProtocolNumber());
        assertEquals("The date of the protocol should be " + protocolDate + ".", protocolDate, protocol.getDate());
        assertEquals("The number of reduced members of the protocol should be 5.", 5, protocol.getReducedMembers());
        assertEquals("The number of the quorum of the protocol should be 3.", 3, protocol.getQuorum());
        assertEquals("The number of present members of the protocol should be 4.", 4, protocol.getPresentMembers());

        clearDatabase(protocol, type, reporter, department, faculty);
    }

    /**
     * Test for the behavior of the insert method, when a non entity object is
     * tried to be inserted.
     */
    @Test(expected = IllegalArgumentException.class)
    public void insertShouldThrowIlligalArgumentExceptionWhenANonEntityObjectIsInserted() {
        TransactionExecuter.insert(EntityManagerCreator.create(), () -> {
            final List<Object> entities = new ArrayList<>();
            entities.add(new Object());

            return entities;
        });
    }

    private static <T> List<T> getEntities(final String query, final Class<T> entityType) {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<T> result = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<T> typedQuery = entityManager.createQuery(query, entityType);
            return typedQuery.getResultList();
        });

        return result;
    }

    private static void clearDatabase(final Object... objects) {
        for (final Object object : objects) {
            TransactionExecuter.delete(EntityManagerCreator.create(), object);
        }
    }
}
