/**
 * 
 */
package com.izahariev.projects.pm.services;

import java.util.List;

import com.izahariev.projects.pm.business_logic_interfaces.requests.TeachingStuffRequestsInterface;
import com.izahariev.projects.pm.factories.business_logic.BusinessLogicFactory;
import com.izahariev.projects.pm.services.interfaces.AddProtocolServiceInterafce;

/**
 * Class providing implementation of the methods in the
 * {@link AddProtocolServiceInterafce}.
 * 
 * @author Ivaylo Zahariev
 *
 */
public class AddProtocolServices implements AddProtocolServiceInterafce {

    /**
     * Deafult constructor.
     */
    public AddProtocolServices() {

    }

    /*
     * (non-Javadoc)
     * @see
     * com.izahariev.projects.pm.services.interfaces.AddProtocolServiceInterafce
     * #checkCouncilMember(java.lang.String, java.lang.String)
     */
    @Override
    @SuppressWarnings({ "PMD.CommentRequired", "nls" })
    public boolean checkCouncilMember(final String firstName, final String lastName) {
        final TeachingStuffRequestsInterface teachingStuffRequests = BusinessLogicFactory.getTeachingStuffRequests();
        return teachingStuffRequests.vaildatePerson(firstName.trim() + " " + lastName.trim());
    }

    /*
     * (non-Javadoc)
     * @see
     * com.izahariev.projects.pm.services.interfaces.AddProtocolServiceInterafce
     * #getTeachingStuff()
     */
    @Override
    @SuppressWarnings("PMD.CommentRequired")
    public List<String> getAcademicCouncilMembers() {
        final TeachingStuffRequestsInterface teachingStuffRequests = BusinessLogicFactory.getTeachingStuffRequests();
        return teachingStuffRequests.getAcademicCouncilMembers();
    }

    /*
     * (non-Javadoc)
     * @see
     * com.izahariev.projects.pm.services.interfaces.AddProtocolServiceInterafce
     * #getDepartments()
     */
    @Override
    @SuppressWarnings("PMD.CommentRequired")
    public List<String> getDepartments(final String facultyName) {
        return BusinessLogicFactory.getDepartmentRequests().getDepartments(facultyName);
    }

    /*
     * (non-Javadoc)
     * @see
     * com.izahariev.projects.pm.services.interfaces.AddProtocolServiceInterafce
     * #getFaculties()
     */
    @Override
    @SuppressWarnings("PMD.CommentRequired")
    public List<String> getFaculties() {
        return BusinessLogicFactory.getFacultyRequests().getAllFaculties();
    }

    /*
     * (non-Javadoc)
     * @see
     * com.izahariev.projects.pm.services.interfaces.AddProtocolServiceInterafce
     * #checkFaculty(java.lang.String, java.lang.String)
     */
    @Override
    @SuppressWarnings("PMD.CommentRequired")
    public boolean checkFaculty(final String faculty, final String department) {
        return BusinessLogicFactory.getDepartmentRequests().validateDepartmentFaculty(department, faculty);
    }

    /*
     * (non-Javadoc)
     * @see
     * com.izahariev.projects.pm.services.interfaces.AddProtocolServiceInterafce
     * #getNonAcademicCouncilMembers()
     */
    @Override
    @SuppressWarnings("PMD.CommentRequired")
    public List<String> getNonAcademicCouncilMembers() {
        return BusinessLogicFactory.getTeachingStuffRequests().getNonAcademicCouncilMembers();
    }

    /*
     * (non-Javadoc)
     * @see
     * com.izahariev.projects.pm.services.interfaces.AddProtocolServiceInterafce
     * #getDepartmentDirectorName(java.lang.String)
     */
    @Override
    @SuppressWarnings("PMD.CommentRequired")
    public String getDepartmentDirectorName(final String departmentName) {
        return BusinessLogicFactory.getDepartmentRequests().getDepartmentDirector(departmentName);
    }
}
