/**
 * 
 */
package com.izahariev.projects.pm.services;

import java.util.List;

import com.izahariev.projects.pm.factories.business_logic.BusinessLogicFactory;
import com.izahariev.projects.pm.services.interfaces.AddQuestionServiceInterface;

/**
 * Class providing implementation of the methods in the
 * {@link AddQuestionServiceInterface}.
 * 
 * @author Ivaylo Zahariev
 *
 */
public class AddQuestionServices implements AddQuestionServiceInterface {

    /**
     * Default constructor.
     */
    public AddQuestionServices() {

    }

    /*
     * (non-Javadoc)
     * @see
     * com.izahariev.projects.pm.services.interfaces.AddQuestionServiceInterface
     * #getQuestionTypes()
     */
    @Override
    @SuppressWarnings("PMD.CommentRequired")
    public List<String> getQuestionTypes() {
        return BusinessLogicFactory.getQuestionTypeRequests().getQuestionTypes();
    }

}
