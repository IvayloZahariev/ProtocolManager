/**
 * 
 */
package com.izahariev.projects.pm.services;

import com.izahariev.projects.pm.beans.ProtocolBean;
import com.izahariev.projects.pm.factories.business_logic.BusinessLogicFactory;
import com.izahariev.projects.pm.services.interfaces.DisaplySingleProtocolServicesInterface;

/**
 * @author Ivaylo Zahariev
 *
 */
public class DisplaySingleProtocolServices implements DisaplySingleProtocolServicesInterface {

    /**
     * Default constructor.
     */
    public DisplaySingleProtocolServices() {

    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.services.interfaces.
     * DisaplySingleProtocolServicesInterface#addProtocol(com.izahariev.projects
     * .pm.beans.ProtocolBean)
     */
    @Override
    @SuppressWarnings("PMD.CommentRequired")
    public void addProtocol(final ProtocolBean protocolBean) {
        BusinessLogicFactory.getProtocolRequests().addProtocol(protocolBean);
    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.services.interfaces.
     * DisaplySingleProtocolServicesInterface#updateProtocol(com.izahariev.
     * projects.pm.beans.ProtocolBean,
     * com.izahariev.projects.pm.beans.ProtocolBean)
     */
    @Override
    @SuppressWarnings("PMD.CommentRequired")
    public void updateProtocol(final ProtocolBean oldBean, final ProtocolBean newBean) {
        BusinessLogicFactory.getProtocolRequests().updateProtocol(oldBean, newBean);
    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.services.interfaces.
     * DisaplySingleProtocolServicesInterface#deleteProtocol(com.izahariev.
     * projects.pm.beans.ProtocolBean)
     */
    @Override
    public void deleteProtocol(final ProtocolBean bean) {
        BusinessLogicFactory.getProtocolRequests().deleteProtocol(bean);
    }
}
