/**
 * 
 */
package com.izahariev.projects.pm.services;

import java.util.List;

import com.izahariev.projects.pm.beans.ProtocolBean;
import com.izahariev.projects.pm.factories.business_logic.BusinessLogicFactory;
import com.izahariev.projects.pm.services.interfaces.FindProtocolServiceinterface;

/**
 * Class providing implementation of the methods in the
 * {@link FindProtocolServiceinterface}.
 * 
 * @author Ivaylo Zahariev
 *
 */
public class FindProtocolServices implements FindProtocolServiceinterface {

    /**
     * Default constructor.
     */
    public FindProtocolServices() {

    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.services.interfaces.
     * FindProtocolServiceinterface#getFaculties()
     */
    @Override
    @SuppressWarnings("PMD.CommentRequired")
    public List<String> getFaculties() {
        return BusinessLogicFactory.getFacultyRequests().getAllFaculties();
    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.services.interfaces.
     * FindProtocolServiceinterface#getDepartments()
     */
    @Override
    @SuppressWarnings("PMD.CommentRequired")
    public List<String> getDepartments() {
        return BusinessLogicFactory.getDepartmentRequests().getAllDepartments();
    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.services.interfaces.
     * FindProtocolServiceinterface#getTeachingStuff()
     */
    @Override
    @SuppressWarnings("PMD.CommentRequired")
    public List<String> getNonAcademicCouncilMembers() {
        return BusinessLogicFactory.getTeachingStuffRequests().getNonAcademicCouncilMembers();
    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.services.interfaces.
     * FindProtocolServiceinterface#getProtocols(java.lang.String, int,
     * java.util.Date, java.lang.String, java.lang.String, java.lang.String)
     */
    @SuppressWarnings({ "nls", "PMD.CommentRequired" })
    @Override
    public List<ProtocolBean> getProtocols(final String protocolType, final String protocolNumber,
            final String councilDate, final String faculty, final String department, final String reporter,
            final String questionSpecification, final String questionDecision) {
        String protocolTypeValue = protocolType;
        String facultyValue = faculty;
        String departmentValue = department;
        String reporterValue = reporter;
        if (protocolType == null) {
            protocolTypeValue = "";
        }
        if ("default".equals(faculty)) {
            facultyValue = "";
        }
        if ("default".equals(department)) {
            departmentValue = "";
        }
        if ("default".equals(reporter)) {
            reporterValue = "";
        }
        return BusinessLogicFactory.getProtocolRequests().getProtocols(protocolTypeValue, protocolNumber, councilDate,
                facultyValue, departmentValue, reporterValue, questionSpecification, questionDecision);
    }

}
