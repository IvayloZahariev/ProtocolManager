/**
 * 
 */
package com.izahariev.projects.pm.services;

import com.izahariev.projects.pm.business_logic_interfaces.requests.UsersRequestsInterface;
import com.izahariev.projects.pm.factories.business_logic.BusinessLogicFactory;
import com.izahariev.projects.pm.services.interfaces.LoginServiceInterface;

/**
 * Class containing the services used by the LoginPageServlet.
 * 
 * @author Ivaylo Zahariev
 *
 */
public class LoginServices implements LoginServiceInterface {

    /**
     * Default constructor.
     */
    public LoginServices() {

    }

    @Override
    @SuppressWarnings("PMD.CommentRequired")
    public boolean checkUser(final String username, final String password) {
        final UsersRequestsInterface usersRequest = BusinessLogicFactory.getUserRequests();

        return usersRequest.validateUser(username, password);
    }
}
