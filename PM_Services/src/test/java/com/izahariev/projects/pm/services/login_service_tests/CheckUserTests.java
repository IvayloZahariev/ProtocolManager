/**
 * 
 */
package com.izahariev.projects.pm.services.login_service_tests;

import org.junit.BeforeClass;

import com.izahariev.projects.pm.services.LoginServices;

/**
 * Class containing unit tests for the checkUser method in the
 * {@link LoginServices} class.
 * 
 * @author Ivaylo Zahariev
 *
 */
// @RunWith(PowerMockRunner.class)
// @PrepareForTest({ BusinessLogicFactory.class, LoginServices.class })
@SuppressWarnings("PMD.UseUtilityClass")
public class CheckUserTests {

    /**
     * 
     */
    @BeforeClass
    public static void prepareMock() {
        // PowerMock.mockStatic(BusinessLogicFactory.class);
        //
        // final UserRequests userRequestsMock =
        // Mockito.mock(UserRequests.class);
        // Mockito.when(userRequestsMock.validate("admin",
        // "CustomAdminPass100")).thenReturn(true);
        //
        // Mockito.when(BusinessLogicFactory.getUserRequests()).thenReturn(userRequestsMock);
    }

    /**
     * Test method for
     * {@link com.izahariev.projects.pm.services.LoginServices#checkUser(java.lang.String, java.lang.String)}.
     */
    // @Test
    // public void testCheckUser() {
    // final LoginServices loginServices = new LoginServices();
    // final boolean result = loginServices.checkUser("admin",
    // "CustomAdminPass100");
    // assertTrue(result);
    // }

}
