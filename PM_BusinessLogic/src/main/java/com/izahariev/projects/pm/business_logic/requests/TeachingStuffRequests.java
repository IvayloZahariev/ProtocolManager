/**
 * 
 */
package com.izahariev.projects.pm.business_logic.requests;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringEscapeUtils;

import com.izahariev.projects.pm.business_logic_interfaces.requests.TeachingStuffRequestsInterface;
import com.izahariev.projects.pm.persistance.entities.TeachingStuff;
import com.izahariev.projects.pm.persistance.transactions.classes.EntityManagerCreator;
import com.izahariev.projects.pm.persistance.transactions.classes.TransactionExecuter;

/**
 * Class implementing the methods in the {@link TeachingStuffRequestsInterface}.
 * 
 * @author Ivaylo Zahariev
 *
 */
public class TeachingStuffRequests implements TeachingStuffRequestsInterface {

    /**
     * Default constructor.
     */
    public TeachingStuffRequests() {

    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.business_logic_interfaces.requests.
     * TeachingStuffRequestsInterface#vaildatePerson(java.lang.String)
     */
    @SuppressWarnings({ "nls", "PMD.CommentRequired" })
    @Override
    public boolean vaildatePerson(final String name) {

        final EntityManager entityManager = EntityManagerCreator.create();
        final List<TeachingStuff> result = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<TeachingStuff> typedQuery = entityManager
                    .createQuery("SELECT ts FROM TeachingStuff ts WHERE name=:name", TeachingStuff.class);
            typedQuery.setParameter("name", StringEscapeUtils.escapeHtml(name));
            return typedQuery.getResultList();
        });

        return !result.isEmpty();
    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.business_logic_interfaces.requests.
     * TeachingStuffRequestsInterface#getTeachingStuff()
     */
    @SuppressWarnings({ "nls", "PMD.CommentRequired", "PMD.AvoidLiteralsInIfCondition" })
    @Override
    public List<String> getAcademicCouncilMembers() {

        final EntityManager entityManager = EntityManagerCreator.create();
        final List<TeachingStuff> result = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<TeachingStuff> typedQuery = entityManager.createQuery("SELECT ts FROM TeachingStuff ts",
                    TeachingStuff.class);
            return typedQuery.getResultList();
        });

        final List<String> names = new ArrayList<>();
        for (final TeachingStuff member : result) {
            if (member.getIsAcademicCouncilMember() == 1) {
                names.add(StringEscapeUtils.unescapeHtml(member.getName()));
            }
        }

        return names;
    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.business_logic_interfaces.requests.
     * TeachingStuffRequestsInterface#getNonAcademicCouncilMembers()
     */
    @SuppressWarnings({ "nls", "PMD.CommentRequired" })
    @Override
    public List<String> getNonAcademicCouncilMembers() {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<TeachingStuff> result = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<TeachingStuff> typedQuery = entityManager.createQuery("SELECT ts FROM TeachingStuff ts",
                    TeachingStuff.class);
            return typedQuery.getResultList();
        });

        final List<String> names = new ArrayList<>();
        for (final TeachingStuff member : result) {
            if (member.getIsAcademicCouncilMember() == 0) {
                names.add(StringEscapeUtils.unescapeHtml(member.getName()));
            }
        }

        return names;
    }
}
