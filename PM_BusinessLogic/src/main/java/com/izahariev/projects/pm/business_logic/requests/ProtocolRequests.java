/**
 * 
 */
package com.izahariev.projects.pm.business_logic.requests;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringEscapeUtils;

import com.izahariev.projects.pm.beans.CommentBean;
import com.izahariev.projects.pm.beans.ProtocolBean;
import com.izahariev.projects.pm.beans.QuestionBean;
import com.izahariev.projects.pm.beans.VoteBean;
import com.izahariev.projects.pm.business_logic_interfaces.requests.ProtocolRequestsInterface;
import com.izahariev.projects.pm.persistance.entities.Absent;
import com.izahariev.projects.pm.persistance.entities.Department;
import com.izahariev.projects.pm.persistance.entities.Protocol;
import com.izahariev.projects.pm.persistance.entities.ProtocolAgenda;
import com.izahariev.projects.pm.persistance.entities.ProtocolType;
import com.izahariev.projects.pm.persistance.entities.Question;
import com.izahariev.projects.pm.persistance.entities.QuestionComment;
import com.izahariev.projects.pm.persistance.entities.QuestionType;
import com.izahariev.projects.pm.persistance.entities.TeachingStuff;
import com.izahariev.projects.pm.persistance.entities.Vote;
import com.izahariev.projects.pm.persistance.entities.VoteType;
import com.izahariev.projects.pm.persistance.transactions.classes.EntityManagerCreator;
import com.izahariev.projects.pm.persistance.transactions.classes.TransactionExecuter;

/**
 * Class providing implementation for the methods in the
 * {@link ProtocolRequestsInterface}.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings({ "PMD.AvoidInstantiatingObjectsInLoops", "PMD.GodClass" })
public class ProtocolRequests implements ProtocolRequestsInterface {

    /**
     * Default constructor.
     */
    public ProtocolRequests() {

    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.business_logic_interfaces.requests.
     * ProtocolRequestsInterface#getProtocols(java.lang.String, int,
     * java.util.Date, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    @SuppressWarnings("PMD.CommentRequired")
    public List<ProtocolBean> getProtocols(final String protocolType, final String protocolNumber,
            final String councilDate, final String faculty, final String department, final String reporter,
            final String questionSpecification, final String questionDecision) {

        final List<Protocol> protocols = getProtocolEntities(protocolType, protocolNumber, councilDate, faculty,
                department, reporter);

        if (!questionSpecification.isEmpty() || !questionDecision.isEmpty()) {
            return setProtocolBeans(
                    getProtocolsWithSpecificationAndDecision(protocols, questionSpecification, questionDecision));
        }
        return setProtocolBeans(protocols);
    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.business_logic_interfaces.requests.
     * ProtocolRequestsInterface#addProtocol(com.izahariev.projects.pm.beans.
     * ProtocolBean)
     */
    @SuppressWarnings("PMD.CommentRequired")
    @Override
    public void addProtocol(final ProtocolBean protocolBean) {
        final Protocol protocol = new Protocol();
        protocol.setDepartment(getDepartment(protocolBean.getDepartment()));
        protocol.setProtocolNumber(protocolBean.getNumber());
        protocol.setDate(protocolBean.getCouncilDate());
        protocol.setReporter(getPerson(protocolBean.getReporter()));
        protocol.setType(getType(protocolBean.getType()));
        protocol.setAllMembers(protocolBean.getAllMembers());
        protocol.setReducedMembers(protocolBean.getReducedMembers());
        protocol.setQuorum(protocolBean.getQuorum());
        protocol.setPresentMembers(protocolBean.getPresentMembers());
        TransactionExecuter.persistEntity(EntityManagerCreator.create(), protocol);
        setProtocolAgenda(protocolBean, protocol);
        setQuestionEntities(protocolBean.getQuestions(), protocol);
        setCommentEntities(protocol, protocolBean.getQuestions());
        setVoteEntities(protocol, protocolBean.getQuestions());
        setAbsentEntities(protocolBean.getAbsentMembersNames(), protocolBean.getFurloughMembersNames(), protocol);
        setOtherAbsentEntities(protocolBean.getOtherAbsentMembersNames(), protocolBean.getOtherFurloughMembersNames(),
                protocol);
    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.business_logic_interfaces.requests.
     * ProtocolRequestsInterface#updateProtocol(com.izahariev.projects.pm.beans.
     * ProtocolBean, com.izahariev.projects.pm.beans.ProtocolBean)
     */
    @Override
    @SuppressWarnings({ "PMD.CommentRequired", "boxing" })
    public void updateProtocol(final ProtocolBean oldBean, final ProtocolBean newBean) {
        final Protocol protocol = getProtocolEntities(oldBean.getType(), Integer.toString(oldBean.getNumber()),
                oldBean.getCouncilDateForDateField(), oldBean.getFaculty(), oldBean.getDepartment(),
                oldBean.getReporter()).get(0);
        final EntityManager entityManager = EntityManagerCreator.create();
        TransactionExecuter.update(entityManager, () -> {
            final Protocol protocolToBeChanged = entityManager.find(Protocol.class, protocol.getId());
            protocolToBeChanged.setDepartment(getDepartment(newBean.getDepartment()));
            protocolToBeChanged.setProtocolNumber(newBean.getNumber());
            protocolToBeChanged.setDate(newBean.getCouncilDate());
            protocolToBeChanged.setReporter(getPerson(newBean.getReporter()));
            protocolToBeChanged.setType(getType(newBean.getType()));
            protocolToBeChanged.setAllMembers(newBean.getAllMembers());
            protocolToBeChanged.setReducedMembers(newBean.getReducedMembers());
            protocolToBeChanged.setQuorum(newBean.getQuorum());
            protocolToBeChanged.setPresentMembers(newBean.getPresentMembers());
        });
        updateAgenda(newBean, protocol);
        updateQuestions(newBean, protocol);
        updateAbsentMembers(newBean.getAbsentMembersNames(), newBean.getFurloughMembersNames(),
                newBean.getOtherAbsentMembersNames(), newBean.getOtherFurloughMembersNames(), protocol);

    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.business_logic_interfaces.requests.
     * ProtocolRequestsInterface#deleteProtocol(com.izahariev.projects.pm.beans.
     * ProtocolBean)
     */
    @Override
    @SuppressWarnings("PMD.CommentRequired")
    public void deleteProtocol(final ProtocolBean bean) {
        final Protocol protocol = getProtocolEntities(bean.getType(), Integer.toString(bean.getNumber()),
                bean.getCouncilDateForDateField(), bean.getFaculty(), bean.getDepartment(), bean.getReporter()).get(0);
        for (final ProtocolAgenda agendaPoint : protocol.getProtocolAgenda()) {
            TransactionExecuter.delete(EntityManagerCreator.create(), agendaPoint);
        }
        for (final Absent absent : getAbsent(protocol)) {
            TransactionExecuter.delete(EntityManagerCreator.create(), absent);
        }
        for (final Question question : protocol.getQuestions()) {
            for (final QuestionComment comment : question.getComments()) {
                TransactionExecuter.delete(EntityManagerCreator.create(), comment);
            }
            for (final Vote vote : question.getVotes()) {
                TransactionExecuter.delete(EntityManagerCreator.create(), vote);
            }
            TransactionExecuter.delete(EntityManagerCreator.create(), question);
        }
        TransactionExecuter.delete(EntityManagerCreator.create(), protocol);
    }

    private static void updateAgenda(final ProtocolBean protocolBean, final Protocol protocol) {
        for (final ProtocolAgenda agendaPoint : protocol.getProtocolAgenda()) {
            TransactionExecuter.delete(EntityManagerCreator.create(), agendaPoint);
        }
        setProtocolAgenda(protocolBean, protocol);
    }

    private static void updateQuestions(final ProtocolBean protocolBean, final Protocol protocol) {
        for (final Question question : protocol.getQuestions()) {
            for (final QuestionComment comment : question.getComments()) {
                TransactionExecuter.delete(EntityManagerCreator.create(), comment);
            }
            for (final Vote vote : question.getVotes()) {
                TransactionExecuter.delete(EntityManagerCreator.create(), vote);
            }
            question.getComments().clear();
            question.getVotes().clear();
            TransactionExecuter.delete(EntityManagerCreator.create(), question);
        }
        setQuestionEntities(protocolBean.getQuestions(), protocol);
        setCommentEntities(protocol, protocolBean.getQuestions());
        setVoteEntities(protocol, protocolBean.getQuestions());
    }

    private static void updateAbsentMembers(final List<String> absentMembers, final List<String> furloughMembers,
            final List<String> otherAbsentMembers, final List<String> otherFurloughMembers, final Protocol protocol) {
        for (final Absent absentMember : getAbsent(protocol)) {
            TransactionExecuter.delete(EntityManagerCreator.create(), absentMember);
        }
        setAbsentEntities(absentMembers, furloughMembers, protocol);
        setOtherAbsentEntities(otherAbsentMembers, otherFurloughMembers, protocol);
    }

    @SuppressWarnings({ "nls", "PMD.CommentRequired", "boxing", "PMD.NPathComplexity" })
    private static List<Protocol> getProtocolEntities(final String protocolType, final String protocolNumber,
            final String councilDate, final String faculty, final String department, final String reporter) {
        final StringBuilder whereClause = new StringBuilder();
        if (!(protocolType.isEmpty() && protocolNumber.isEmpty() && councilDate.isEmpty() && faculty.isEmpty()
                && department.isEmpty() && reporter.isEmpty())) {
            whereClause
                    .append(buildWhereClause(protocolType, protocolNumber, councilDate, faculty, department, reporter));
        }

        final EntityManager entityManager = EntityManagerCreator.create();
        final List<Protocol> protocols = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<Protocol> typedQuery = entityManager
                    .createQuery("SELECT p FROM Protocol p" + whereClause.toString(), Protocol.class);

            if (!protocolType.isEmpty()) {
                setQueryParameter(typedQuery, "type", getType(protocolType));
            }
            if (!protocolNumber.isEmpty()) {
                setQueryParameter(typedQuery, "protocolNumber", Integer.parseInt(protocolNumber));
            }
            if (!councilDate.isEmpty()) {
                setQueryParameter(typedQuery, "date", Date.valueOf(councilDate));
            }
            if (!department.isEmpty()) {
                setQueryParameter(typedQuery, "department", getDepartment(department));
            }
            if (!reporter.isEmpty()) {
                setQueryParameter(typedQuery, "reporter", getPerson(reporter));
            }
            return typedQuery.getResultList();
        });

        return protocols;
    }

    private static void setVoteEntities(final Protocol protocol, final List<QuestionBean> questionBeans) {
        final List<Question> questions = getQuestions(protocol);
        for (final QuestionBean questionBean : questionBeans) {
            for (final VoteBean voteBean : questionBean.getVotes()) {
                Question neededQuestion = null;
                for (final Question question : questions) {
                    if (question.getNumber() == questionBean.getNumber()) {
                        neededQuestion = question;
                        break;
                    }
                }
                final Vote vote = new Vote(voteBean.getNumYesVotes(), voteBean.getNumNoVotes(),
                        voteBean.getNumBlankVotes(), getPerson(voteBean.getModerator()),
                        getPerson(voteBean.getMember1()), getPerson(voteBean.getMember2()), neededQuestion,
                        getVoteType(voteBean.getVoteType()), StringEscapeUtils.escapeHtml(voteBean.getVoteSubject()));
                TransactionExecuter.mergeEntity(EntityManagerCreator.create(), vote);
            }
        }
    }

    private static void setCommentEntities(final Protocol protocol, final List<QuestionBean> questionBeans) {
        final List<Question> questions = getQuestions(protocol);
        for (final QuestionBean questionBean : questionBeans) {
            for (final CommentBean commentBean : questionBean.getComments()) {
                Question neededQuestion = null;
                for (final Question question : questions) {
                    if (question.getNumber() == questionBean.getNumber()) {
                        neededQuestion = question;
                        break;
                    }
                }

                QuestionComment comment;
                if (getPerson(commentBean.getOwner()) == null) {
                    comment = new QuestionComment();
                    comment.setQuestion(neededQuestion);
                    comment.setSpecification(StringEscapeUtils.escapeHtml(commentBean.getSpecification()));
                    comment.setOtherCommentOwner(StringEscapeUtils.escapeHtml(commentBean.getOwner()));
                }
                else {
                    comment = new QuestionComment(neededQuestion, getPerson(commentBean.getOwner()),
                            StringEscapeUtils.escapeHtml(commentBean.getSpecification()));
                }
                TransactionExecuter.mergeEntity(EntityManagerCreator.create(), comment);
            }
        }
    }

    @SuppressWarnings("nls")
    private static String buildWhereClause(final String protocolType, final String protocolNumber,
            final String councilDate, final String faculty, final String department, final String reporter) {
        final StringBuilder whereClause = new StringBuilder();
        whereClause.append(" WHERE ");
        boolean andFlag = false;
        // We doesn't know when we will need the AND keyword in the where clause
        // so we are initializing it after each parameter check.
        andFlag = checkAndAppendParameters(protocolType, "type", whereClause, andFlag);
        andFlag = checkAndAppendParameters(protocolNumber, "protocolNumber", whereClause, andFlag);
        andFlag = checkAndAppendParameters(councilDate, "date", whereClause, andFlag);
        andFlag = checkAndAppendParameterDepartment(faculty, department, whereClause, andFlag);
        checkAndAppendParameters(reporter, "reporter", whereClause, andFlag);

        return whereClause.toString();
    }

    @SuppressWarnings("nls")
    private static boolean checkAndAppendParameters(final String parameter, final String parameterName,
            final StringBuilder whereClause, final boolean andFlag) {
        if (!parameter.isEmpty()) {
            if (andFlag) {
                whereClause.append(" AND ");
            }
            whereClause.append(parameterName);
            whereClause.append("=:");
            whereClause.append(parameterName);
            if (!andFlag) {
                return true;
            }
        }
        return andFlag;
    }

    @SuppressWarnings({ "nls", "PMD.AppendCharacterWithChar" })
    private static boolean checkAndAppendParameterDepartment(final String faculty, final String department,
            final StringBuilder whereClause, final boolean andFlag) {
        if (department.isEmpty()) {
            if (faculty.isEmpty()) {
                return andFlag;
            }
            if (andFlag) {
                whereClause.append(" AND ");
            }
            whereClause.append("department.id IN (");
            final DepartmentRequests departmentRequests = new DepartmentRequests();
            final List<String> departmentsIDs = departmentRequests.getDepartmentsIDs(faculty);
            for (int i = 0; i < departmentsIDs.size(); i++) {
                whereClause.append("'");
                whereClause.append(departmentsIDs.get(i));
                whereClause.append("'");
                if (i < departmentsIDs.size() - 1) {
                    whereClause.append(", ");
                }
            }
            whereClause.append(')');
            return true;
        }
        return checkAndAppendParameters(department, "department", whereClause, andFlag);
    }

    private static <T> void setQueryParameter(final TypedQuery<T> query, final String parameterName,
            final Object parameterValue) {
        query.setParameter(parameterName, parameterValue);
    }

    @SuppressWarnings({ "nls", "PMD.AvoidDuplicateLiterals" })
    private static ProtocolType getType(final String typeName) {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<ProtocolType> questionTypes = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<ProtocolType> typedQuery = entityManager
                    .createQuery("SELECT pt FROM ProtocolType pt WHERE name=:name", ProtocolType.class);
            typedQuery.setParameter("name", StringEscapeUtils.escapeHtml(typeName));
            return typedQuery.getResultList();
        });
        return questionTypes.get(0);
    }

    @SuppressWarnings("nls")
    private static Department getDepartment(final String name) {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<Department> departments = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<Department> typedQuery = entityManager
                    .createQuery("SELECT pt FROM Department pt WHERE name=:name", Department.class);
            typedQuery.setParameter("name", StringEscapeUtils.escapeHtml(name));
            return typedQuery.getResultList();
        });
        return departments.get(0);
    }

    @SuppressWarnings("nls")
    private static TeachingStuff getPerson(final String name) {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<TeachingStuff> questionTypes = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<TeachingStuff> typedQuery = entityManager
                    .createQuery("SELECT ts FROM TeachingStuff ts WHERE name=:name", TeachingStuff.class);
            typedQuery.setParameter("name", StringEscapeUtils.escapeHtml(name));
            return typedQuery.getResultList();
        });

        if (questionTypes.isEmpty()) {
            return null;
        }
        return questionTypes.get(0);
    }

    @SuppressWarnings("nls")
    private static VoteType getVoteType(final String name) {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<VoteType> voteTypes = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<VoteType> typedQuery = entityManager
                    .createQuery("SELECT vt FROM VoteType vt WHERE name=:name", VoteType.class);
            typedQuery.setParameter("name", StringEscapeUtils.escapeHtml(name));
            return typedQuery.getResultList();
        });
        return voteTypes.get(0);
    }

    @SuppressWarnings({ "nls", "PMD.AvoidDuplicateLiterals" })
    private static List<ProtocolBean> setProtocolBeans(final List<Protocol> protocolEntities) {
        final List<ProtocolBean> protocolBeans = new ArrayList<>();
        for (final Protocol protocol : protocolEntities) {
            final ProtocolBean bean = new ProtocolBean();
            bean.setFaculty(StringEscapeUtils.unescapeHtml(protocol.getDepartment().getFaculty().getName()));
            bean.setDepartment(StringEscapeUtils.unescapeHtml(protocol.getDepartment().getName()));
            bean.setDepartmentDirector(
                    StringEscapeUtils.unescapeHtml(protocol.getDepartment().getDirector().getName()));
            bean.setNumber(protocol.getProtocolNumber());
            bean.setCouncilDate(new java.util.Date(protocol.getDate().getTime()));
            bean.setReporter(StringEscapeUtils.unescapeHtml(protocol.getReporter().getName()));
            bean.setType(StringEscapeUtils.unescapeHtml(protocol.getType().getName()));
            bean.setAllMembers(protocol.getAllMembers());
            bean.setAbsent(getAbsentNumber(getAbsent(protocol), "Отсъства"));
            bean.setAbsentMembersNames(setAbsentMembersNames(protocol, "Отсъства"));
            bean.setFurlough(getAbsentNumber(getAbsent(protocol), "Отпуск"));
            bean.setFurloughMembersNames(setAbsentMembersNames(protocol, "Отпуск"));
            bean.setOtherAbsentMembersNames(setOtherAbsentMembersNames(protocol, "Отсъства"));
            bean.setOtherFurloughMembersNames(setOtherAbsentMembersNames(protocol, "Отпуск"));
            bean.setReducedMembers(protocol.getReducedMembers());
            bean.setQuorum(protocol.getQuorum());
            bean.setPresentMembers(protocol.getPresentMembers());
            bean.setQuestions(setQuestionBeans(protocol));
            bean.setAgenda(getAgenda(protocol));
            protocolBeans.add(bean);
        }
        return protocolBeans;
    }

    private static List<String> getAgenda(final Protocol protocol) {
        final List<String> agendaPoints = new ArrayList<>();
        for (final Question question : protocol.getQuestions()) {
            final String questionType = StringEscapeUtils.unescapeHtml(question.getType().getName());
            if (!agendaPoints.contains(questionType)) {
                agendaPoints.add(questionType);
            }
        }
        return agendaPoints;
    }

    @SuppressWarnings({ "nls", "PMD.StdCyclomaticComplexity" })
    private static List<Protocol> getProtocolsWithSpecificationAndDecision(final List<Protocol> protocols,
            final String questionSpecification, final String questionDecision) {
        final Set<Protocol> protocolsWithSpecification = new HashSet<>();
        final Set<Protocol> protocolsWithDecision = new HashSet<>();
        final String[] specifications = questionSpecification.split("///");
        final String[] decisions = questionDecision.split("///");
        for (final Protocol protocol : protocols) {
            for (final Question question : protocol.getQuestions()) {
                if (!questionSpecification.isEmpty()) {
                    for (final String specification : specifications) {
                        if (StringEscapeUtils.unescapeHtml(question.getSpecification()).contains(specification)) {
                            protocolsWithSpecification.add(protocol);
                        }
                    }
                }
                if (!questionDecision.isEmpty()) {
                    for (final String decision : decisions) {
                        if (StringEscapeUtils.unescapeHtml(question.getDecision()).contains(decision)) {
                            protocolsWithDecision.add(protocol);
                        }
                    }
                }
            }
        }
        final List<Protocol> result = new ArrayList<>();
        if (protocolsWithSpecification.isEmpty()) {
            result.addAll(protocolsWithDecision);
        }
        else if (protocolsWithDecision.isEmpty()) {
            result.addAll(protocolsWithSpecification);
        }
        else {
            protocolsWithSpecification.retainAll(protocolsWithDecision);
            result.addAll(protocolsWithSpecification);
        }

        return result;
    }

    private static List<QuestionBean> setQuestionBeans(final Protocol protocol) {
        final List<Question> questions = getQuestions(protocol);
        final List<QuestionBean> questionBeans = new ArrayList<>();
        for (final Question question : questions) {
            final QuestionBean bean = new QuestionBean();
            bean.setNumber(question.getNumber());
            bean.setType(StringEscapeUtils.unescapeHtml(question.getType().getName()));
            bean.setSpecification(StringEscapeUtils.unescapeHtml(question.getSpecification()));
            bean.setDecision(StringEscapeUtils.unescapeHtml(question.getDecision()));
            bean.setComments(setCommentBeans(question));
            bean.setVotes(setVoteBeans(question));
            questionBeans.add(bean);
        }

        return questionBeans;
    }

    @SuppressWarnings("nls")
    private static List<Question> getQuestions(final Protocol protocol) {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<Question> questions = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<Question> typedQuery = entityManager
                    .createQuery("SELECT q FROM Question q WHERE protocol=:protocol", Question.class);
            typedQuery.setParameter("protocol", protocol);
            return typedQuery.getResultList();
        });
        return questions;
    }

    private static List<CommentBean> setCommentBeans(final Question question) {
        final List<QuestionComment> comments = question.getComments();
        final List<CommentBean> commentBeans = new ArrayList<>();
        for (final QuestionComment comment : comments) {
            final CommentBean bean = new CommentBean();
            bean.setQuestionNumber(question.getNumber());
            if (comment.getCommentOwner() == null) {
                bean.setOwner(StringEscapeUtils.unescapeHtml(comment.getOtherCommentOwner()));
            }
            else {
                bean.setOwner(StringEscapeUtils.unescapeHtml(comment.getCommentOwner().getName()));
            }
            bean.setSpecification(StringEscapeUtils.unescapeHtml(comment.getSpecification()));
            commentBeans.add(bean);
        }
        return commentBeans;
    }

    private static List<VoteBean> setVoteBeans(final Question question) {
        final List<Vote> votes = question.getVotes();
        final List<VoteBean> voteBeans = new ArrayList<>();
        for (final Vote vote : votes) {
            final VoteBean bean = new VoteBean();
            bean.setQuestionNumber(question.getNumber());
            bean.setVoteSubject(StringEscapeUtils.unescapeHtml(vote.getVoteSubject()));
            bean.setVoteType(StringEscapeUtils.unescapeHtml(vote.getType().getName()));
            bean.setNumYesVotes(vote.getNumberYes());
            bean.setNumNoVotes(vote.getNumberNo());
            bean.setNumBlankVotes(vote.getNumberWhite());
            bean.setModerator(StringEscapeUtils.unescapeHtml(vote.getModerator().getName()));
            bean.setMember1(StringEscapeUtils.unescapeHtml(vote.getMember1().getName()));
            bean.setMember2(StringEscapeUtils.unescapeHtml(vote.getMember2().getName()));
            voteBeans.add(bean);
        }
        return voteBeans;
    }

    @SuppressWarnings("nls")
    private static List<Absent> getAbsent(final Protocol protocol) {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<Absent> absents = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<Absent> typedQuery = entityManager
                    .createQuery("SELECT a FROM Absent a WHERE protocol=:protocol", Absent.class);
            typedQuery.setParameter("protocol", protocol);
            return typedQuery.getResultList();
        });
        return absents;
    }

    private static int getAbsentNumber(final List<Absent> absents, final String absentReason) {
        int number = 0;
        for (final Absent absent : absents) {
            if (absentReason.equals(StringEscapeUtils.unescapeHtml(absent.getReason()))) {
                number++;
            }
        }
        return number;
    }

    private static List<String> setAbsentMembersNames(final Protocol protocol, final String absentReason) {
        final List<Absent> absents = getAbsent(protocol);
        final List<String> absentsNames = new ArrayList<>();
        for (final Absent absent : absents) {
            if (StringEscapeUtils.unescapeHtml(absent.getReason()).equals(absentReason)
                    && absent.getPersonName() == null) {
                absentsNames.add(StringEscapeUtils.unescapeHtml(absent.getPerson().getName()));
            }
        }
        return absentsNames;
    }

    private static List<String> setOtherAbsentMembersNames(final Protocol protocol, final String absentReason) {
        final List<Absent> absents = getAbsent(protocol);
        final List<String> absentsNames = new ArrayList<>();
        for (final Absent absent : absents) {
            if (StringEscapeUtils.unescapeHtml(absent.getReason()).equals(absentReason) && absent.getPerson() == null) {
                absentsNames.add(StringEscapeUtils.unescapeHtml(absent.getPersonName()));
            }
        }
        return absentsNames;
    }

    @SuppressWarnings("nls")
    private static void setProtocolAgenda(final ProtocolBean protocolBean, final Protocol protocol) {
        final Set<String> questionTypes = new HashSet<>();
        for (final QuestionBean questionBean : protocolBean.getQuestions()) {
            questionTypes.add(questionBean.getType());
        }
        checkAndAddQuestionType(questionTypes, "Учебни", protocol);
        checkAndAddQuestionType(questionTypes, "Докторантски", protocol);
        checkAndAddQuestionType(questionTypes, "Кадрови", protocol);
        checkAndAddQuestionType(questionTypes, "Разни", protocol);
    }

    private static void checkAndAddQuestionType(final Set<String> questionTypes, final String questionTypeName,
            final Protocol protocol) {
        if (questionTypes.contains(questionTypeName)) {
            final ProtocolAgenda protocolAgenda = new ProtocolAgenda(protocol, getQuestionType(questionTypeName));
            TransactionExecuter.mergeEntity(EntityManagerCreator.create(), protocolAgenda);
        }
    }

    @SuppressWarnings("nls")
    private static QuestionType getQuestionType(final String questionTypeName) {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<QuestionType> questionTypes = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<QuestionType> typedQuery = entityManager
                    .createQuery("SELECT qt FROM QuestionType qt WHERE name=:name", QuestionType.class);
            typedQuery.setParameter("name", StringEscapeUtils.escapeHtml(questionTypeName));
            return typedQuery.getResultList();
        });
        return questionTypes.get(0);
    }

    private static void setQuestionEntities(final List<QuestionBean> questionBeans, final Protocol protocol) {
        for (final QuestionBean questionBean : questionBeans) {
            final Question question = new Question(questionBean.getNumber(), getQuestionType(questionBean.getType()),
                    StringEscapeUtils.escapeHtml(questionBean.getSpecification()), protocol,
                    StringEscapeUtils.escapeHtml(questionBean.getDecision()));
            TransactionExecuter.mergeEntity(EntityManagerCreator.create(), question);
        }
    }

    @SuppressWarnings("nls")
    private static void setAbsentEntities(final List<String> absentMembers, final List<String> furloughMembers,
            final Protocol protocol) {
        for (final String absentMember : absentMembers) {
            final Absent absent = new Absent(protocol, getPerson(absentMember),
                    StringEscapeUtils.escapeHtml("Отсъства"));
            TransactionExecuter.mergeEntity(EntityManagerCreator.create(), absent);
        }
        for (final String furloughMember : furloughMembers) {
            final Absent absent = new Absent(protocol, getPerson(furloughMember),
                    StringEscapeUtils.escapeHtml("Отпуск"));
            TransactionExecuter.mergeEntity(EntityManagerCreator.create(), absent);
        }
    }

    @SuppressWarnings("nls")
    private static void setOtherAbsentEntities(final List<String> otherAbsentMembers,
            final List<String> otherFurloughMembers, final Protocol protocol) {
        for (final String absentMember : otherAbsentMembers) {
            final Absent absent = new Absent();
            absent.setProtocol(protocol);
            absent.setReason(StringEscapeUtils.escapeHtml("Отсъства"));
            absent.setPersonName(StringEscapeUtils.escapeHtml(absentMember));
            TransactionExecuter.mergeEntity(EntityManagerCreator.create(), absent);
        }
        for (final String furloghMember : otherFurloughMembers) {
            final Absent absent = new Absent();
            absent.setProtocol(protocol);
            absent.setReason(StringEscapeUtils.escapeHtml("Отпуск"));
            absent.setPersonName(StringEscapeUtils.escapeHtml(furloghMember));
            TransactionExecuter.mergeEntity(EntityManagerCreator.create(), absent);
        }
    }
}
