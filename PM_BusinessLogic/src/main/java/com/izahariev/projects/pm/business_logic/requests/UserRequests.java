/**
 * 
 */
package com.izahariev.projects.pm.business_logic.requests;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.izahariev.projects.pm.business_logic_interfaces.requests.UsersRequestsInterface;
import com.izahariev.projects.pm.persistance.entities.User;
import com.izahariev.projects.pm.persistance.transactions.classes.EntityManagerCreator;
import com.izahariev.projects.pm.persistance.transactions.classes.TransactionExecuter;

/**
 * Class implementing the methods used to create requests for the user table in
 * the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
public class UserRequests implements UsersRequestsInterface {

    /**
     * Default constructor.
     */
    public UserRequests() {

    }

    @SuppressWarnings({ "nls", "PMD.CommentRequired" })
    @Override
    public boolean validateUser(final String username, final String password) {

        final EntityManager entityManager = EntityManagerCreator.create();
        final List<User> users = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<User> typedQuery = entityManager
                    .createQuery("SELECT u FROM User u WHERE username=:username AND password=:password", User.class);
            typedQuery.setParameter("username", username);
            typedQuery.setParameter("password", cryptPassword(password));
            return typedQuery.getResultList();
        });

        return !users.isEmpty();
    }

    @SuppressWarnings({ "nls", "PMD.AvoidPrintStackTrace" })
    private static String cryptPassword(final String password) {
        MessageDigest messageDigest;
        final StringBuilder stringBuilder = new StringBuilder();

        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(password.getBytes());
            final byte[] bytes = messageDigest.digest();

            for (final byte byteNum : bytes) {
                stringBuilder.append(Integer.toString((byteNum & 0xff) + 0x100, 16));
            }
        }
        catch (final NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return stringBuilder.toString();
    }
}
