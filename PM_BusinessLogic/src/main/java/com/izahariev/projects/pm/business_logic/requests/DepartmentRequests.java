/**
 * 
 */
package com.izahariev.projects.pm.business_logic.requests;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringEscapeUtils;

import com.izahariev.projects.pm.business_logic_interfaces.requests.DepartmentRequestsInterface;
import com.izahariev.projects.pm.persistance.entities.Department;
import com.izahariev.projects.pm.persistance.transactions.classes.EntityManagerCreator;
import com.izahariev.projects.pm.persistance.transactions.classes.TransactionExecuter;

/**
 * Class providing implementation for the methods in the
 * {@link DepartmentRequestsInterface}.
 * 
 * @author Ivaylo Zahariev
 *
 */
public class DepartmentRequests implements DepartmentRequestsInterface {

    /**
     * Default constructor.
     */
    public DepartmentRequests() {

    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.business_logic_interfaces.requests.
     * DepartmentRequestsInterface#getAllDepartments()
     */
    @SuppressWarnings({ "nls", "PMD.CommentRequired" })
    @Override
    public List<String> getAllDepartments() {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<Department> departments = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<Department> typedQuery = entityManager.createQuery("SELECT d FROM Department d",
                    Department.class);
            return typedQuery.getResultList();
        });

        final List<String> departmentNames = new ArrayList<>();
        for (final Department department : departments) {
            departmentNames.add(StringEscapeUtils.unescapeHtml(department.getName()));
        }

        return departmentNames;
    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.business_logic_interfaces.requests.
     * DepartmentRequestsInterface#getDepartments()
     */
    @SuppressWarnings({ "nls", "PMD.CommentRequired" })
    @Override
    public List<String> getDepartments(final String facultyName) {
        final EntityManager entityManager = EntityManagerCreator.create();
        @SuppressWarnings("PMD.AvoidDuplicateLiterals")
        final List<Department> departments = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<Department> typedQuery = entityManager.createQuery(
                    "SELECT d FROM Department d WHERE d.faculty IN (SELECT f FROM Faculty f WHERE name=:name)",
                    Department.class);
            typedQuery.setParameter("name", StringEscapeUtils.escapeHtml(facultyName));
            return typedQuery.getResultList();
        });

        final List<String> departmentNames = new ArrayList<>();
        for (final Department department : departments) {
            departmentNames.add(StringEscapeUtils.unescapeHtml(department.getName()));
        }

        return departmentNames;
    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.business_logic_interfaces.requests.
     * DepartmentRequestsInterface#getDepartmentsIDs(java.lang.String)
     */
    @SuppressWarnings({ "nls", "PMD.CommentRequired" })
    @Override
    public List<String> getDepartmentsIDs(final String facultyName) {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<Department> departments = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<Department> typedQuery = entityManager.createQuery(
                    "SELECT d FROM Department d WHERE d.faculty IN (SELECT f FROM Faculty f WHERE name=:name)",
                    Department.class);
            typedQuery.setParameter("name", StringEscapeUtils.escapeHtml(facultyName));
            return typedQuery.getResultList();
        });

        final List<String> departmentIDs = new ArrayList<>();
        for (final Department department : departments) {
            departmentIDs.add(Integer.toString(department.getId()));
        }

        return departmentIDs;
    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.business_logic_interfaces.requests.
     * DepartmentRequestsInterface#validateDepartmentFaculty(java.lang.String,
     * java.lang.String)
     */
    @SuppressWarnings({ "nls", "PMD.CommentRequired" })
    @Override
    public boolean validateDepartmentFaculty(final String department, final String faculty) {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<Department> departments = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<Department> typedQuery = entityManager
                    .createQuery("SELECT d FROM Department d WHERE name=:name)", Department.class);
            typedQuery.setParameter("name", StringEscapeUtils.escapeHtml(department));
            return typedQuery.getResultList();
        });
        return StringEscapeUtils.unescapeHtml(departments.get(0).getFaculty().getName()).equals(faculty);
    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.business_logic_interfaces.requests.
     * DepartmentRequestsInterface#getDepartmentDirector(java.lang.String)
     */
    @SuppressWarnings({ "nls", "PMD.CommentRequired" })
    @Override
    public String getDepartmentDirector(final String departmentName) {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<Department> departments = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<Department> typedQuery = entityManager
                    .createQuery("SELECT d FROM Department d WHERE name=:name)", Department.class);
            typedQuery.setParameter("name", StringEscapeUtils.escapeHtml(departmentName));
            return typedQuery.getResultList();
        });
        return StringEscapeUtils.unescapeHtml(departments.get(0).getDirector().getName());
    }
}
