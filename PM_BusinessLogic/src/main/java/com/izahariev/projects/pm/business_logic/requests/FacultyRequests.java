/**
 * 
 */
package com.izahariev.projects.pm.business_logic.requests;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringEscapeUtils;

import com.izahariev.projects.pm.business_logic_interfaces.requests.FacultyRequestsInterface;
import com.izahariev.projects.pm.persistance.entities.Faculty;
import com.izahariev.projects.pm.persistance.transactions.classes.EntityManagerCreator;
import com.izahariev.projects.pm.persistance.transactions.classes.TransactionExecuter;

/**
 * Class providing implementation for the methods in the
 * {@link FacultyRequestsInterface}.
 * 
 * @author Ivaylo Zahariev
 *
 */
public class FacultyRequests implements FacultyRequestsInterface {

    /**
     * Default constructor.
     */
    public FacultyRequests() {

    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.business_logic_interfaces.requests.
     * FacultyRequestsInterface#getFaculties()
     */
    @SuppressWarnings({ "nls", "PMD.CommentRequired" })
    @Override
    public List<String> getAllFaculties() {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<Faculty> faculties = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<Faculty> typedQuery = entityManager.createQuery("SELECT f FROM Faculty f", Faculty.class);
            return typedQuery.getResultList();
        });

        final List<String> facultyNames = new ArrayList<>();
        for (final Faculty faculty : faculties) {
            facultyNames.add(StringEscapeUtils.unescapeHtml(faculty.getName()));
        }

        return facultyNames;
    }

}
