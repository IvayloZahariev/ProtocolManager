/**
 * 
 */
package com.izahariev.projects.pm.business_logic.requests;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.izahariev.projects.pm.business_logic_interfaces.requests.QuestionTypeRequestsInterface;
import com.izahariev.projects.pm.persistance.entities.QuestionType;
import com.izahariev.projects.pm.persistance.transactions.classes.EntityManagerCreator;
import com.izahariev.projects.pm.persistance.transactions.classes.TransactionExecuter;

/**
 * @author Ivaylo Zahariev
 *
 */
public class QuestionTypeRequests implements QuestionTypeRequestsInterface {

    /**
     * Deafult constructor.
     */
    public QuestionTypeRequests() {

    }

    /*
     * (non-Javadoc)
     * @see com.izahariev.projects.pm.business_logic_interfaces.requests.
     * QuestionTypeRequestsInterface#getQuestionTypes()
     */
    @SuppressWarnings({ "nls", "PMD.CommentRequired" })
    @Override
    public List<String> getQuestionTypes() {
        final EntityManager entityManager = EntityManagerCreator.create();
        final List<QuestionType> questionTypes = TransactionExecuter.select(entityManager, () -> {
            final TypedQuery<QuestionType> typedQuery = entityManager.createQuery("SELECT qt FROM QuestionType qt",
                    QuestionType.class);
            return typedQuery.getResultList();
        });

        final List<String> questionTypesNames = new ArrayList<>();
        for (final QuestionType questionType : questionTypes) {
            questionTypesNames.add(questionType.getName());
        }

        return questionTypesNames;
    }

}
