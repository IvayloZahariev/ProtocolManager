/**
 * 
 */
package com.izahariev.projects.pm.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Class used to create question beans.
 * 
 * @author Ivaylo Zahariev
 *
 */
public class QuestionBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6333990121407544484L;

    private float number;
    private String type;
    private String specification;
    private String decision;
    private List<CommentBean> comments;
    private List<VoteBean> votes;

    /**
     * No argument constructor.
     */
    public QuestionBean() {

    }

    /**
     * Gets the value of the number property.
     * 
     * @return The value of the number property.
     */
    public float getNumber() {
        return number;
    }

    /**
     * Sets a new value for the number property.
     * 
     * @param number
     *            The new value of the number property.
     */
    public void setNumber(final float number) {
        this.number = number;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return The value of the type property.
     */
    public String getType() {
        return type;
    }

    /**
     * Sets a new value for the type property.
     * 
     * @param type
     *            The new value of the type property.
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * Gets the value of the specification property.
     * 
     * @return The value of the specification property.
     */
    public String getSpecification() {
        return specification;
    }

    /**
     * Sets a new value for the specification property.
     * 
     * @param specification
     *            The new value of the specification property.
     */
    public void setSpecification(final String specification) {
        this.specification = specification;
    }

    /**
     * Gets the value of the decision property.
     * 
     * @return The value of the decision property.
     */
    public String getDecision() {
        return decision;
    }

    /**
     * Sets a new value for the decision property.
     * 
     * @param decision
     *            The new value of the decision property.
     */
    public void setDecision(final String decision) {
        this.decision = decision;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return The value of the comments property.
     */
    public List<CommentBean> getComments() {
        return comments;
    }

    /**
     * Sets a new value for the comments property.
     * 
     * @param comments
     *            The new value of the comments property.
     */
    public void setComments(final List<CommentBean> comments) {
        this.comments = comments;
    }

    /**
     * Gets the value of the votes property.
     * 
     * @return The value of the votes property.
     */
    public List<VoteBean> getVotes() {
        return votes;
    }

    /**
     * Sets a new value for the votes property.
     * 
     * @param votes
     *            The new value of the votes property.
     */
    public void setVotes(final List<VoteBean> votes) {
        this.votes = votes;
    }

    /**
     * Gets the number of the question. If the number is an integer removes the
     * .0 from it.
     * 
     * @return The number of the question.
     */
    public String getNumberForDisplay() {
        if (number % 1 == 0) {
            return Integer.toString((int) number);
        }
        return Float.toString(number);
    }

}
