/**
 * 
 */
package com.izahariev.projects.pm.beans;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Class used to create comment beans.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings("PMD.TooManyFields")
public class ProtocolBean {

    private String faculty;
    private String department;
    private int number;
    private Date councilDate;
    private String reporter;
    private String departmentDirector;
    private String type;
    private int allMembers;
    private int absent;
    private List<String> absentMembersNames;
    private List<String> otherAbsentMembersNames;
    private int furlough;
    private List<String> furloughMembersNames;
    private List<String> otherFurloughMembersNames;
    private int reducedMembers;
    private int presentMembers;
    private int quorum;
    private List<QuestionBean> questions;
    private List<String> agenda;

    /**
     * Default constructor.
     */
    public ProtocolBean() {

    }

    /**
     * Gets the value of the faculty property.
     * 
     * @return The value of the faculty property.
     */
    public String getFaculty() {
        return faculty;
    }

    /**
     * Sets a new value for the faculty property.
     * 
     * @param faculty
     *            The new value of the faculty property.
     */
    public void setFaculty(final String faculty) {
        this.faculty = faculty;
    }

    /**
     * Gets the value of the department property.
     * 
     * @return The value of the department property.
     */
    public String getDepartment() {
        return department;
    }

    /**
     * Sets a new value for the department property.
     * 
     * @param department
     *            The new value of the department property.
     */
    public void setDepartment(final String department) {
        this.department = department;
    }

    /**
     * Gets the value of the number property.
     * 
     * @return The value of the number property.
     */
    public int getNumber() {
        return number;
    }

    /**
     * Sets a new value for the number property.
     * 
     * @param number
     *            The new value of the number property.
     */
    public void setNumber(final int number) {
        this.number = number;
    }

    /**
     * Gets the value of the councilDate property.
     * 
     * @return The value of the councilDate property.
     */
    public Date getCouncilDate() {
        return councilDate;
    }

    /**
     * Sets a new value for the councilDate property.
     * 
     * @param councilDate
     *            The new value of the councilDate property.
     */
    public void setCouncilDate(final Date councilDate) {
        this.councilDate = councilDate;
    }

    /**
     * Gets the value of the reporter property.
     * 
     * @return The value of the reporter property.
     */
    public String getReporter() {
        return reporter;
    }

    /**
     * Sets a new value for the reporter property.
     * 
     * @param reporter
     *            The new value of the reporter property.
     */
    public void setReporter(final String reporter) {
        this.reporter = reporter;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return The value of the type property.
     */
    public String getType() {
        return type;
    }

    /**
     * Sets a new value for the type property.
     * 
     * @param type
     *            The new value of the type property.
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * Gets the value of the absent property.
     * 
     * @return The value of the absent property.
     */
    public int getAbsent() {
        return absent;
    }

    /**
     * Sets a new value for the absent property.
     * 
     * @param absent
     *            The new value of the absent property.
     */
    public void setAbsent(final int absent) {
        this.absent = absent;
    }

    /**
     * Gets the value of the absentMembersNames property.
     * 
     * @return The value of the absentMembersNames property.
     */
    public List<String> getAbsentMembersNames() {
        return absentMembersNames;
    }

    /**
     * Sets a new value for the absentMembersNames property.
     * 
     * @param absentMembersNames
     *            The new value of the absentMembersNames property.
     */
    public void setAbsentMembersNames(final List<String> absentMembersNames) {
        this.absentMembersNames = absentMembersNames;
    }

    /**
     * Gets the value of the furlough property.
     * 
     * @return The value of the furlough property.
     */
    public int getFurlough() {
        return furlough;
    }

    /**
     * Sets a new value for the furlough property.
     * 
     * @param furlough
     *            The new value of the furlough property.
     */
    public void setFurlough(final int furlough) {
        this.furlough = furlough;
    }

    /**
     * Gets the value of the furloughMembersNames property.
     * 
     * @return The value of the furloughMembersNames property.
     */
    public List<String> getFurloughMembersNames() {
        return furloughMembersNames;
    }

    /**
     * Sets a new value for the furloughMembersNames property.
     * 
     * @param furloughMembersNames
     *            The new value of the furloughMembersNames property.
     */
    public void setFurloughMembersNames(final List<String> furloughMembersNames) {
        this.furloughMembersNames = furloughMembersNames;
    }

    /**
     * Gets the value of the questions property.
     * 
     * @return The value of the questions property.
     */
    public List<QuestionBean> getQuestions() {
        return questions;
    }

    /**
     * Sets a new value for the questions property.
     * 
     * @param questions
     *            The new value of the questions property.
     */
    public void setQuestions(final List<QuestionBean> questions) {
        this.questions = questions;
    }

    /**
     * Gets the day, month and year of the council date.
     * 
     * @return String containing the council date in format dd/mm/yyyy.
     */
    @SuppressWarnings("nls")
    public String getCouncilDateForDisplay() {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(councilDate);
        final int month = cal.get(Calendar.MONTH) + 1;
        final int day = cal.get(Calendar.DAY_OF_MONTH);
        final int year = cal.get(Calendar.YEAR);

        return day + "/" + month + "/" + year;
    }

    /**
     * Creates a string date in format yyyy-mm-dd to be used in initializing an
     * HTML date field.
     * 
     * @return The created date string.
     */
    @SuppressWarnings({ "nls", "PMD.AvoidLiteralsInIfCondition", "PMD.UseStringBufferForStringAppends" })
    public String getCouncilDateForDateField() {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(councilDate);
        final int month = cal.get(Calendar.MONTH) + 1;
        final int day = cal.get(Calendar.DAY_OF_MONTH);
        final int year = cal.get(Calendar.YEAR);
        String monthString = Integer.toString(month);
        String dayString = Integer.toString(day);

        if (month < 10) {
            monthString = 0 + monthString;
        }
        if (day < 10) {
            dayString = 0 + dayString;
        }

        return year + "-" + monthString + "-" + dayString;
    }

    /**
     * Gets the value of the allMembers property.
     * 
     * @return The value of the allMembers property.
     */
    public int getAllMembers() {
        return allMembers;
    }

    /**
     * Sets a new value for the allMembers property.
     * 
     * @param allMembers
     *            The new value of the allMembers property.
     */
    public void setAllMembers(final int allMembers) {
        this.allMembers = allMembers;
    }

    /**
     * Gets the value of the reducedMembers property.
     * 
     * @return The value of the reducedMembers property.
     */
    public int getReducedMembers() {
        return reducedMembers;
    }

    /**
     * Sets a new value for the reducedMembers property.
     * 
     * @param reducedMembers
     *            The new value of the reducedMembers property.
     */
    public void setReducedMembers(final int reducedMembers) {
        this.reducedMembers = reducedMembers;
    }

    /**
     * Gets the value of the presentMembers property.
     * 
     * @return The value of the presentMembers property.
     */
    public int getPresentMembers() {
        return presentMembers;
    }

    /**
     * Sets a new value for the presentMembers property.
     * 
     * @param presentMembers
     *            The new value of the presentMembers property.
     */
    public void setPresentMembers(final int presentMembers) {
        this.presentMembers = presentMembers;
    }

    /**
     * Gets the value of the quorum property.
     * 
     * @return The value of the quorum property.
     */
    public int getQuorum() {
        return quorum;
    }

    /**
     * Sets a new value for the quorum property.
     * 
     * @param quorum
     *            The new value of the quorum property.
     */
    public void setQuorum(final int quorum) {
        this.quorum = quorum;
    }

    /**
     * Gets the value of the otherAbsentMembersNames property.
     * 
     * @return The value of the otherAbsentMembersNames property.
     */
    public List<String> getOtherAbsentMembersNames() {
        return otherAbsentMembersNames;
    }

    /**
     * Sets a new value for the otherAbsentMembersNames property.
     * 
     * @param otherAbsentMembersNames
     *            The new value of the otherAbsentMembersNames property.
     */
    public void setOtherAbsentMembersNames(final List<String> otherAbsentMembersNames) {
        this.otherAbsentMembersNames = otherAbsentMembersNames;
    }

    /**
     * Gets the value of the otherFurloughMembersNames property.
     * 
     * @return The value of the otherFurloughMembersNames property.
     */
    public List<String> getOtherFurloughMembersNames() {
        return otherFurloughMembersNames;
    }

    /**
     * Sets a new value for the otherFurloughMembersNames property.
     * 
     * @param otherFurloughMembersNames
     *            The new value of the otherFurloughMembersNames property.
     */
    public void setOtherFurloughMembersNames(final List<String> otherFurloughMembersNames) {
        this.otherFurloughMembersNames = otherFurloughMembersNames;
    }

    /**
     * Gets the value of the agenda property.
     * 
     * @return The value of the agenda property.
     */
    public List<String> getAgenda() {
        return agenda;
    }

    /**
     * Sets a new value for the agenda property.
     * 
     * @param agenda
     *            The new value of the agenda property.
     */
    public void setAgenda(final List<String> agenda) {
        this.agenda = agenda;
    }

    /**
     * Gets the value of the departmentDirector property.
     * 
     * @return The value of the departmentDirector property.
     */
    public String getDepartmentDirector() {
        return departmentDirector;
    }

    /**
     * Sets a new value for the departmentDirector property.
     * 
     * @param departmentDirector
     *            The new value of the departmentDirector property.
     */
    public void setDepartmentDirector(final String departmentDirector) {
        this.departmentDirector = departmentDirector;
    }

}
