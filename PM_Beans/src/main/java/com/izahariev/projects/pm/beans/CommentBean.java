/**
 * 
 */
package com.izahariev.projects.pm.beans;

import java.io.Serializable;

/**
 * Class used to create comment beans.
 * 
 * @author Ivaylo Zahariev
 *
 */
public class CommentBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1513859280793727256L;

    private int number;
    private float questionNumber;
    private String owner;
    private String specification;

    /**
     * No arguments constructor.
     */
    public CommentBean() {

    }

    /**
     * Gets the value of the number property.
     * 
     * @return The value of the number property.
     */
    public int getNumber() {
        return number;
    }

    /**
     * Sets a new value for the number property.
     * 
     * @param number
     *            The new value of the number property.
     */
    public void setNumber(final int number) {
        this.number = number;
    }

    /**
     * Gets the value of the questionNumber property.
     * 
     * @return The value of the questionNumber property.
     */
    public float getQuestionNumber() {
        return questionNumber;
    }

    /**
     * Sets a new value for the questionNumber property.
     * 
     * @param questionNumbet
     *            The new value of the questionNumber property.
     */
    public void setQuestionNumber(final float questionNumber) {
        this.questionNumber = questionNumber;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return The value of the owner property.
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Sets a new value for the owner property.
     * 
     * @param owner
     *            The new value of the owner property.
     */
    public void setOwner(final String owner) {
        this.owner = owner;
    }

    /**
     * Gets the value of the specification property.
     * 
     * @return The value of the specification property.
     */
    public String getSpecification() {
        return specification;
    }

    /**
     * Sets a new value for the specification property.
     * 
     * @param specification
     *            The new value of the specification property.
     */
    public void setSpecification(final String specification) {
        this.specification = specification;
    }

    /**
     * Gets the number of the question. If the number is an integer removes the
     * .0 from it.
     * 
     * @return The number of the question.
     */
    public String getQuestionNumberForDisplay() {
        if (questionNumber % 1 == 0) {
            return Integer.toString((int) questionNumber);
        }
        return Float.toString(questionNumber);
    }
}
