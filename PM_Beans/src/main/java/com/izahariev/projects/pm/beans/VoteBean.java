/**
 * 
 */
package com.izahariev.projects.pm.beans;

import java.io.Serializable;

/**
 * Class used to create vote beans.
 * 
 * @author Ivaylo Zahariev
 *
 */
public class VoteBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -189466224167439155L;

    private int voteNumber;
    private float questionNumber;
    private String voteSubject;
    private String voteType;
    private int numYesVotes;
    private int numNoVotes;
    private int numBlankVotes;
    private String moderator;
    private String member1;
    private String member2;

    /**
     * No arguments constructor.
     */
    public VoteBean() {

    }

    /**
     * Gets the value of the voteNumber property.
     * 
     * @return The value of the voteNumber property.
     */
    public int getVoteNumber() {
        return voteNumber;
    }

    /**
     * Sets a new value for the voteNumber property.
     * 
     * @param voteNumber
     *            The new value of the voteNumber property.
     */
    public void setVoteNumber(final int voteNumber) {
        this.voteNumber = voteNumber;
    }

    /**
     * Gets the value of the questionNumber property.
     * 
     * @return The value of the questionNumber property.
     */
    public float getQuestionNumber() {
        return questionNumber;
    }

    /**
     * Sets a new value for the questionNumber property.
     * 
     * @param questionNumber
     *            The new value of the questionNumber property.
     */
    public void setQuestionNumber(final float questionNumber) {
        this.questionNumber = questionNumber;
    }

    /**
     * Gets the value of the voteSubject property.
     * 
     * @return The value of the voteSubject property.
     */
    public String getVoteSubject() {
        return voteSubject;
    }

    /**
     * Sets a new value for the voteSubject property.
     * 
     * @param voteSubject
     *            The new value of the voteSubject property.
     */
    public void setVoteSubject(final String voteSubject) {
        this.voteSubject = voteSubject;
    }

    /**
     * Gets the value of the voteType property.
     * 
     * @return The value of the voteType property.
     */
    public String getVoteType() {
        return voteType;
    }

    /**
     * Sets a new value for the voteType property.
     * 
     * @param voteType
     *            The new value of the voteType property.
     */
    public void setVoteType(final String voteType) {
        this.voteType = voteType;
    }

    /**
     * Gets the value of the numYesVotes property.
     * 
     * @return The value of the numYesVotes property.
     */
    public int getNumYesVotes() {
        return numYesVotes;
    }

    /**
     * Sets a new value for the numYesVotes property.
     * 
     * @param numYesVotes
     *            The new value of the numYesVotes property.
     */
    public void setNumYesVotes(final int numYesVotes) {
        this.numYesVotes = numYesVotes;
    }

    /**
     * Gets the value of the numNoVotes property.
     * 
     * @return The value of the numNoVotes property.
     */
    public int getNumNoVotes() {
        return numNoVotes;
    }

    /**
     * Sets a new value for the numNoVotes property.
     * 
     * @param numNoVotes
     *            The new value of the numNoVotes property.
     */
    public void setNumNoVotes(final int numNoVotes) {
        this.numNoVotes = numNoVotes;
    }

    /**
     * Gets the value of the numBlankVotes property.
     * 
     * @return The value of the numBlankVotes property.
     */
    public int getNumBlankVotes() {
        return numBlankVotes;
    }

    /**
     * Sets a new value for the numBlankVotes property.
     * 
     * @param numBlankVotes
     *            The new value of the numBlankVotes property.
     */
    public void setNumBlankVotes(final int numBlankVotes) {
        this.numBlankVotes = numBlankVotes;
    }

    /**
     * Gets the value of the moderator property.
     * 
     * @return The value of the moderator property.
     */
    public String getModerator() {
        return moderator;
    }

    /**
     * Sets a new value for the moderator property.
     * 
     * @param moderator
     *            The new value of the moderator property.
     */
    public void setModerator(final String moderator) {
        this.moderator = moderator;
    }

    /**
     * Gets the value of the member1 property.
     * 
     * @return The value of the member1 property.
     */
    public String getMember1() {
        return member1;
    }

    /**
     * Sets a new value for the member1 property.
     * 
     * @param member1
     *            The new value of the member1 property.
     */
    public void setMember1(final String member1) {
        this.member1 = member1;
    }

    /**
     * Gets the value of the member2 property.
     * 
     * @return The value of the member2 property.
     */
    public String getMember2() {
        return member2;
    }

    /**
     * Sets a new value for the member2 property.
     * 
     * @param member2
     *            The new value of the member2 property.
     */
    public void setMember2(final String member2) {
        this.member2 = member2;
    }

    /**
     * Gets the number of the question. If the number is an integer removes the
     * .0 from it.
     * 
     * @return The number of the question.
     */
    public String getQuestionNumberForDisplay() {
        if (questionNumber % 1 == 0) {
            return Integer.toString((int) questionNumber);
        }
        return Float.toString(questionNumber);
    }

}
