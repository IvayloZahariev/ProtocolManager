/**
 * 
 */
package com.izahariev.projects.pm.business_logic_interfaces.requests;

import java.util.List;

import com.izahariev.projects.pm.beans.ProtocolBean;

/**
 * Interface defining the methods for the different kinds of requests on the
 * protocols in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
public interface ProtocolRequestsInterface {

    /**
     * Gets all protocols with the given parameters.
     * 
     * @param protocolType
     *            The type of the protocol.
     * @param protocolNumber
     *            The number of the protocol.
     * @param councilDate
     *            The date of the council meeting.
     * @param faculty
     *            The protocol faculty.
     * @param department
     *            The protocol department.
     * @param reporter
     *            The protocol reporter.
     * @param questionSpecification
     *            Parts of the specification of one or more questions in the
     *            required protocols.
     * @param questionDecision
     *            Parts of the decision of one or more questions in the required
     *            protocols.
     * @return List of protocol beans with the protocols meeting the given
     *         requirements.
     */
    List<ProtocolBean> getProtocols(final String protocolType, final String protocolNumber, final String councilDate,
            final String faculty, final String department, final String reporter, String questionSpecification,
            String questionDecision);

    /**
     * Creates a protocol entity with the data from the protocol bean and
     * inserts it in the database.
     * 
     * @param protocolBean
     *            The protocol bean which contains the data for the protocol
     *            entity.
     */
    void addProtocol(ProtocolBean protocolBean);

    /**
     * Updates the old protocol fields with the new protocol fields values.
     * 
     * @param oldBean
     *            Bean containing the old field values.
     * @param newBean
     *            Bean containing the new field values.
     */
    void updateProtocol(ProtocolBean oldBean, ProtocolBean newBean);

    /**
     * Deletes the protocol which is represented by the given protocol bean.
     * 
     * @param bean
     *            The protocol which corresponding entity will be removed from
     *            the database.
     */
    void deleteProtocol(ProtocolBean bean);
}
