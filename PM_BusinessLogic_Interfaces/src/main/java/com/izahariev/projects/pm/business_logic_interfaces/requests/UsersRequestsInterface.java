/**
 * 
 */
package com.izahariev.projects.pm.business_logic_interfaces.requests;

/**
 * Interface defining the methods for the different kinds of requests on the
 * users in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
public interface UsersRequestsInterface {

    /**
     * Method used to validate that a user with the given username and password
     * exists in the database.
     * 
     * @param username
     *            The username of the user.
     * @param password
     *            The password of the user
     * @return True if there is such user in the database, otherwise false.
     */
    boolean validateUser(String username, String password);

}
