/**
 * 
 */
package com.izahariev.projects.pm.business_logic_interfaces.requests;

import java.util.List;

/**
 * Interface defining the methods for the different kinds of requests on the
 * faculties in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
public interface FacultyRequestsInterface {

    /**
     * Gets all faculties in the database.
     * 
     * @return List containing the names of all faculties in the database.
     */
    List<String> getAllFaculties();
}
