/**
 * 
 */
package com.izahariev.projects.pm.business_logic_interfaces.requests;

import java.util.List;

/**
 * Interface defining the methods for the different kinds of requests on the
 * teaching stuff in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
public interface TeachingStuffRequestsInterface {

    /**
     * Validates that a teaching stuff member with the given name exists in the
     * database.
     * 
     * @param name
     *            The name of the teaching stuff member.
     * @return True if the person exists, otherwise false.
     */
    boolean vaildatePerson(String name);

    /**
     * Gets all members of the teaching stuff who are members of the academic
     * council.
     * 
     * @return List containing the names of the academic council members.
     */
    List<String> getAcademicCouncilMembers();

    /**
     * Gets the names of all people on the teaching stuff who are not members of
     * the academic council.
     * 
     * @return List containing the names of all people who are not part of the
     *         academic council.
     */
    List<String> getNonAcademicCouncilMembers();
}
