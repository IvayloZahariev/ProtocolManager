/**
 * 
 */
package com.izahariev.projects.pm.business_logic_interfaces.requests;

import java.util.List;

/**
 * Interface defining the methods for the different kinds of requests on the
 * departments in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
public interface DepartmentRequestsInterface {

    /**
     * Gets all departments in the database.
     * 
     * @return List containing the names of all departments in the database.
     */
    List<String> getAllDepartments();

    /**
     * Gets all departments in the database from the given faculty.
     * 
     * @param facultyName
     *            The name of the faculty which departments are required.
     * @return List containing the names of all departments in the database from
     *         the given faculty.
     */
    List<String> getDepartments(String facultyName);

    /**
     * Gets the IDs of all departments in the database from the given faculty.
     * 
     * @param facultyName
     *            The name of the faculty which departments are required.
     * @return List containing the IDs of all departments in the database from
     *         the given faculty.
     */
    List<String> getDepartmentsIDs(String facultyName);

    /**
     * Validates that the given departments is part of the given faculty.
     * 
     * @param department
     *            The name of the department.
     * @param faculty
     *            The name of the faculty.
     * @return True if the department is part of the faculty, otherwise false.
     */
    boolean validateDepartmentFaculty(String department, String faculty);

    /**
     * Gets the name of the director of the given department.
     * 
     * @param departmentName
     *            The name of the department which director is needed.
     * @return The name of the director.
     */
    String getDepartmentDirector(String departmentName);
}
