/**
 * 
 */
package com.izahariev.projects.pm.business_logic_interfaces.requests;

import java.util.List;

/**
 * Interface defining the methods for the different kinds of requests for the
 * question types in the database.
 * 
 * @author Ivaylo Zahariev
 *
 */
public interface QuestionTypeRequestsInterface {
    /**
     * Gets all question types from the database.
     * 
     * @return List containing the names of all question types in the database.
     */
    List<String> getQuestionTypes();
}
