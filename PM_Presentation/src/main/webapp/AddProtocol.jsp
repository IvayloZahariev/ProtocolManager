<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<style>
		table {
    		font-family: arial, sans-serif;
    		border-collapse: collapse;
    		width: 100%;
		}
		td, th {
    		border: 1px solid #dddddd;
    		text-align: left;
    		padding: 8px;
		}
	</style>
	<meta http-equiv="Content-Type">
	<title>Add Protocol</title>
</head>
<body>
	<form action="addProtocol" method="post">
		Тип: <input type="radio" name="protocolType" value="Факултетен"
		<c:if test="${checkedRadioButton == 'Факултетен'}">checked</c:if>>Факултетен съвет 
		<input type="radio" name="protocolType" value="Катедрен"
		<c:if test="${checkedRadioButton == 'Катедрен'}">checked</c:if>>Катедрен съвет<br><br>
		<c:if test="${protocolTypeErrorMessage != null}"><font color="red">${protocolTypeErrorMessage}<br><br></font></c:if>
        Номер: <input type="text" name="protocolNumber" value="${protocolNumber}"><br><br>
        <c:if test="${protocolNumberErrorMessage != null}"><font color="red">${protocolNumberErrorMessage}<br><br></font></c:if>
        Факултет: <select name="selectedFaculty">
        	<option value="default">Select faculty</option>
        	<c:forEach var="faculty" items="${facultyList}">
        		<option value="${faculty}"
        		<c:if test="${faculty == selectedFacultyOption}">selected</c:if>>${faculty}</option>
        	</c:forEach>
        </select>
        <input type="submit" name="action" value="Покажи катедри"><br><br>
        <c:if test="${facultyErrorMessage != null}"><font color="red">${facultyErrorMessage}</font><br><br></c:if>
        <c:if test="${isSetFaculty}">Катедри: <select name="selectedDepartment">
        	<option value="default">Select department</option>
        	<c:forEach var="department" items="${departmentList}">
        		<option value="${department}"
        		<c:if test="${department == selectedDepartmentOption}">selected</c:if>>${department}</option>
        	</c:forEach>
        </select><br><br></c:if>
        <c:if test="${departmentErrorMessage != null}"><font color="red">${departmentErrorMessage}<br><br></font></c:if>
        Дата на провеждане на съвета: <input type="date" name="councilDate" value="${councilDate}"><br><br>
        <c:if test="${dateErrorMessage != null}"><font color="red">${dateErrorMessage}<br><br></font></c:if>
        Протоколчик: <select name="selectedReporter">
        	<option value="default">Select reporter</option>
        	<c:forEach var="name" items="${teachingStuff}">
        		<option value="${name}"
        		<c:if test="${name == selectedReporterOption}">selected</c:if>>${name}</option>
        	</c:forEach>
        </select><br><br>
        <c:if test="${reporterErrorMessage != null}"><font color="red">${reporterErrorMessage}<br><br></font></c:if>
        Академичен състав: <input type="text" name="allMembers" value="${allMembers}"><br><br>
        <c:if test="${membersNumberErrorMessage != null}"><font color="red">${membersNumberErrorMessage}<br><br></font></c:if>
        Отсъстващи
        <table>
        	<tr>
    			<th>Име</th>
    			<th>Отсъства</th>
    			<th>Отпуск</th>
  			</tr>
  			<c:forEach var="name" items="${councilMembers}" varStatus="status">
                <tr>
                    <td>${name}</td>
                    <td><input type="checkbox" name="absent" value="${status.index}"
                    <c:if test="${checkedAbsent[status.index]}">checked</c:if>></td>
                    <td><input type="checkbox" name="furlough" value="${status.index}"
                    <c:if test="${checkedFurlough[status.index]}">checked</c:if>></td>
                </tr>
            </c:forEach> 
  		</table><br><br>
        <c:if test="${addOtherAbsent == null}"><input type="submit" name="action" value="Добави други отсъстващи"><br><br></c:if>
        <c:if test="${addOtherAbsent != null}">
        	Име: <input type="text" name="otherAbsentName"><br><br>
        	<c:if test="${addAbsentError != null}"><font color="red">${addAbsentError}<br><br></font></c:if>
        	<input type="submit" name="action" value="Добави отсъстващ"><br><br>
        	Други отсъстващи<br><br>
        	<table>
        		<tr>
    				<th>Име</th>
    				<th>Отсъства</th>
    				<th>Отпуск</th>
  				</tr>
  				<c:forEach var="name" items="${otherAbsentList}" varStatus="status">
        	        <tr>
        	            <td>${name}</td>
        	            <td><input type="checkbox" name="otherAbsent" value="${status.index}"
                    	<c:if test="${checkedOtherAbsent[status.index]}">checked</c:if>></td>
                    	<td><input type="checkbox" name="otherFurlough" value="${status.index}"
                    	<c:if test="${checkedOtherFurlough[status.index]}">checked</c:if>></td>
        	        </tr>
        	    </c:forEach> 
        	</table><br><br>
        </c:if>
        <c:if test="${addProtocolError != null}"><font color="red">${addProtocolError}<br><br></font></c:if>
        <input type="submit" name="action" value="Напред">
        <input style="float: right;" type="submit" name="action" value="Главно меню">
    </form>
</body>
</html>