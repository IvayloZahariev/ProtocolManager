<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<style>
		table {
    		font-family: arial, sans-serif;
    		border-collapse: collapse;
    		width: 100%;
		}
		td, th {
    		border: 1px solid #dddddd;
    		text-align: left;
    		padding: 8px;
		}
	</style>
	<meta http-equiv="Content-Type">
	<title>Add questions</title>
</head>
<body>
	<form action="addQuestions" method="post">
		<b>Данни за въпроси</b><br><br>
		Номер: <input type="text" name="questionNumber"><br><br>
        <c:if test="${questionNumberErrorMessage != null}"><font color="red">${questionNumberErrorMessage}<br><br></font></c:if>
		Тип:
		<select name="selectedQuestionType">
        	<option value="default">Select question type</option>
        	<c:forEach var="questionType" items="${questionTypesList}">
        		<option value="${questionType}"
        		<c:if test="${questionType == selectedQuestionType}">selected</c:if>>${questionType}</option>
        	</c:forEach>
        </select><br><br>
        <c:if test="${questionTypeErrorMessage != null}"><font color="red">${questionTypeErrorMessage}<br><br></font></c:if>
		Описание:<br><br>
		<textarea rows="4" cols="50" name="questionSpecification">${questionSpecification}</textarea><br><br>
		<c:if test="${questionSpecificationErrorMessage != null}"><font color="red">${questionSpecificationErrorMessage}<br><br></font></c:if>
		Взето решение:<br><br>
		<textarea rows="4" cols="50" name="questionDecision">${questionDecision}</textarea><br><br>
		<c:if test="${questionDecisionErrorMessage != null}"><font color="red">${questionDecisionErrorMessage}<br><br></font></c:if>
		<input type="submit" name="action" value="Добави въпрос"><br><br>
		Въпроси:
        <table>
        <tr>
    		<th>Номер</th>
    		<th>Тип</th>
    		<th>Описание</th>
    		<th>Решение</th>
  		</tr>
  		<c:forEach var="questionBean" items="${questionBeansList}" varStatus="status">
               <tr>
                   <td>${questionBean.getNumberForDisplay()}</td>
                   <td>${questionBean.getType()}</td>
                   <td>${questionBean.getSpecification()}</td>
                   <td>${questionBean.getDecision()}</td>
               </tr>
           </c:forEach> 
  		</table><br><br>
  		Номер на въпрос: <input type="text" name="removeQuestionNumber"><br><br>
  		<c:if test="${removeQuestionErrorMessage != null}"><font color="red">${removeQuestionErrorMessage}<br><br></font></c:if>
  		<input type="submit" name="action" value="Премахни въпрос"><br><br>
  		<b>Данни за коментари</b><br><br>
  		Номер на въпрос: <input type="text" name="commentQuestionNumber" value="${commentQuestionNumber}"><br><br>
  		<c:if test="${commentQuestionNumberErrorMessage != null}"><font color="red">${commentQuestionNumberErrorMessage}<br><br></font></c:if>
		Собственик на коментара: 
		<select name="selectedCommentOwner">
        	<option value="default">Select comment owner</option>
        	<c:forEach var="name" items="${councilMembers}">
        		<option value="${name}"
        		<c:if test="${name == selctedCommentOwner}">selected</c:if>>${name}</option>
        	</c:forEach>
        </select><br><br>
        <c:if test="${commentOwnerErrorMessage != null}"><font color="red">${commentOwnerErrorMessage}<br><br></font></c:if>
        Other comment owner	name: <input type="text" name="otherCommentOwnerName"><br><br>
        <c:if test="${otherCommentOwnerError != null}"><font color="red">${otherCommentOwnerError}<br><br></font></c:if>
        Коментар: <br><br>
        <textarea rows="4" cols="50" name="commentSpecification">${comentSpecification}</textarea><br><br>
        <c:if test="${commentSpecificationErrorMessage != null}"><font color="red">${commentSpecificationErrorMessage}<br><br></font></c:if>
        <input type="submit" name="action" value="Добави коментар"><br><br>
  		Коментари:<br><br>
        <table>
        <tr>
    		<th>Номер</th>
    		<th>Номер на въпрос</th>
    		<th>Собственик на коментара</th>
    		<th>Коментар</th>
  		</tr>
  		<c:forEach var="commentBean" items="${commentBeansList}" varStatus="status">
               <tr>
                   <td>${commentBean.getNumber()}</td>
                   <td>${commentBean.getQuestionNumberForDisplay()}</td>
                   <td>${commentBean.getOwner()}</td>
                   <td>${commentBean.getSpecification()}</td>
               </tr>
           </c:forEach> 
  		</table><br><br>
  		Номер на коментар: <input type="text" name="removeCommentNumber"><br><br>
  		<c:if test="${removeCommentErrorMessage != null}"><font color="red">${removeCommentErrorMessage}<br><br></font></c:if>
  		<input type="submit" name="action" value="Премахни коментар"><br><br>
  		<b>Гласувания</b><br><br>
  		Номер на въпрос: <input type="text" name="voteQuestionNumber" value="${voteQuestionNumber}"><br><br>
  		<c:if test="${addVoteQuestionNumberErrorMessage != null}"><font color="red">${addVoteQuestionNumberErrorMessage}<br><br></font></c:if>
		Предмет на гласуването:<br><br>
		<textarea rows="4" cols="50" name="voteSubject">${voteSubject}</textarea><br><br>
		<c:if test="${voteSubjectErrorMessage != null}"><font color="red">${voteSubjectErrorMessage}<br><br></font></c:if>
		Тип на гласуването:<br><br>
		<input type="radio" name="voteType" value="Тайно"
		<c:if test="${selectedVoteType == 'Тайно'}">checked</c:if>>Тайно гласуване
		<input type="radio" name="voteType" value="Открито"
		<c:if test="${selectedVoteType == 'Открито'}">checked</c:if>>Открито гласуване<br><br>
		<c:if test="${voteTypeErrorMessage != null}"><font color="red">${voteTypeErrorMessage}<br><br></font></c:if>
		Брой ДА гласове: <input type="text" name="yesVotes" value="${numberYesVotes}"><br><br>
		<c:if test="${yesVotesErrorMessage != null}"><font color="red">${yesVotesErrorMessage}<br><br></font></c:if>
		Брой НЕ гласове: <input type="text" name="noVotes" value="${numberNoVotes}"><br><br>
		<c:if test="${noVotesErrorMessage != null}"><font color="red">${noVotesErrorMessage}<br><br></font></c:if>
		Брой БЕЛИ гласове: <input type="text" name="blankVotes" value="${numberBlankVotes}"><br><br>
		<c:if test="${blankVotesErrorMessage != null}"><font color="red">${blankVotesErrorMessage}<br><br></font></c:if>
		Председател: <select name="selectedVoteModerator">
        	<option value="default">Select comment owner</option>
        	<c:forEach var="name" items="${councilMembers}">
        		<option value="${name}"
        		<c:if test="${name == selectedVoteModerator}">selected</c:if>>${name}</option>
        	</c:forEach>
        </select><br><br>
        <!-- Other moderator	name: <input type="text" name="otherModeratorName" value="${otherModeratorName}"><br><br>
        <c:if test="${otherModeratorError != null}"><font color="red">${otherModeratorError}<br><br></font></c:if> -->
		Член 1: <select name="selectedVoteMember1">
        	<option value="default">Select comment owner</option>
        	<c:forEach var="name" items="${councilMembers}">
        		<option value="${name}"
        		<c:if test="${name == selectedVoteMember1}">selected</c:if>>${name}</option>
        	</c:forEach>
        </select><br><br>
        <!-- Other member 1 name: <input type="text" name="otherMember1Name" value="${otherMember1Name}"><br><br>
        <c:if test="${otherMember1Error != null}"><font color="red">${otherMember1Error}<br><br></font></c:if> -->
		Член 2: <select name="selectedVoteMember2">
        	<option value="default">Select comment owner</option>
        	<c:forEach var="name" items="${councilMembers}">
        		<option value="${name}"
        		<c:if test="${name == selectedVoteMember2}">selected</c:if>>${name}</option>
        	</c:forEach>
        </select><br><br>
        <!-- Other member 2 name: <input type="text" name="otherMember2Name" value="${otherMember2Name}"><br><br>
        <c:if test="${otherMember2Error != null}"><font color="red">${otherMember2Error}<br><br></font></c:if> -->
		<input type="submit" name="action" value="Добави гласуване"><br><br>
  		Гласувания:
        <table>
        <tr>
    		<th>Номер на гласуването</th>
    		<th>Номер на въпрос</th>
    		<th>Предмет на гласуването</th>
    		<th>Тип</th>
    		<th>Брой ДА гласове</th>
    		<th>Брой НЕ гласове</th>
    		<th>Брой БЕЛИ гласове</th>
    		<th>Председател</th>
    		<th>Член 1</th>
    		<th>Член 2</th>
  		</tr>
  		<c:forEach var="voteBean" items="${voteBeansList}" varStatus="status">
               <tr>
                   <td>${voteBean.getVoteNumber()}</td>
                   <td>${voteBean.getQuestionNumberForDisplay()}</td>
                   <td>${voteBean.getVoteSubject()}</td>
                   <td>${voteBean.getVoteType()}</td>
                   <td>${voteBean.getNumYesVotes()}</td>
                   <td>${voteBean.getNumNoVotes()}</td>
                   <td>${voteBean.getNumBlankVotes()}</td>
                   <td>${voteBean.getModerator()}</td>
                   <td>${voteBean.getMember1()}</td>
                   <td>${voteBean.getMember2()}</td>
               </tr>
           </c:forEach> 
  		</table><br><br>
  		Номер на гласуване: <input type="text" name="removeVoteNumber"><br><br>
  		<c:if test="${removeVoteNumberError != null}"><font color="red">${removeVoteNumberError}<br><br></font></c:if>
  		<input type="submit" name="action" value="Премахни гласуване"><br><br>
  		<c:if test="${questionError != null}"><font color="red">${questionError}<br><br></font></c:if>
  		<input type="submit" name="action" value="Назад">
  		<input type="submit" name="action" value="Напред">
  		<input style="float: right;" type="submit" name="action" value="Главно меню">
    </form>
</body>
</html>