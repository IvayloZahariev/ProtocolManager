<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form action="displayProtocol" method="post">
	<div style="padding-left: 25%; padding-right: 25%">
		<div style="margin-top: 2%; margin-left: 15%"><font size="5"><b><u>ТЕХНИЧЕСКИ УНИВЕРСИТЕТ – СОФИЯ</u></b></font></div>
		<div style="margin-left: 13%"><b><font size="5">Факултет "${selectedProtocolBean.getFaculty()}"</b></font></div>
		<div style="margin-left: 26%"><b><font size="5">Катедра ${selectedProtocolBean.getDepartment()}</b></font></div>
		<br>
		<div style="margin-left: 35%"><font size="5"><b>Катедрен съвет</b></font></div>
		<br>
		<div style="margin-left: 36%"><font size="5"><b>Протокол № ${selectedProtocolBean.getNumber()}</b></font></div>
		<br>
		<div style="margin-left: 28%">от заседание, проведено на ${selectedProtocolBean.getCouncilDateForDisplay()}</div>
		<br>
		<div>АКАДЕМИЧЕН СЪСТАВ НА КАТЕДРЕНИЯ СЪВЕТ – ${selectedProtocolBean.getAllMembers()}</div>
		<div>В ОТПУСК – ${selectedProtocolBean.getFurlough()}
		<c:if test="${selectedProtocolBean.getFurloughMembersNames() != null}">
			(
			<c:forEach var="name" items="${selectedProtocolBean.getFurloughMembersNames()}" varStatus="status">
				<c:if test="status.index != 0">, </c:if>
				${name}
    	    </c:forEach>
    	    <c:forEach var="name" items="${selectedProtocolBean.getOtherFurloughMembersNames()}" varStatus="status">
				<c:if test="${!selectedProtocolBean.getFurloughMembersNames().isEmpty() || status.index != 0}">, </c:if>
				${name}
        	</c:forEach>
			)
		</c:if></div>
		<div>РЕДУЦИРАН СЪСТАВ - ${selectedProtocolBean.getReducedMembers()}</div>
		<div>КВОРУМ – ${selectedProtocolBean.getQuorum()} (2/3*)</div>
		<div>ПРИСЪСТВАТ – ${selectedProtocolBean.getPresentMembers()}</div>
		<div>ОТСЪСТВАТ – ${selectedProtocolBean.getAbsent()}
			<c:if test="${selectedProtocolBean.getAbsent() != null}">: </c:if>
			<c:forEach var="name" items="${selectedProtocolBean.getAbsentMembersNames()}" varStatus="status">
				<c:if test="status.index != 0">, </c:if>
				${name}
    	    </c:forEach>
    	    <c:forEach var="name" items="${selectedProtocolBean.getOtherAbsentMembersNames()}" varStatus="status">
				<c:if test="${!selectedProtocolBean.getAbsentMembersNames().isEmpty() || status.index != 0}">, </c:if>
				${name}
    	    </c:forEach>
		</div>
		<br>
		<div style="margin-left: 38%"><font size="5"><b>Дневен ред</b></font></div>
		<c:forEach var="agendaPoint" items="${selectedProtocolBean.getAgenda()}" varStatus="status">
			<div>${status.count}. ${agendaPoint} въпроси</div>
		</c:forEach>
		<br><br>
		<c:forEach var="questionType" items="${selectedProtocolBean.getAgenda()}" varStatus="status">
			<div style="margin-left: 15%"><b><font size="5">
			По т.${status.count} от дневния ред "${selectedProtocolBean.getAgenda().get(status.index)} въпроси"</b></font></div>
			<br><br>
			<c:forEach var="questionBean" items="${selectedProtocolBean.getQuestions()}">
				<c:if test="${questionBean.getType() == questionType}">
					<div><b>${questionBean.getNumberForDisplay()}</b> ${questionBean.getSpecification()}</div>
					<c:forEach var="vote" items="${questionBean.getVotes()}">
						<div>	Проведе се избор на комисия за ${vote.getVoteType()} гласуване. Единодушно бяха приети кандидатурите на:</div>
						<div><b><u>	  Председател: </u></b> ${vote.getModerator()}</div>
						<div style="float: left; padding-right: 2%;"><b><u>   Членове: </u></b></div>
						<div>1.${vote.getMember1()}</div>
						<div style="margin-left: 12%;">2.${vote.getMember2()}</div>
						<div>   Комисията проведе ${vote.getVoteType()} гласуване при:</div>
						<div>   АКАДЕМИЧЕН СЪСТАВ НА КАТЕДРЕНИЯ СЪВЕТ – ${selectedProtocolBean.getAllMembers()}</div>
						<div>   В ОТПУСК – ${selectedProtocolBean.getFurlough()}
						<c:if test="${selectedProtocolBean.getFurloughMembersNames() != null}">
							(
							<c:forEach var="name" items="${selectedProtocolBean.getFurloughMembersNames()}" varStatus="status">
								<c:if test="status.index != 0">, </c:if>
									${name}
    					    </c:forEach>
    	    				<c:forEach var="name" items="${selectedProtocolBean.getOtherFurloughMembersNames()}" varStatus="status">
								<c:if test="${!selectedProtocolBean.getFurloughMembersNames().isEmpty() || status.index != 0}">, </c:if>
									${name}
        					</c:forEach>
							)
						</c:if></div>
						<div>   РЕДУЦИРАН СЪСТАВ - ${selectedProtocolBean.getReducedMembers()}</div>
						<div>   КВОРУМ – ${selectedProtocolBean.getQuorum()} (2/3*)</div>
						<div>   ПРИСЪСТВАТ – ${selectedProtocolBean.getPresentMembers()}</div>
						<div>ОТСЪСТВАТ – ${selectedProtocolBean.getAbsent()}
							<c:if test="${selectedProtocolBean.getAbsent() != null}">: </c:if>
							<c:forEach var="name" items="${selectedProtocolBean.getAbsentMembersNames()}" varStatus="status">
								<c:if test="status.index != 0">, </c:if>
									${name}
    	    				</c:forEach>
    	    				<c:forEach var="name" items="${selectedProtocolBean.getOtherAbsentMembersNames()}" varStatus="status">
								<c:if test="${!selectedProtocolBean.getAbsentMembersNames().isEmpty() || status.index != 0}">, </c:if>
									${name}
    	    				</c:forEach>
						</div>
						<div>   Председателят на комисията ${vote.getModerator()} обяви следните резултати:</div>
						<div>   Общ брой на гласувалите – ${selectedProtocolBean.getPresentMembers()}</div>
						<div>   ${vote.getVoteSubject()}</div>
						<br>
						<div>   ДА - ${vote.getNumYesVotes()}	НЕ - ${vote.getNumNoVotes()}	БЕЛИ - ${vote.getNumBlankVotes()}</div>
					</c:forEach>
					<br>
					<div><b><u>РЕШЕНИЕ: </u></b>${questionBean.getDecision()}</div>
					<c:if test="${!questionBean.getComments().isEmpty()}">
						<div><b>Мнения и предложения:</b></div>
						<c:forEach var="commentBean" items="${questionBean.getComments()}">
							<div>   <b>${commentBean.getOwner()}</b> - ${commentBean.getSpecification()}</div>
						</c:forEach>
					</c:if>
				</c:if>
			</c:forEach>
			<br>
		</c:forEach>
		<br><br><br>
		<div style="float: left;"> Протоколчик: .................</div>
		<div style="margin-left: 55%">Ръководител катедра:....................</div>
		<div style="margin-left: 8%; float:left;">/${selectedProtocolBean.getReporter()}/</div>
		<div style="margin-left: 65%">/${selectedProtocolBean.getDepartmentDirector()}/</div>
		<br><br>
		<c:if test="${showCreateProtocol != null}"><input type="submit" name="action" value="Създай протокол"></c:if>
  		<c:if test="${EditProtocol != null}"><input type="submit" name="action" value="Запази промените"></c:if>
  		<c:if test="${deleteProtocol != null}"><input type="submit" name="action" value="Изтрий протокол"></c:if>
  		<input style="float: right;" type="submit" name="action" value="Главно меню">
	</div>
	</form>
</body>
</html>