<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<style>
		table {
    		font-family: arial, sans-serif;
    		border-collapse: collapse;
    		width: 100%;
		}
		td, th {
    		border: 1px solid #dddddd;
    		text-align: left;
    		padding: 8px;
		}
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Display Protocol</title>
</head>
<body>
	<form action="displayProtocol" method="post">
		Protocol type: ${selectedProtocolBean.getType()}<br><br>
		Protocol number: ${selectedProtocolBean.getNumber()}<br><br>
		Protocol faculty: ${selectedProtocolBean.getFaculty()}<br><br>
		Protocol department: ${selectedProtocolBean.getDepartment()}<br><br>
		Council date: ${selectedProtocolBean.getCouncilDateForDisplay()}<br><br>
		Protocol reporter: ${selectedProtocolBean.getReporter()}<br><br>
		Academic staff of the department council: ${selectedProtocolBean.getAllMembers()}<br><br>
		Furlough members: ${selectedProtocolBean.getFurlough()}
		<c:if test="${selectedProtocolBean.getFurlough() != null}"> - </c:if>
		<c:forEach var="name" items="${selectedProtocolBean.getFurloughMembersNames()}" varStatus="status">
			<c:if test="status.index != 0">, </c:if>
			${name}
        </c:forEach>
        <c:forEach var="name" items="${selectedProtocolBean.getOtherFurloughMembersNames()}" varStatus="status">
			<c:if test="${selectedProtocolBean.getFurloughMembersNames() != null || status.index != 0}">, </c:if>
			${name}
        </c:forEach><br><br>
        Reduced members: ${selectedProtocolBean.getReducedMembers()}<br><br>
        Quorum: ${selectedProtocolBean.getQuorum()}<br><br>
        Present members: ${selectedProtocolBean.getPresentMembers()}<br><br>
		Absent members: ${selectedProtocolBean.getAbsent()}
		<c:if test="${selectedProtocolBean.getAbsent() != null}"> - </c:if>
		<c:forEach var="name" items="${selectedProtocolBean.getAbsentMembersNames()}" varStatus="status">
			<c:if test="status.index != 0">, </c:if>
			${name}
        </c:forEach>
        <c:forEach var="name" items="${selectedProtocolBean.getOtherAbsentMembersNames()}" varStatus="status">
			<c:if test="${selectedProtocolBean.getAbsentMembersNames() != null || status.index != 0}">, </c:if>
			${name}
        </c:forEach><br><br>
		Questions:
        <table>
        <tr>
    		<th>Question number</th>
    		<th>Question type</th>
    		<th>Question specification</th>
    		<th>Question decision</th>
  		</tr>
  		<c:forEach var="questionBean" items="${selectedProtocolBean.getQuestions()}" varStatus="status">
               <tr>
                   <td>${questionBean.getNumberForDisplay()}</td>
                   <td>${questionBean.getType()}</td>
                   <td>${questionBean.getSpecification()}</td>
                   <td>${questionBean.getDecision()}</td>
               </tr>
           </c:forEach> 
  		</table><br><br>
  		Comments:<br><br>
        <table>
        <tr>
        	<th>Question number</th>
    		<th>Comment owner</th>
    		<th>Comment specification</th>
  		</tr>
  		<c:forEach var="commentBean" items="${questionsComments}" varStatus="status">
               <tr>
               	   <td>${commentBean.getQuestionNumber()}</td>
                   <td>${commentBean.getOwner()}</td>
                   <td>${commentBean.getSpecification()}</td>
               </tr>
           </c:forEach> 
  		</table><br><br>
  		Votes:
        <table>
        <tr>
        	<th>Question number</th>
    		<th>Vote subject</th>
    		<th>Vote type</th>
    		<th>Number yes votes</th>
    		<th>Number no votes</th>
    		<th>Number blank votes</th>
    		<th>Moderator</th>
    		<th>Member 1</th>
    		<th>Member 2</th>
  		</tr>
  		<c:forEach var="voteBean" items="${questionsVotes}" varStatus="status">
               <tr>
               	   <td>${voteBean.getQuestionNumber()}</td>
                   <td>${voteBean.getVoteSubject()}</td>
                   <td>${voteBean.getVoteType()}</td>
                   <td>${voteBean.getNumYesVotes()}</td>
                   <td>${voteBean.getNumNoVotes()}</td>
                   <td>${voteBean.getNumBlankVotes()}</td>
                   <td>${voteBean.getModerator()}</td>
                   <td>${voteBean.getMember1()}</td>
                   <td>${voteBean.getMember2()}</td>
               </tr>
           </c:forEach> 
  		</table><br><br>
  		<c:if test="${showCreateProtocol != null}"><input type="submit" name="action" value="Create protocol"></c:if>
  		<c:if test="${EditProtocol != null}"><input type="submit" name="action" value="Save changes"></c:if>
  		<c:if test="${deleteProtocol != null}"><input type="submit" name="action" value="Delete protocol"></c:if>
  		<input type="submit" name="action" value="Home">
	</form>
</body>
</html>