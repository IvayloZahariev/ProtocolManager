<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<style>
		table {
    		font-family: arial, sans-serif;
    		border-collapse: collapse;
    		width: 100%;
		}
		td, th {
    		border: 1px solid #dddddd;
    		text-align: left;
    		padding: 8px;
		}
	</style>
	<meta http-equiv="Content-Type">
	<title>Намерени протоколи</title>
</head>
<body>
	<form action="displayProtocols" method="post">
		<b>Протоколи</b>
		<table>
        	<tr>
        		<th>Номер на ред</th>
        		<th>Тип</th>
    			<th>Номер на протокол</th>
    			<th>Факултет</th>
    			<th>Катедра</th>
    			<th>Дата на съвета</th>
    			<th>Протоколчик</th>
  			</tr>
  			<c:forEach var="protocolBean" items="${protocolBeansList}" varStatus="status">
                <tr>
                	<td>${status.count}</td>
                    <td>${protocolBean.getType()}</td>
                    <td>${protocolBean.getNumber()}</td>
                    <td>${protocolBean.getFaculty()}</td>
                    <td>${protocolBean.getDepartment()}</td>
                    <td>${protocolBean.getCouncilDateForDisplay()}</td>
                    <td>${protocolBean.getReporter()}</td>
                </tr>
            </c:forEach>
  		</table><br><br>
  		<b>Въпроси към протокол</b><br><br>
  		Номер на реда на протокола: <input type="text" name="protocolRowNumber"><br><br>
  		<c:if test="${protocolRowNumberErrorMessage != null}"><font color="red">${protocolRowNumberErrorMessage}<br><br></font></c:if>
  		<input type="submit" name="action" value="Покажи въпроси"><br><br>
  		Въпроси:
        <table>
        <tr>
    		<th>Номер</th>
    		<th>Тип</th>
    		<th>Описание</th>
    		<th>Решение</th>
  		</tr>
  		<c:forEach var="questionBean" items="${protocolQuestions}" varStatus="status">
               <tr>
                   <td>${questionBean.getNumberForDisplay()}</td>
                   <td>${questionBean.getType()}</td>
                   <td>${questionBean.getSpecification()}</td>
                   <td>${questionBean.getDecision()}</td>
               </tr>
           </c:forEach> 
  		</table><br><br>
  		<b>Коментари към въпрос</b><br><br>
  		Номер на въпрос: <input type="text" name="commentQuestionNumber"><br><br>
  		<c:if test="${commentQuestionNumberErrorMessage != null}"><font color="red">${commentQuestionNumberErrorMessage}<br><br></font></c:if>
  		<input type="submit" name="action" value="Покажи коментари"><br><br>
  		Коментари:<br><br>
        <table>
        <tr>
    		<th>Собственик на коментара</th>
    		<th>Коментар</th>
  		</tr>
  		<c:forEach var="commentBean" items="${questionComments}" varStatus="status">
               <tr>
                   <td>${commentBean.getOwner()}</td>
                   <td>${commentBean.getSpecification()}</td>
               </tr>
           </c:forEach> 
  		</table><br><br>
  		<b>Гласувания към въпрос</b><br><br>
  		Номер на въпрос: <input type="text" name="voteQuestionNumber"><br><br>
  		<c:if test="${voteQuestionNumberErrorMessage != null}"><font color="red">${voteQuestionNumberErrorMessage}<br><br></font></c:if>
  		<input type="submit" name="action" value="Покажи гласувания"><br><br>
  		Гласувания:
        <table>
        <tr>
    		<th>Предмет на гласуването</th>
    		<th>Тип</th>
    		<th>Брой ДА</th>
    		<th>Брой НЕ</th>
    		<th>Брой БЕЛИ</th>
    		<th>Председател</th>
    		<th>Член 1</th>
    		<th>Член 2</th>
  		</tr>
  		<c:forEach var="voteBean" items="${questionVotes}" varStatus="status">
               <tr>
                   <td>${voteBean.getVoteSubject()}</td>
                   <td>${voteBean.getVoteType()}</td>
                   <td>${voteBean.getNumYesVotes()}</td>
                   <td>${voteBean.getNumNoVotes()}</td>
                   <td>${voteBean.getNumBlankVotes()}</td>
                   <td>${voteBean.getModerator()}</td>
                   <td>${voteBean.getMember1()}</td>
                   <td>${voteBean.getMember2()}</td>
               </tr>
           </c:forEach> 
  		</table><br><br>
  		Ред на протокол: <input type="text" name="viewProtocolRowNumber"><br><br>
  		<c:if test="${viewProtocolRowNumberErrorMessage != null}"><font color="red">${viewProtocolRowNumberErrorMessage}<br><br></font></c:if>
  		<c:if test="${EditProtocol == null}"><input type="submit" name="action" value="Покажи протокол"></c:if>
  		<c:if test="${EditProtocol != null}"><input type="submit" name="action" value="Промени протокол"></c:if>
  		<input style="float: right;" type="submit" name="action" value="Главно меню">
	</form>
</body>
</html>