<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type">
	<title>Find protocol</title>
</head>
<body>
	<form action="findProtocol" method="post">
		Тип на протокол: <input type="radio" name="protocolType" value="Факултетен"
			<c:if test="${checkedRadioButton == 'Факултетен'}">checked</c:if>>Факултетен съвет 
			<input type="radio" name="protocolType" value="Катедрен"
			<c:if test="${checkedRadioButton == 'Катедрен'}">checked</c:if>>Катедрен съвет
			<input type="radio" name="protocolType" value="Всички типове"
			<c:if test="${checkedRadioButton == 'Всички типове'}">checked</c:if>>Всички типове<br><br>
		Номер на протокол: <input type="text" name="protocolNumber" value="${protocolNumber}"><br><br>
        <c:if test="${protocolNumberErrorMessage != null}"><font color="red">${protocolNumberErrorMessage}<br><br></font></c:if>
        Дата на съвета: <input type="date" name="councilDate" value="${councilDate}"><br><br>
        Факултет: <select name="selectedFaculty">
        	<option value="default">Select faculty</option>
        	<c:forEach var="faculty" items="${facultyList}">
        		<option value="${faculty}"
        		<c:if test="${faculty == selectedFacultyOption}">selected</c:if>>${faculty}</option>
        	</c:forEach>
        </select><br><br>
        Катедера: <select name="selectedDepartment">
        	<option value=default>Select department</option>
        	<c:forEach var="department" items="${departmentList}">
        		<option value="${department}"
        		<c:if test="${department == selectedDepartmentOption}">selected</c:if>>${department}</option>
        	</c:forEach>
        </select>
        <i> Ако са предоставени и факултет и катедра за протокола, параметъра за факултет няма да бъде взет предвид!</i><br><br>
        Протоколчик: <select name="selectedReporter">
        	<option value="default">Select reporter</option>
        	<c:forEach var="name" items="${councilMembers}">
        		<option value="${name}"
        		<c:if test="${name == selectedReporterOption}">selected</c:if>>${name}</option>
        	</c:forEach>
        </select><br><br>
        Описание на въпрос:<br><br>
        <textarea rows="4" cols="50" name="questionSpecification">${questionSpecification}</textarea><br><br>
        Решение на въпрос:<br><br>
        <textarea rows="4" cols="50" name="questionDecision">${questionDecision}</textarea><br><br>
        <c:if test="${findProtocolErrorMessage != null}"><font color="red">${findProtocolErrorMessage}<br><br></font></c:if>
		<input type="submit" name="action" value="Намери протокол">
		<input style="float: right;" type="submit" name="action" value="Главно меню">
 	</form>
</body>
</html>