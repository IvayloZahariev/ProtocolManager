/**
 * 
 */
package com.izahariev.projects.pm.presentation.validation;

import javax.servlet.http.HttpServletRequest;

/**
 * Class providing different kinds of validation methods.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings("PMD.UseUtilityClass")
public class Validator {

    /**
     * Default constructor.
     */
    public Validator() {

    }

    /**
     * Validates that the selected option of a given drop down menu is not the
     * default.
     * 
     * @param request
     *            HttpServletRequest of the servlet which uses the method.
     * @param parameterName
     *            The name of the select parameter.
     * @param errorMessageAttribute
     *            The name of the attribute used to store the error message if
     *            the selected option is the default option.
     * @param errorMessage
     *            The message which should be shown if the selected option is
     *            the default option.
     * @return True if the selected option is not the default option, otherwise
     *         false.
     */
    @SuppressWarnings("nls")
    public static boolean validateDrpodownNotEmpty(final HttpServletRequest request, final String parameterName,
            final String errorMessageAttribute, final String errorMessage) {
        if ("default".equals(request.getParameter(parameterName))) {
            request.setAttribute(errorMessageAttribute, errorMessage);
            return false;
        }
        return true;
    }

    /**
     * Validates that the given text box is not empty.
     * 
     * @param request
     *            HttpServletRequest of the servlet which uses the method.
     * @param parameterName
     *            The name of the text box parameter.
     * @param errorMessageAttribute
     *            The name of the attribute used to store the error message if
     *            the text box is empty.
     * @param errorMessage
     *            The message which should be shown if the text box is empty.
     * @return True if the text box is not empty, otherwise false.
     */
    public static boolean validateTextBoxNotEmpty(final HttpServletRequest request, final String parameterName,
            final String errorMessageAttribute, final String errorMessage) {
        if (request.getParameter(parameterName).trim().isEmpty()) {
            request.setAttribute(errorMessageAttribute, errorMessage);
            return false;
        }
        return true;
    }

    /**
     * Validates that the given text box contains a number.
     * 
     * @param request
     *            HttpServletRequest of the servlet which uses the method.
     * @param parameterName
     *            The name of the text box parameter.
     * @param errorMessageAttribute
     *            The name of the attribute used to store the error message if
     *            the text box doesn't contains a number.
     * @param errorMessage
     *            The message which should be shown if the text box doesn't
     *            contains a number.
     * @return True if the text box contains a number, otherwise false.
     */
    public static boolean validateTextBoxNumber(final HttpServletRequest request, final String parameterName,
            final String errorMessageAttribute, final String errorMessage) {
        try {
            Float.parseFloat(request.getParameter(parameterName));
        }
        catch (@SuppressWarnings("unused") final NumberFormatException numberFormatException) {
            request.setAttribute(errorMessageAttribute, errorMessage);
            return false;
        }
        return true;
    }

    /**
     * Validates that the given text box contains a positive number.
     * 
     * @param request
     *            HttpServletRequest of the servlet which uses the method.
     * @param parameterName
     *            The name of the text box parameter.
     * @param errorMessageAttribute
     *            The name of the attribute used to store the error message if
     *            the text box doesn't contains a positive number.
     * @param errorMessage
     *            The message which should be shown if the text box doesn't
     *            contains a positive number.
     * @return True if the text box contains a positive number, otherwise false.
     */
    public static boolean validatePositiveNumber(final HttpServletRequest request, final String parameterName,
            final String errorMessageAttribute, final String errorMessage) {
        if (Integer.parseInt(request.getParameter(parameterName)) < 0) {
            request.setAttribute(errorMessageAttribute, errorMessage);
            return false;
        }
        return true;
    }

    /**
     * Validates that one of the radio buttons with the given name is selected.
     * 
     * @param request
     *            HttpServletRequest of the servlet which uses the method.
     * @param parameterName
     *            The name of the radio buttons.
     * @param errorMessageAttribute
     *            The name of the attribute used to store the error message if
     *            no radio button is selected.
     * @param errorMessage
     *            The message which should be shown if no radio button is
     *            selected.
     * @return True if one of the radio buttons is selected, otherwise false.
     */
    public static boolean validatePressedRadioButton(final HttpServletRequest request, final String parameterName,
            final String errorMessageAttribute, final String errorMessage) {
        if (request.getParameter(parameterName) == null) {
            request.setAttribute(errorMessageAttribute, errorMessage);
            return false;
        }
        return true;
    }

}
