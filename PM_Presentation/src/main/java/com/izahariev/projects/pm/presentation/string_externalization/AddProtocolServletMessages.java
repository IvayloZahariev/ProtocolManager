package com.izahariev.projects.pm.presentation.string_externalization;

import org.eclipse.osgi.util.NLS;

/**
 * Class used for sting externalization in the AddProtocolServlet.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings("PMD.LongVariable")
public final class AddProtocolServletMessages extends NLS {
    private static final String BUNDLE_NAME = "com.izahariev.projects.pm.presentation.string_externalization.addProtocolServletMessages"; //$NON-NLS-1$
    public static String AddProtocolServlet_EmptyNameMessage;
    public static String AddProtocolServlet_EmptyFacultyMessage;
    public static String AddProtocolServlet_EmptyDepartmentMessage;
    public static String AddProtocolServlet_NoProtocolTypeMessage;
    public static String AddProtocolServlet_InvalidMembersNumberMessage;
    public static String AddProtocolServlet_InvalidProtocolNumberMessage;
    public static String AddProtocolServlet_NoProtocolDateMessage;
    public static String AddProtocolServlet_NoReporterMessage;
    public static String AddProtocolServlet_DepartmentFacultyMissmatchMessage;
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, AddProtocolServletMessages.class);
    }

    private AddProtocolServletMessages() {
        super();
    }
}
