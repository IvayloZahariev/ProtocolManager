package com.izahariev.projects.pm.presentation.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.izahariev.projects.pm.beans.ProtocolBean;
import com.izahariev.projects.pm.factories.services.ServicesFactory;
import com.izahariev.projects.pm.presentation.string_externalization.FindProtocolServletMessages;
import com.izahariev.projects.pm.presentation.validation.Validator;

/**
 * Servlet implementation class FindProtocolServlet
 */
public class FindProtocolServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FindProtocolServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @SuppressWarnings({ "nls", "boxing" })
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession().getAttribute("dropDownsSet") == null) {
            request.getSession().setAttribute("facultyList", ServicesFactory.getFindProtocolServices().getFaculties());
            request.getSession().setAttribute("departmentList",
                    ServicesFactory.getFindProtocolServices().getDepartments());
            request.getSession().setAttribute("councilMembers",
                    ServicesFactory.getFindProtocolServices().getNonAcademicCouncilMembers());
            request.getSession().setAttribute("dropDownsSet", true);
        }

        final RequestDispatcher RequetsDispatcherObj = request.getRequestDispatcher("/FindProtocol.jsp");
        RequetsDispatcherObj.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @SuppressWarnings({ "nls", "PMD.AvoidDuplicateLiterals", "boxing" })
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final String action = request.getParameter("action");
        if ("Намери протокол".equals(action)) {
            @SuppressWarnings("PMD.UselessParentheses")
            final boolean validProtocolNumber = request.getParameter("protocolNumber").trim().isEmpty()
                    || (Validator.validateTextBoxNumber(request, "protocolNumber", "protocolNumberErrorMessage",
                            FindProtocolServletMessages.FindProtocolServlet_EnterNumberMessage)
                            && Validator.validatePositiveNumber(request, "protocolNumber", "protocolNumberErrorMessage",
                                    FindProtocolServletMessages.FindProtocolServlet_EnterPositiveNumberMessage));
            if (validProtocolNumber) {
                String protocolType = request.getParameter("protocolType");
                if ("Всички типове".equals(protocolType)) {
                    protocolType = "";
                }
                final List<ProtocolBean> protocolBeans = ServicesFactory.getFindProtocolServices().getProtocols(
                        protocolType, request.getParameter("protocolNumber"), request.getParameter("councilDate"),
                        request.getParameter("selectedFaculty"), request.getParameter("selectedDepartment"),
                        request.getParameter("selectedReporter"), request.getParameter("questionSpecification"),
                        request.getParameter("questionDecision"));
                if (protocolBeans.isEmpty()) {
                    request.setAttribute("findProtocolErrorMessage",
                            FindProtocolServletMessages.FindProtocolServlet_NoProtocolsMessage);
                    doGet(request, response);
                }
                else {
                    request.getSession().removeAttribute("councilMembers");
                    request.getSession().setAttribute("protocolBeansList", protocolBeans);
                    response.sendRedirect("/PM_Presentation/private/displayProtocols");
                }
            }
            else {
                doGet(request, response);
            }
        }
        else {
            final HttpSession session = request.getSession();
            session.invalidate();
            request.getSession(true).setAttribute("userIsLogged", true);
            response.sendRedirect("/PM_Presentation/private/home");
        }
    }

}
