package com.izahariev.projects.pm.presentation.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.izahariev.projects.pm.beans.CommentBean;
import com.izahariev.projects.pm.beans.ProtocolBean;
import com.izahariev.projects.pm.beans.QuestionBean;
import com.izahariev.projects.pm.beans.VoteBean;
import com.izahariev.projects.pm.factories.services.ServicesFactory;
import com.izahariev.projects.pm.presentation.string_externalization.AddQuestionServletMessages;
import com.izahariev.projects.pm.presentation.utilities.CommentUtility;
import com.izahariev.projects.pm.presentation.utilities.QuestionUtility;
import com.izahariev.projects.pm.presentation.utilities.VoteUtility;

/**
 * Servlet implementation class SetProtocolAgendaServlet
 */
@SuppressWarnings({ "PMD.AvoidDuplicateLiterals", "PMD.StdCyclomaticComplexity" })
public class AddQuestionsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     * 
     * @see HttpServlet#HttpServlet()
     */
    public AddQuestionsServlet() {
        super();
    }

    /**
     * Initializes the questionTypesList if it's null and forwards to
     * AddQuestion.jsp.
     * 
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @SuppressWarnings("nls")
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession().getAttribute("questionTypesList") == null) {
            request.getSession().setAttribute("questionTypesList",
                    ServicesFactory.getAddQuestionServices().getQuestionTypes());
        }

        final RequestDispatcher RequetsDispatcherObj = request.getRequestDispatcher("/AddQuestions.jsp");

        RequetsDispatcherObj.forward(request, response);
    }

    /**
     * Determines which button is pressed in the jsp and executes the
     * corresponding method.
     * 
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @SuppressWarnings({ "nls", "boxing" })
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final String action = request.getParameter("action");
        boolean nextOrBackActionExecuted = false;
        switch (action) {
            case "Добави въпрос":
                if (!QuestionUtility.addQuestion(request, response)) {
                    setInputData(request);
                }
                break;
            case "Премахни въпрос":
                QuestionUtility.removeQuestion(request);
                break;
            case "Добави коментар":
                if (!CommentUtility.addComment(request)) {
                    setInputData(request);
                }
                break;
            case "Премахни коментар":
                CommentUtility.removeComment(request);
                break;
            case "Добави гласуване":
                if (!VoteUtility.addVote(request)) {
                    setInputData(request);
                }
                break;
            case "Премахни гласуване":
                VoteUtility.removeVote(request);
                break;
            case "Напред":
                if (setProtocolInfo(request)) {
                    nextOrBackActionExecuted = true;
                    request.getSession().setAttribute("showCreateProtocol", true);
                    response.sendRedirect("/PM_Presentation/private/displayProtocol");
                }
                break;
            case "Главно меню":
                final HttpSession session = request.getSession();
                session.invalidate();
                request.getSession(true).setAttribute("userIsLogged", true);
                response.sendRedirect("/PM_Presentation/private/home");
                break;
            default:
                nextOrBackActionExecuted = true;
                response.sendRedirect("/PM_Presentation/private/addProtocol");
                break;
        }
        if (!nextOrBackActionExecuted) {
            doGet(request, response);
        }
    }

    @SuppressWarnings({ "nls", "unchecked", "PMD.AvoidInstantiatingObjectsInLoops" })
    private static boolean setProtocolInfo(final HttpServletRequest request) {
        if (request.getSession().getAttribute("questionBeansList") == null) {
            request.setAttribute("questionError",
                    AddQuestionServletMessages.AddQuestionsServlet_NoQuestionsAddedMessage);
            return false;
        }
        final ProtocolBean protocolBean = (ProtocolBean) request.getSession().getAttribute("protocolBean");
        final List<QuestionBean> questionBeans = (List<QuestionBean>) request.getSession()
                .getAttribute("questionBeansList");
        final List<CommentBean> commentBeans = (List<CommentBean>) request.getSession()
                .getAttribute("commentBeansList");
        final List<VoteBean> voteBeans = (List<VoteBean>) request.getSession().getAttribute("voteBeansList");
        final List<String> agendaPoints = new ArrayList<>();
        for (final QuestionBean questionBean : questionBeans) {
            final List<CommentBean> questionComments = new ArrayList<>();
            if (commentBeans != null) {
                for (final CommentBean commentBean : commentBeans) {
                    if (questionBean.getNumber() == commentBean.getQuestionNumber()) {
                        questionComments.add(commentBean);
                    }
                }
            }
            final List<VoteBean> questionVotes = new ArrayList<>();
            if (voteBeans != null) {
                for (final VoteBean voteBean : voteBeans) {
                    if (questionBean.getNumber() == voteBean.getQuestionNumber()) {
                        questionVotes.add(voteBean);
                    }
                }
            }
            if (!agendaPoints.contains(questionBean.getType())) {
                agendaPoints.add(questionBean.getType());
            }
            questionBean.setComments(questionComments);
            questionBean.setVotes(questionVotes);
        }
        protocolBean.setAgenda(agendaPoints);
        protocolBean.setQuestions(questionBeans);
        request.getSession().setAttribute("selectedProtocolBean", protocolBean);

        return true;
    }

    @SuppressWarnings("nls")
    private static void setInputData(final HttpServletRequest request) {
        request.getSession().setAttribute("selectedQuestionType", request.getParameter("selectedQuestionType"));
        request.getSession().setAttribute("questionNumber", request.getParameter("questionNumber"));
        request.getSession().setAttribute("questionSpecification", request.getParameter("questionSpecification"));
        request.getSession().setAttribute("questionDecision", request.getParameter("questionDecision"));
        request.getSession().setAttribute("commentQuestionNumber", request.getParameter("commentQuestionNumber"));
        request.getSession().setAttribute("selctedCommentOwner", request.getParameter("selectedCommentOwner"));
        request.getSession().setAttribute("comentSpecification", request.getParameter("commentSpecification"));
        request.getSession().setAttribute("voteQuestionNumber", request.getParameter("voteQuestionNumber"));
        request.getSession().setAttribute("voteSubject", request.getParameter("voteSubject"));
        request.getSession().setAttribute("selectedVoteType", request.getParameter("voteType"));
        request.getSession().setAttribute("numberYesVotes", request.getParameter("yesVotes"));
        request.getSession().setAttribute("numberNoVotes", request.getParameter("noVotes"));
        request.getSession().setAttribute("numberBlankVotes", request.getParameter("blankVotes"));
        request.getSession().setAttribute("selectedVoteModerator", request.getParameter("selectedVoteModerator"));
        request.getSession().setAttribute("otherModeratorName", request.getParameter("otherModeratorName"));
        request.getSession().setAttribute("selectedVoteMember1", request.getParameter("selectedVoteMember1"));
        request.getSession().setAttribute("otherMember1Name", request.getParameter("otherMember1Name"));
        request.getSession().setAttribute("selectedVoteMember2", request.getParameter("selectedVoteMember2"));
        request.getSession().setAttribute("otherMember2Name", request.getParameter("otherMember2Name"));
    }
}
