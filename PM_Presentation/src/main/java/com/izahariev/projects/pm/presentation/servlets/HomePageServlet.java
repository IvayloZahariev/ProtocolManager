package com.izahariev.projects.pm.presentation.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet containing the controls in the home page of the Protocol Manager
 * application.
 */
public class HomePageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     * 
     * @see HttpServlet#HttpServlet()
     */
    public HomePageServlet() {
        super();
    }

    /**
     * Forwards to the Homepage.jsp.
     * 
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        @SuppressWarnings("nls")
        final RequestDispatcher RequetsDispatcherObj = request.getRequestDispatcher("/HomePage.jsp");
        RequetsDispatcherObj.forward(request, response);
    }

    /**
     * Determines which button was pressed on the home page and forwards to the
     * corresponding jsp.
     * 
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @SuppressWarnings({ "nls", "boxing" })
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final String action = request.getParameter("action");

        if (action != null) {
            switch (action) {
                case "Добавяне на протокол":
                    response.sendRedirect("/PM_Presentation/private/addProtocol");
                    break;
                case "Променяне на протокол":
                    request.getSession().setAttribute("EditProtocol", true);
                    response.sendRedirect("/PM_Presentation/private/findProtocol");
                    break;
                case "Намиране на протокол":
                    response.sendRedirect("/PM_Presentation/private/findProtocol");
                    break;
                default:
                    request.getSession().setAttribute("deleteProtocol", true);
                    response.sendRedirect("/PM_Presentation/private/findProtocol");
                    break;
            }
        }
    }

}
