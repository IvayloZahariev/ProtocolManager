package com.izahariev.projects.pm.presentation.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.izahariev.projects.pm.beans.CommentBean;
import com.izahariev.projects.pm.beans.ProtocolBean;
import com.izahariev.projects.pm.beans.QuestionBean;
import com.izahariev.projects.pm.beans.VoteBean;
import com.izahariev.projects.pm.factories.services.ServicesFactory;

/**
 * Servlet implementation class DisplaySingleProtocolServlet
 */
public class DisplaySingleProtocolServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplaySingleProtocolServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @SuppressWarnings("nls")
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession().getAttribute("questionsComments") == null
                || request.getSession().getAttribute("questionsVotes") == null) {
            @SuppressWarnings("PMD.AvoidDuplicateLiterals")
            final ProtocolBean protocolBean = (ProtocolBean) request.getSession().getAttribute("selectedProtocolBean");
            request.getSession().setAttribute("questionsComments", getComments(protocolBean));
            request.getSession().setAttribute("questionsVotes", getVotes(protocolBean));
        }
        // TODO: Fix the ',' in jsp.
        // final RequestDispatcher RequetsDispatcherObj =
        // request.getRequestDispatcher("/DisplaySingleProtocol.jsp");
        final RequestDispatcher RequetsDispatcherObj = request.getRequestDispatcher("/ProtocolReview.jsp");
        RequetsDispatcherObj.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @SuppressWarnings({ "nls", "incomplete-switch", "boxing", "PMD.SwitchStmtsShouldHaveDefault" })
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final String action = request.getParameter("action");
        switch (action) {
            case "Създай протокол":
                ServicesFactory.getDisplaySingleProtocolServices()
                        .addProtocol((ProtocolBean) request.getSession().getAttribute("selectedProtocolBean"));
                // TODO: Display message before redirecting.
                break;
            case "Изтрий протокол":
                ServicesFactory.getDisplaySingleProtocolServices()
                        .deleteProtocol((ProtocolBean) request.getSession().getAttribute("selectedProtocolBean"));
                break;
            case "Запази промените":
                ServicesFactory.getDisplaySingleProtocolServices().updateProtocol(
                        (ProtocolBean) request.getSession().getAttribute("oldProtocolBean"),
                        (ProtocolBean) request.getSession().getAttribute("selectedProtocolBean"));
                break;
        }
        final HttpSession session = request.getSession();
        session.invalidate();
        request.getSession(true).setAttribute("userIsLogged", true);
        response.sendRedirect("/PM_Presentation/private/home");
    }

    private static List<CommentBean> getComments(final ProtocolBean bean) {
        final List<CommentBean> commentBeans = new ArrayList<>();
        for (final QuestionBean questionBean : bean.getQuestions()) {
            if (questionBean.getComments() != null) {
                commentBeans.addAll(questionBean.getComments());
            }
        }
        return commentBeans;
    }

    private static List<VoteBean> getVotes(final ProtocolBean bean) {
        final List<VoteBean> voteBeans = new ArrayList<>();
        for (final QuestionBean questionBean : bean.getQuestions()) {
            if (questionBean.getVotes() != null) {
                voteBeans.addAll(questionBean.getVotes());
            }
        }
        return voteBeans;
    }
}
