package com.izahariev.projects.pm.presentation.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.izahariev.projects.pm.beans.ProtocolBean;
import com.izahariev.projects.pm.beans.QuestionBean;
import com.izahariev.projects.pm.presentation.string_externalization.DisplayProtocolServletMessages;
import com.izahariev.projects.pm.presentation.validation.Validator;

/**
 * Servlet implementation class DisplayProtocolsServlet
 */
public class DisplayProtocolsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     * 
     * @see HttpServlet#HttpServlet()
     */
    public DisplayProtocolsServlet() {
        super();
    }

    /**
     * Forwards to DisplayProtocols.jsp.
     * 
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @SuppressWarnings("nls")
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final RequestDispatcher RequetsDispatcherObj = request.getRequestDispatcher("/DisplayProtocols.jsp");
        RequetsDispatcherObj.forward(request, response);
    }

    /**
     * Determines which button was pressed in the jsp and executes the
     * corresponding action.
     * 
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @SuppressWarnings({ "nls", "boxing" })
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final String action = request.getParameter("action");
        switch (action) {
            case "Покажи въпроси":
                showProtocolQuestions(request);
                doGet(request, response);
                break;
            case "Покажи коментари":
                showQuestionComments(request);
                doGet(request, response);
                break;
            case "Покажи гласувания":
                showQuestionVotes(request);
                doGet(request, response);
                break;
            case "Промени протокол":
                editProtocol(request, response);
                break;
            case "Главно меню":
                final HttpSession session = request.getSession();
                session.invalidate();
                request.getSession(true).setAttribute("userIsLogged", true);
                response.sendRedirect("/PM_Presentation/private/home");
                break;
            default:
                viewProtocol(request, response);
                break;
        }
    }

    @SuppressWarnings({ "unchecked", "nls", "PMD.AvoidDuplicateLiterals" })
    private static void showProtocolQuestions(final HttpServletRequest request) {
        final boolean validProtocolNumber = Validator.validateTextBoxNotEmpty(request, "protocolRowNumber",
                "protocolRowNumberErrorMessage",
                DisplayProtocolServletMessages.DisplayProtocolsServlet_EmptyFieldMessage)
                && Validator.validateTextBoxNumber(request, "protocolRowNumber", "protocolRowNumberErrorMessage",
                        DisplayProtocolServletMessages.DisplayProtocolsServlet_EnterNumberMessage);
        if (validProtocolNumber) {
            final List<ProtocolBean> protocolBeans = (List<ProtocolBean>) request.getSession()
                    .getAttribute("protocolBeansList");
            if (Integer.parseInt(request.getParameter("protocolRowNumber")) < 1
                    || Integer.parseInt(request.getParameter("protocolRowNumber")) > protocolBeans.size()) {
                request.setAttribute("protocolRowNumberErrorMessage",
                        DisplayProtocolServletMessages.DisplayProtocolsServlet_NoSuchRowMessage);
            }
            else {
                request.getSession().setAttribute("protocolQuestions", protocolBeans
                        .get(Integer.parseInt(request.getParameter("protocolRowNumber")) - 1).getQuestions());
            }
        }
    }

    @SuppressWarnings({ "unchecked", "nls", "PMD.AvoidDuplicateLiterals" })
    private static void showQuestionComments(final HttpServletRequest request) {
        final boolean validQuestionNumber = Validator.validateTextBoxNotEmpty(request, "commentQuestionNumber",
                "commentQuestionNumberErrorMessage",
                DisplayProtocolServletMessages.DisplayProtocolsServlet_EmptyFieldMessage)
                && Validator.validateTextBoxNumber(request, "commentQuestionNumber",
                        "commentQuestionNumberErrorMessage",
                        DisplayProtocolServletMessages.DisplayProtocolsServlet_EnterNumberMessage);
        if (validQuestionNumber) {
            final List<QuestionBean> questionBeans = (List<QuestionBean>) request.getSession()
                    .getAttribute("protocolQuestions");
            if (questionBeans == null) {
                request.setAttribute("commentQuestionNumberErrorMessage",
                        DisplayProtocolServletMessages.DisplayProtocolsServlet_EmptyTableMessage);
            }
            else {
                QuestionBean questionBean = null;
                for (final QuestionBean bean : questionBeans) {
                    if (bean.getNumber() == Float.parseFloat(request.getParameter("commentQuestionNumber"))) {
                        questionBean = bean;
                    }
                }
                if (questionBean == null) {
                    request.setAttribute("commentQuestionNumberErrorMessage",
                            DisplayProtocolServletMessages.DisplayProtocolsServlet_NoSuchQuestionMessage);
                }
                else {
                    request.getSession().setAttribute("questionComments", questionBean.getComments());
                }
            }
        }
    }

    @SuppressWarnings({ "unchecked", "nls", "PMD.AvoidDuplicateLiterals" })
    private static void showQuestionVotes(final HttpServletRequest request) {
        final boolean validQuestionNumber = Validator.validateTextBoxNotEmpty(request, "voteQuestionNumber",
                "voteQuestionNumberErrorMessage",
                DisplayProtocolServletMessages.DisplayProtocolsServlet_EmptyFieldMessage)
                && Validator.validateTextBoxNumber(request, "voteQuestionNumber", "voteQuestionNumberErrorMessage",
                        DisplayProtocolServletMessages.DisplayProtocolsServlet_EnterNumberMessage);
        if (validQuestionNumber) {
            final List<QuestionBean> questionBeans = (List<QuestionBean>) request.getSession()
                    .getAttribute("protocolQuestions");
            if (questionBeans == null) {
                request.setAttribute("voteQuestionNumberErrorMessage",
                        DisplayProtocolServletMessages.DisplayProtocolsServlet_EmptyTableMessage);
            }
            else {
                QuestionBean questionBean = null;
                for (final QuestionBean bean : questionBeans) {
                    if (bean.getNumber() == Float.parseFloat(request.getParameter("voteQuestionNumber"))) {
                        questionBean = bean;
                    }
                }
                if (questionBean == null) {
                    request.setAttribute("voteQuestionNumberErrorMessage",
                            DisplayProtocolServletMessages.DisplayProtocolsServlet_NoSuchQuestionMessage);
                }
                else {
                    request.getSession().setAttribute("questionVotes", questionBean.getVotes());
                }
            }
        }
    }

    @SuppressWarnings({ "nls", "unchecked", "PMD.AvoidDuplicateLiterals" })
    private void viewProtocol(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException, ServletException {
        final boolean validProtocolNumber = Validator.validateTextBoxNotEmpty(request, "viewProtocolRowNumber",
                "viewProtocolRowNumberErrorMessage",
                DisplayProtocolServletMessages.DisplayProtocolsServlet_EmptyFieldMessage)
                && Validator.validateTextBoxNumber(request, "viewProtocolRowNumber",
                        "viewProtocolRowNumberErrorMessage",
                        DisplayProtocolServletMessages.DisplayProtocolsServlet_EnterNumberMessage);
        if (validProtocolNumber) {
            final List<ProtocolBean> protocolBeans = (List<ProtocolBean>) request.getSession()
                    .getAttribute("protocolBeansList");
            request.getSession().setAttribute("selectedProtocolBean",
                    protocolBeans.get(Integer.parseInt(request.getParameter("viewProtocolRowNumber")) - 1));
            response.sendRedirect("/PM_Presentation/private/displayProtocol");
        }
        else {
            doGet(request, response);
        }
    }

    @SuppressWarnings({ "nls", "unchecked" })
    private void editProtocol(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException, ServletException {
        final boolean validProtocolNumber = Validator.validateTextBoxNotEmpty(request, "viewProtocolRowNumber",
                "viewProtocolRowNumberErrorMessage",
                DisplayProtocolServletMessages.DisplayProtocolsServlet_EmptyFieldMessage)
                && Validator.validateTextBoxNumber(request, "viewProtocolRowNumber",
                        "viewProtocolRowNumberErrorMessage",
                        DisplayProtocolServletMessages.DisplayProtocolsServlet_EnterNumberMessage);
        if (validProtocolNumber) {
            final List<ProtocolBean> protocolBeans = (List<ProtocolBean>) request.getSession()
                    .getAttribute("protocolBeansList");
            request.getSession().setAttribute("oldProtocolBean",
                    protocolBeans.get(Integer.parseInt(request.getParameter("viewProtocolRowNumber")) - 1));
            response.sendRedirect("/PM_Presentation/private/addProtocol");
        }
        else {
            doGet(request, response);
        }
    }
}
