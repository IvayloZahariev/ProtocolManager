/**
 * 
 */
package com.izahariev.projects.pm.presentation.utilities;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.izahariev.projects.pm.beans.CommentBean;
import com.izahariev.projects.pm.beans.QuestionBean;
import com.izahariev.projects.pm.presentation.string_externalization.AddQuestionServletMessages;
import com.izahariev.projects.pm.presentation.validation.Validator;

/**
 * Class providing methods for comment creation, removal and editing.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings("PMD.UseUtilityClass")
public class CommentUtility {

    /**
     * Default constructor.
     */
    public CommentUtility() {

    }

    /**
     * Attempts to add a new comment to the commentBeansList. The parameters for
     * the creation of the comment are taken from the servlet request.
     * 
     * @param request
     *            HttpServletRequest of the servlet which uses the method.
     * @return True if the comment is successfully created and added to
     *         commentBeansList, otherwise false.
     */
    @SuppressWarnings({ "nls", "PMD.AvoidDuplicateLiterals" })
    public static boolean addComment(final HttpServletRequest request) {
        if (checkAddCommentFields(request)) {
            setCommenCounterAndBeanList(request);
            if (Validator.validateTextBoxNumber(request, "commentQuestionNumber", "commentQuestionNumberErrorMessage",
                    AddQuestionServletMessages.AddQuestionsServlet_EnterNumberMessage)) {
                // Returns true if bean is successfully created and added to
                // commentBeansList.
                return attpemtToSetAndAddBeanToBeanList(request);
            }
        }
        return false;
    }

    /**
     * Attempts to remove a comment from the commentBeansList. The number of the
     * comment which should be removed is taken from the servlet request.
     * 
     * @param request
     *            HttpServletRequest of the servlet which uses the method.
     */
    @SuppressWarnings({ "nls", "unchecked", "PMD.AvoidDuplicateLiterals" })
    public static void removeComment(final HttpServletRequest request) {
        if (Validator.validateTextBoxNotEmpty(request, "removeCommentNumber", "removeCommentErrorMessage",
                AddQuestionServletMessages.AddQuestionsServlet_EmptyFieldMessage)
                && Validator.validateTextBoxNumber(request, "removeCommentNumber", "removeCommentErrorMessage",
                        AddQuestionServletMessages.AddQuestionsServlet_EnterNumberMessage)) {
            final List<CommentBean> commentBeans = (List<CommentBean>) request.getSession()
                    .getAttribute("commentBeansList");
            CommentBean beanToBeRemoved = null;
            for (final CommentBean bean : commentBeans) {
                if (bean.getNumber() == Float.parseFloat(request.getParameter("removeCommentNumber"))) {
                    beanToBeRemoved = bean;
                }
            }
            if (beanToBeRemoved == null) {
                request.setAttribute("removeCommentErrorMessage",
                        AddQuestionServletMessages.AddQuestionsServlet_NoCommentWithThisNumberMessage);
            }
            else {
                commentBeans.remove(beanToBeRemoved);
                request.getSession().setAttribute("commentBeansList", commentBeans);
            }
        }
    }

    @SuppressWarnings({ "nls", "boxing", "PMD.AvoidDuplicateLiterals" })
    private static void setCommenCounterAndBeanList(final HttpServletRequest request) {
        if (request.getSession().getAttribute("commentCounter") == null) {
            request.getSession().setAttribute("commentCounter", 1);
        }
        if (request.getSession().getAttribute("commentBeansList") == null) {
            final List<CommentBean> questionBeans = new ArrayList<>();
            request.getSession().setAttribute("commentBeansList", questionBeans);
        }
    }

    @SuppressWarnings({ "boxing", "nls", "unchecked" })
    private static boolean attpemtToSetAndAddBeanToBeanList(final HttpServletRequest request) {
        final CommentBean commentBean = new CommentBean();
        final int counter = (int) request.getSession().getAttribute("commentCounter");
        commentBean.setNumber(counter);
        final List<QuestionBean> questionBeans = (List<QuestionBean>) request.getSession()
                .getAttribute("questionBeansList");
        if (questionBeans == null) {
            request.setAttribute("commentQuestionNumberErrorMessage",
                    AddQuestionServletMessages.AddQuestionsServlet_NoQuestionsMessage);
            return false;
        }
        boolean questionExists = false;
        for (final QuestionBean bean : questionBeans) {
            if (bean.getNumber() == Float.parseFloat(request.getParameter("commentQuestionNumber"))) {
                questionExists = true;
            }
        }
        if (questionExists) {
            commentBean.setQuestionNumber(Float.parseFloat(request.getParameter("commentQuestionNumber")));
            if ("default".equals(request.getParameter("selectedCommentOwner"))) {
                commentBean.setOwner(request.getParameter("otherCommentOwnerName"));
            }
            else {
                commentBean.setOwner(request.getParameter("selectedCommentOwner"));
            }
            commentBean.setSpecification(request.getParameter("commentSpecification"));
            final List<CommentBean> commentBeans = (List<CommentBean>) request.getSession()
                    .getAttribute("commentBeansList");
            commentBeans.add(commentBean);
            request.getSession().setAttribute("commentBeansList", commentBeans);
            request.getSession().setAttribute("commentCounter", counter + 1);
            return true;
        }
        request.setAttribute("commentQuestionNumberErrorMessage",
                AddQuestionServletMessages.AddQuestionsServlet_NoQuestionWithThisNumberMessage);
        return false;
    }

    @SuppressWarnings("nls")
    private static boolean checkAddCommentFields(final HttpServletRequest request) {
        final boolean validCommentQuestionNumber = Validator.validateTextBoxNotEmpty(request, "commentQuestionNumber",
                "commentQuestionNumberErrorMessage", AddQuestionServletMessages.AddQuestionsServlet_EmptyFieldMessage);
        final boolean validCommentOwner = Validator.validateDrpodownNotEmpty(request, "selectedCommentOwner",
                "commentOwnerErrorMessage",
                AddQuestionServletMessages.AddQuestionsServlet_SelectOrEnterOwnerNameMessage)
                || Validator.validateTextBoxNotEmpty(request, "otherCommentOwnerName", "otherCommentOwnerError",
                        AddQuestionServletMessages.AddQuestionsServlet_SelectOrEnterOwnerNameMessage);
        if (validCommentOwner) {
            request.removeAttribute("commentOwnerErrorMessage");
        }
        final boolean validCommentSpecification = Validator.validateTextBoxNotEmpty(request, "commentSpecification",
                "commentSpecificationErrorMessage", AddQuestionServletMessages.AddQuestionsServlet_EmptyFieldMessage);
        return validCommentOwner && validCommentQuestionNumber && validCommentSpecification;
    }
}
