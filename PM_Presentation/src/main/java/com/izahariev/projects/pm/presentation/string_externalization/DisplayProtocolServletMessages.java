package com.izahariev.projects.pm.presentation.string_externalization;

import org.eclipse.osgi.util.NLS;

/**
 * Class used for the string externalization in the DisplayProtocolServlet.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings("PMD.LongVariable")
public final class DisplayProtocolServletMessages extends NLS {
    private static final String BUNDLE_NAME = "com.izahariev.projects.pm.presentation.string_externalization.displayProtocolServletMessages"; //$NON-NLS-1$
    public static String DisplayProtocolsServlet_EmptyFieldMessage;
    public static String DisplayProtocolsServlet_EmptyTableMessage;
    public static String DisplayProtocolsServlet_EnterNumberMessage;
    public static String DisplayProtocolsServlet_NoSuchQuestionMessage;
    public static String DisplayProtocolsServlet_NoSuchRowMessage;
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, DisplayProtocolServletMessages.class);
    }

    private DisplayProtocolServletMessages() {
        super();
    }
}
