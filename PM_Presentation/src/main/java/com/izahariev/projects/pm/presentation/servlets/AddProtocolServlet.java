package com.izahariev.projects.pm.presentation.servlets;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.izahariev.projects.pm.beans.CommentBean;
import com.izahariev.projects.pm.beans.ProtocolBean;
import com.izahariev.projects.pm.beans.QuestionBean;
import com.izahariev.projects.pm.beans.VoteBean;
import com.izahariev.projects.pm.factories.services.ServicesFactory;
import com.izahariev.projects.pm.presentation.string_externalization.AddProtocolServletMessages;
import com.izahariev.projects.pm.presentation.validation.Validator;

/**
 * Servlet implementation class AddProtocolServlet
 */
@SuppressWarnings("PMD.GodClass")
public class AddProtocolServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     * 
     * @see HttpServlet#HttpServlet()
     */
    public AddProtocolServlet() {
        super();
    }

    /**
     * Forwards to AddProtocol.jsp.
     * 
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @SuppressWarnings({ "nls", "PMD.AvoidDuplicateLiterals" })
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        final HttpSession session = request.getSession();

        final List<String> allCouncilMembers = ServicesFactory.getAddProtocolServices().getAcademicCouncilMembers();
        if (session.getAttribute("councilMembers") == null) {
            session.setAttribute("councilMembers", allCouncilMembers);
            session.setAttribute("facultyList", ServicesFactory.getAddProtocolServices().getFaculties());
            session.setAttribute("teachingStuff",
                    ServicesFactory.getAddProtocolServices().getNonAcademicCouncilMembers());
        }

        if (session.getAttribute("oldProtocolBean") != null) {
            setFields(request, allCouncilMembers);
        }

        final RequestDispatcher RequetsDispatcherObj = request.getRequestDispatcher("/AddProtocol.jsp");

        RequetsDispatcherObj.forward(request, response);
    }

    /**
     * Determines which button was pressed in the jsp and executes the
     * corresponding action to it.
     * 
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @SuppressWarnings({ "nls", "boxing" })
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final String action = request.getParameter("action");

        switch (action) {
            case "Добави отсъстващ":
                addAbsentAction(request, response);
                break;
            case "Добави други отсъстващи":
                request.getSession().setAttribute("addOtherAbsent", true);
                setInputValues(request);
                doGet(request, response);
                break;
            case "Покажи катедри":
                getDepartments(request, response);
                break;
            case "Главно меню":
                final HttpSession session = request.getSession();
                session.invalidate();
                request.getSession(true).setAttribute("userIsLogged", true);
                response.sendRedirect("/PM_Presentation/private/home");
                break;
            default:
                addQuestionAction(request, response);
                break;
        }

    }

    @SuppressWarnings({ "nls", "unchecked", "PMD.AvoidDuplicateLiterals" })
    private void addAbsentAction(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final String otherAbsentNameString = "otherAbsentName";

        if (Validator.validateTextBoxNotEmpty(request, otherAbsentNameString, "addAbsentError",
                AddProtocolServletMessages.AddProtocolServlet_EmptyNameMessage)) {
            final HttpSession session = request.getSession();
            List<String> otherAbsentMembers = (List<String>) session.getAttribute(otherAbsentNameString);
            if (otherAbsentMembers == null) {
                otherAbsentMembers = new ArrayList<>();
            }
            otherAbsentMembers.add(request.getParameter(otherAbsentNameString));
            session.setAttribute("otherAbsentList", otherAbsentMembers);
        }
        setInputValues(request);
        doGet(request, response);
    }

    @SuppressWarnings({ "PMD.AvoidDuplicateLiterals", "nls", "unchecked" })
    private void addQuestionAction(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        setInputValues(request);
        if (validProtocolData(request)) {
            if (setProtocolBean(request)) {
                final List<String> presentCouncilMembers = (List<String>) request.getSession()
                        .getAttribute("councilMembers");
                final ProtocolBean protocolBean = (ProtocolBean) request.getSession().getAttribute("protocolBean");
                presentCouncilMembers.removeAll(protocolBean.getAbsentMembersNames());
                presentCouncilMembers.removeAll(protocolBean.getFurloughMembersNames());
                request.getSession().setAttribute("councilMembers", presentCouncilMembers);
                response.sendRedirect("/PM_Presentation/private/addQuestions");
            }
            else {
                doGet(request, response);
            }
        }
        else {
            doGet(request, response);
        }
    }

    @SuppressWarnings({ "nls", "boxing", "PMD.AvoidDuplicateLiterals" })
    private void getDepartments(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final String selectedFaculty = "selectedFaculty";
        if (Validator.validateDrpodownNotEmpty(request, selectedFaculty, "facultyErrorMessage",
                AddProtocolServletMessages.AddProtocolServlet_EmptyFacultyMessage)) {
            request.getSession().setAttribute("selectedFacultyOption", request.getParameter(selectedFaculty));
            request.getSession().removeAttribute("selectedDepartmentOption");
            request.getSession().setAttribute("departmentList",
                    ServicesFactory.getAddProtocolServices().getDepartments(request.getParameter(selectedFaculty)));
            request.getSession().setAttribute("isSetFaculty", true);
        }
        setInputValues(request);
        doGet(request, response);
    }

    @SuppressWarnings({ "nls", "PMD.AvoidDuplicateLiterals" })
    private static void setInputValues(final HttpServletRequest request) {
        request.getSession().setAttribute("checkedRadioButton", request.getParameter("protocolType"));
        request.getSession().setAttribute("protocolNumber", request.getParameter("protocolNumber"));
        request.getSession().setAttribute("selectedDepartmentOption", request.getParameter("selectedDepartment"));
        request.getSession().setAttribute("councilDate", request.getParameter("councilDate"));
        request.getSession().setAttribute("selectedReporterOption", request.getParameter("selectedReporter"));
        request.getSession().setAttribute("reporter", request.getParameter("reporter"));
        request.getSession().setAttribute("allMembers", request.getParameter("allMembers"));
        setAbsentInSession(request.getSession(), request);
        if (request.getSession().getAttribute("otherAbsentList") != null) {
            setOtherAbsentInSession(request.getSession(), request);
        }
    }

    @SuppressWarnings({ "nls", "unchecked", "PMD.AvoidDuplicateLiterals" })
    private static void setAbsentInSession(final HttpSession session, final HttpServletRequest request) {
        if (request.getParameterValues("absent") != null) {
            final boolean[] isAbsent = new boolean[((List<String>) session.getAttribute("councilMembers")).size()];
            final String[] absentCheckBox = request.getParameterValues("absent");
            for (int i = 0; i < absentCheckBox.length; i++) {
                isAbsent[Integer.parseInt(absentCheckBox[i])] = true;
            }
            session.setAttribute("checkedAbsent", isAbsent);
        }

        if (request.getParameterValues("furlough") != null) {
            final boolean[] isFurlough = new boolean[((List<String>) session.getAttribute("councilMembers")).size()];
            final String[] furloughCheckBox = request.getParameterValues("furlough");
            for (int i = 0; i < furloughCheckBox.length; i++) {
                isFurlough[Integer.parseInt(furloughCheckBox[i])] = true;
            }
            session.setAttribute("checkedFurlough", isFurlough);
        }
    }

    @SuppressWarnings({ "PMD.CollapsibleIfStatements", "nls" })
    private static void setAbsentFromBean(final HttpServletRequest request, final ProtocolBean protocolBean,
            final List<String> allCouncilMembers) {
        final boolean[] isAbsent = new boolean[protocolBean.getAllMembers()];
        final boolean[] isFurlough = new boolean[protocolBean.getAllMembers()];
        for (int i = 0; i < protocolBean.getAbsentMembersNames().size(); i++) {
            for (int j = 0; j < allCouncilMembers.size(); j++) {
                if (allCouncilMembers.get(j).equals(protocolBean.getAbsentMembersNames().get(i))) {
                    isAbsent[j] = true;
                }
            }
        }
        for (int m = 0; m < protocolBean.getFurloughMembersNames().size(); m++) {
            for (int n = 0; n < allCouncilMembers.size(); n++) {
                if (allCouncilMembers.get(n).equals(protocolBean.getFurloughMembersNames().get(m))) {
                    isFurlough[n] = true;
                }
            }
        }
        request.getSession().setAttribute("checkedAbsent", isAbsent);
        request.getSession().setAttribute("checkedFurlough", isFurlough);
    }

    @SuppressWarnings("nls")
    private static void setOtherAbsentFromBean(final HttpServletRequest request, final ProtocolBean protocolBean) {
        final List<String> otherAbsentMembers = protocolBean.getOtherAbsentMembersNames();
        final List<String> otherFurloughMembers = protocolBean.getOtherFurloughMembersNames();
        final List<String> allOtherAbsentMembers = new ArrayList<>();
        allOtherAbsentMembers.addAll(otherAbsentMembers);
        allOtherAbsentMembers.addAll(otherFurloughMembers);

        final boolean[] isAbsent = new boolean[allOtherAbsentMembers.size()];
        final boolean[] isFurlough = new boolean[allOtherAbsentMembers.size()];
        for (int i = 0; i < allOtherAbsentMembers.size(); i++) {
            if (otherAbsentMembers.contains(allOtherAbsentMembers.get(i))) {
                isAbsent[i] = true;
            }
            if (otherFurloughMembers.contains(allOtherAbsentMembers.get(i))) {
                isFurlough[i] = true;
            }
        }

        request.getSession().setAttribute("otherAbsentList", allOtherAbsentMembers);
        request.getSession().setAttribute("checkedOtherAbsent", isAbsent);
        request.getSession().setAttribute("checkedOtherFurlough", isFurlough);
    }

    @SuppressWarnings({ "nls", "unchecked", "PMD.AvoidDuplicateLiterals" })
    private static void setOtherAbsentInSession(final HttpSession session, final HttpServletRequest request) {
        if (request.getParameterValues("otherAbsent") != null) {
            final boolean[] isAbsent = new boolean[((List<String>) session.getAttribute("otherAbsentList")).size()];
            final String[] absentCheckBox = request.getParameterValues("otherAbsent");
            for (int i = 0; i < absentCheckBox.length; i++) {
                isAbsent[Integer.parseInt(absentCheckBox[i])] = true;
            }
            session.setAttribute("checkedOtherAbsent", isAbsent);
        }

        if (request.getParameterValues("otherFurlough") != null) {
            final boolean[] isFurlough = new boolean[((List<String>) session.getAttribute("otherAbsentList")).size()];
            final String[] furloughCheckBox = request.getParameterValues("otherFurlough");
            for (int i = 0; i < furloughCheckBox.length; i++) {
                isFurlough[Integer.parseInt(furloughCheckBox[i])] = true;
            }
            session.setAttribute("checkedOtherFurlough", isFurlough);
        }
    }

    @SuppressWarnings("nls")
    private static boolean validProtocolData(final HttpServletRequest request) {
        final boolean validProtocolType = Validator.validatePressedRadioButton(request, "protocolType",
                "protocolTypeErrorMessage", AddProtocolServletMessages.AddProtocolServlet_NoProtocolTypeMessage);
        final boolean validProtocolNumber = Validator.validateTextBoxNotEmpty(request, "protocolNumber",
                "protocolNumberErrorMessage",
                AddProtocolServletMessages.AddProtocolServlet_InvalidProtocolNumberMessage)
                && Validator.validateTextBoxNumber(request, "protocolNumber", "protocolNumberErrorMessage",
                        AddProtocolServletMessages.AddProtocolServlet_InvalidProtocolNumberMessage);
        final boolean validFaculty = Validator.validateDrpodownNotEmpty(request, "selectedFaculty",
                "facultyErrorMessage", AddProtocolServletMessages.AddProtocolServlet_EmptyFacultyMessage);
        final boolean validDepartment = Validator.validateDrpodownNotEmpty(request, "selectedDepartment",
                "departmentErrorMessage", AddProtocolServletMessages.AddProtocolServlet_EmptyDepartmentMessage);
        final boolean validCouncilDate = Validator.validateTextBoxNotEmpty(request, "councilDate", "dateErrorMessage",
                AddProtocolServletMessages.AddProtocolServlet_NoProtocolDateMessage);
        final boolean validReporter = Validator.validateDrpodownNotEmpty(request, "selectedReporter",
                "reporterErrorMessage", AddProtocolServletMessages.AddProtocolServlet_NoReporterMessage);
        final boolean validAllMembers = Validator.validateTextBoxNotEmpty(request, "allMembers",
                "membersNumberErrorMessage", AddProtocolServletMessages.AddProtocolServlet_InvalidMembersNumberMessage)
                && Validator.validateTextBoxNumber(request, "allMembers", "membersNumberErrorMessage",
                        AddProtocolServletMessages.AddProtocolServlet_InvalidMembersNumberMessage);

        return validProtocolType & validProtocolNumber & validFaculty & validDepartment & validCouncilDate
                & validReporter & validAllMembers;
    }

    @SuppressWarnings("nls")
    private static boolean setProtocolBean(final HttpServletRequest request) {
        final ProtocolBean bean = new ProtocolBean();
        if (ServicesFactory.getAddProtocolServices().checkFaculty(request.getParameter("selectedFaculty"),
                request.getParameter("selectedDepartment"))) {
            bean.setFaculty(request.getParameter("selectedFaculty"));
            bean.setDepartment(request.getParameter("selectedDepartment"));
            bean.setDepartmentDirector(ServicesFactory.getAddProtocolServices()
                    .getDepartmentDirectorName(request.getParameter("selectedDepartment")));
            bean.setNumber(Integer.parseInt(request.getParameter("protocolNumber")));
            bean.setCouncilDate(Date.valueOf(request.getParameter("councilDate")));
            bean.setReporter(request.getParameter("selectedReporter"));
            bean.setType(request.getParameter("protocolType"));
            bean.setAllMembers(Integer.parseInt(request.getParameter("allMembers")));
            final int reducedMembers = Integer.parseInt(request.getParameter("allMembers"))
                    - getFurloughNames(request).size() - getOtherFurloughNames(request).size();
            bean.setReducedMembers(reducedMembers);
            bean.setQuorum(reducedMembers * 2 / 3);// TODO: Round?
            bean.setPresentMembers(reducedMembers - getAbsentNames(request).size());
            bean.setAbsent(getAbsentNames(request).size() + getOtherAbsentNames(request).size());
            final List<String> absentMembersNames = getAbsentNames(request);
            final List<String> furloughMembersNames = getFurloughNames(request);
            absentMembersNames.removeAll(furloughMembersNames);
            bean.setAbsentMembersNames(absentMembersNames);
            bean.setFurlough(getFurloughNames(request).size() + getOtherFurloughNames(request).size());
            bean.setFurloughMembersNames(furloughMembersNames);
            final List<String> otherAbsentMembersNames = getOtherAbsentNames(request);
            final List<String> otherFurloughMembersNames = getOtherFurloughNames(request);
            otherAbsentMembersNames.removeAll(otherFurloughMembersNames);
            bean.setOtherAbsentMembersNames(otherAbsentMembersNames);
            bean.setOtherFurloughMembersNames(otherFurloughMembersNames);
            request.getSession().setAttribute("protocolBean", bean);
            return true;
        }
        request.setAttribute("addProtocolError",
                AddProtocolServletMessages.AddProtocolServlet_DepartmentFacultyMissmatchMessage);
        return false;
    }

    @SuppressWarnings({ "nls", "unchecked" })
    private static List<String> getAbsentNames(final HttpServletRequest request) {
        final List<String> allMembersNames = (List<String>) request.getSession().getAttribute("councilMembers");
        final List<String> absentMembersNames = new ArrayList<>();
        if (request.getParameterValues("absent") != null) {
            for (final String index : request.getParameterValues("absent")) {
                absentMembersNames.add(allMembersNames.get(Integer.parseInt(index)));
            }
        }

        return absentMembersNames;
    }

    @SuppressWarnings({ "nls", "unchecked" })
    private static List<String> getOtherAbsentNames(final HttpServletRequest request) {
        final List<String> otherAbsentMembers = (List<String>) request.getSession().getAttribute("otherAbsentList");
        final List<String> otherAbsentMembersNames = new ArrayList<>();
        if (request.getParameterValues("otherAbsent") != null) {
            for (final String index : request.getParameterValues("otherAbsent")) {
                otherAbsentMembersNames.add(otherAbsentMembers.get(Integer.parseInt(index)));
            }
        }

        return otherAbsentMembersNames;
    }

    @SuppressWarnings({ "nls", "unchecked" })
    private static List<String> getFurloughNames(final HttpServletRequest request) {
        final List<String> allMembersNames = (List<String>) request.getSession().getAttribute("councilMembers");
        final List<String> furloughtMembersNames = new ArrayList<>();
        if (request.getParameterValues("furlough") != null) {
            for (final String index : request.getParameterValues("furlough")) {
                furloughtMembersNames.add(allMembersNames.get(Integer.parseInt(index)));
            }
        }

        return furloughtMembersNames;
    }

    @SuppressWarnings({ "unchecked", "nls" })
    private static List<String> getOtherFurloughNames(final HttpServletRequest request) {
        final List<String> otherAbsentMembers = (List<String>) request.getSession().getAttribute("otherAbsentList");
        final List<String> otherFurloughtMembersNames = new ArrayList<>();
        if (request.getParameterValues("otherFurlough") != null) {
            for (final String index : request.getParameterValues("otherFurlough")) {
                otherFurloughtMembersNames.add(otherAbsentMembers.get(Integer.parseInt(index)));
            }
        }

        return otherFurloughtMembersNames;
    }

    @SuppressWarnings({ "nls", "boxing" })
    private static void setFields(final HttpServletRequest request, final List<String> allCouncilMembers) {
        // TODO: Use bean in jsp.
        final ProtocolBean protocolBean = (ProtocolBean) request.getSession().getAttribute("oldProtocolBean");
        request.setAttribute("checkedRadioButton", protocolBean.getType());
        request.setAttribute("protocolNumber", protocolBean.getNumber());
        request.setAttribute("selectedFacultyOption", protocolBean.getFaculty());
        request.getSession().setAttribute("isSetFaculty", true);
        request.setAttribute("selectedDepartmentOption", protocolBean.getDepartment());
        request.setAttribute("councilDate", protocolBean.getCouncilDateForDateField());
        request.setAttribute("selectedReporterOption", protocolBean.getReporter());
        request.setAttribute("allMembers", protocolBean.getAllMembers());
        setAbsentFromBean(request, protocolBean, allCouncilMembers);
        request.getSession().setAttribute("addOtherAbsent", true);
        setOtherAbsentFromBean(request, protocolBean);
        request.getSession().setAttribute("questionBeansList", protocolBean.getQuestions());
        final List<CommentBean> commentBeans = new ArrayList<>();
        final List<VoteBean> voteBeans = new ArrayList<>();
        for (final QuestionBean questionBean : protocolBean.getQuestions()) {
            commentBeans.addAll(questionBean.getComments());
            voteBeans.addAll(questionBean.getVotes());
        }
        for (int i = 0; i < commentBeans.size(); i++) {
            commentBeans.get(i).setNumber(i + 1);
        }
        for (int j = 0; j < voteBeans.size(); j++) {
            voteBeans.get(j).setVoteNumber(j + 1);
        }
        request.getSession().setAttribute("commentBeansList", commentBeans);
        request.getSession().setAttribute("voteBeansList", voteBeans);
    }
}
