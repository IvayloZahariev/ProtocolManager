/**
 * 
 */
package com.izahariev.projects.pm.presentation.utilities;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.izahariev.projects.pm.beans.ProtocolBean;
import com.izahariev.projects.pm.beans.QuestionBean;
import com.izahariev.projects.pm.beans.VoteBean;
import com.izahariev.projects.pm.presentation.string_externalization.AddQuestionServletMessages;
import com.izahariev.projects.pm.presentation.validation.Validator;

/**
 * Class providing methods for vote creation, removal and editing.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings("PMD.UseUtilityClass")
public class VoteUtility {

    /**
     * Default constructor.
     */
    public VoteUtility() {

    }

    /**
     * Attempts to add a vote to voteBeansList. The parameters for the creation
     * of the vote are taken from the servlet request.
     * 
     * @param request
     * @return
     */
    @SuppressWarnings({ "unchecked", "nls" })
    public static boolean addVote(final HttpServletRequest request) {
        if (chackAddVoteFields(request)) {
            final List<QuestionBean> questionBeans = (List<QuestionBean>) request.getSession()
                    .getAttribute("questionBeansList");
            if (questionBeans == null) {
                request.setAttribute("addVoteQuestionNumberErrorMessage",
                        AddQuestionServletMessages.AddQuestionsServlet_NoQuestionsMessage);
            }
            else {
                boolean questionExists = false;
                for (final QuestionBean bean : questionBeans) {
                    if (bean.getNumber() == Float.parseFloat(request.getParameter("voteQuestionNumber"))) {
                        questionExists = true;
                    }
                }
                if (questionExists) {
                    if (checkVoteNumberFields(request)) {
                        addVoteToVoteBeansList(request);
                        return true;
                    }
                }
                else {
                    request.setAttribute("addVoteQuestionNumberErrorMessage",
                            AddQuestionServletMessages.AddQuestionsServlet_NoQuestionWithThisNumberMessage);
                }
            }
        }
        return false;
    }

    /**
     * Attempts to remove a vote from the voteBeansList. The number of the vote
     * which should be removed is taken from the servlet request.
     * 
     * @param request
     *            HttpServletRequest of the servlet which uses the method.
     */
    @SuppressWarnings({ "nls", "unchecked", "PMD.AvoidDuplicateLiterals" })
    public static void removeVote(final HttpServletRequest request) {
        final boolean validRemoveVoteNumber = Validator.validateTextBoxNotEmpty(request, "removeVoteNumber",
                "removeVoteNumberError", AddQuestionServletMessages.AddQuestionsServlet_EmptyFieldMessage);
        if (validRemoveVoteNumber && Validator.validateTextBoxNumber(request, "removeVoteNumber",
                "removeVoteNumberError", AddQuestionServletMessages.AddQuestionsServlet_EnterNumberMessage)) {
            final List<VoteBean> voteBeans = (List<VoteBean>) request.getSession().getAttribute("voteBeansList");
            VoteBean beanToBeRemoved = null;
            for (final VoteBean bean : voteBeans) {
                if (bean.getVoteNumber() == Float.parseFloat(request.getParameter("removeVoteNumber"))) {
                    beanToBeRemoved = bean;
                }
            }
            if (beanToBeRemoved == null) {
                request.setAttribute("removeVoteNumberError",
                        AddQuestionServletMessages.AddQuestionsServlet_NoVoteWithThisNumberMessage);
            }
            else {
                voteBeans.remove(beanToBeRemoved);
                request.getSession().setAttribute("voteBeansList", voteBeans);
            }
        }
    }

    @SuppressWarnings("nls")
    private static boolean chackAddVoteFields(final HttpServletRequest request) {
        final boolean validQuestionNumber = Validator.validateTextBoxNotEmpty(request, "voteQuestionNumber",
                "addVoteQuestionNumberErrorMessage", AddQuestionServletMessages.AddQuestionsServlet_EmptyFieldMessage);
        final boolean validVoteSubject = Validator.validateTextBoxNotEmpty(request, "voteSubject",
                "voteSubjectErrorMessage", AddQuestionServletMessages.AddQuestionsServlet_EmptyFieldMessage);
        final boolean validVoteType = Validator.validatePressedRadioButton(request, "voteType", "voteTypeErrorMessage",
                AddQuestionServletMessages.AddQuestionsServlet_SelectVoteTypeMessage);
        final boolean validYesVotes = Validator.validateTextBoxNumber(request, "yesVotes", "yesVotesErrorMessage",
                AddQuestionServletMessages.AddQuestionsServlet_EnterNumberMessage);
        final boolean validNoVotes = Validator.validateTextBoxNumber(request, "noVotes", "noVotesErrorMessage",
                AddQuestionServletMessages.AddQuestionsServlet_EnterNumberMessage);
        final boolean validBlankVotes = Validator.validateTextBoxNumber(request, "blankVotes", "blankVotesErrorMessage",
                AddQuestionServletMessages.AddQuestionsServlet_EnterNumberMessage);
        final boolean validVoteModerator = Validator.validateDrpodownNotEmpty(request, "selectedVoteModerator",
                "otherModeratorError", AddQuestionServletMessages.AddQuestionsServlet_SelectModeratorMessage);
        final boolean validVoteMember1 = Validator.validateDrpodownNotEmpty(request, "selectedVoteMember1",
                "otherMember1Error", AddQuestionServletMessages.AddQuestionsServlet_SelectMember1Message);
        final boolean validVoteMember2 = Validator.validateDrpodownNotEmpty(request, "selectedVoteMember2",
                "otherMember2Error", AddQuestionServletMessages.AddQuestionsServlet_SelectMember2Message);
        if (validVoteModerator) {
            request.removeAttribute("otherModeratorError");
        }
        if (validVoteMember1) {
            request.removeAttribute("otherMember1Error");
        }
        if (validVoteMember2) {
            request.removeAttribute("otherMember2Error");
        }
        boolean validVotesNumber = true;
        if (validYesVotes && validNoVotes && validBlankVotes) {
            if (Integer.parseInt(request.getParameter("yesVotes")) + Integer.parseInt(request.getParameter("noVotes"))
                    + Integer.parseInt(request.getParameter(
                            "blankVotes")) != ((ProtocolBean) request.getSession().getAttribute("protocolBean"))
                                    .getPresentMembers()) {
                validVotesNumber = false;
                request.setAttribute("blankVotesErrorMessage",
                        AddQuestionServletMessages.AddQuestionsServlet_WrongVotesNumberMessage);
            }
        }
        return validQuestionNumber && validVoteSubject && validVoteType && validYesVotes && validNoVotes
                && validBlankVotes && validVoteModerator && validVoteMember1 && validVoteMember2 && validVotesNumber;
    }

    @SuppressWarnings("nls")
    private static boolean checkVoteNumberFields(final HttpServletRequest request) {
        final boolean positiveYesVotes = Validator.validatePositiveNumber(request, "yesVotes", "yesVotesErrorMessage",
                AddQuestionServletMessages.AddQuestionsServlet_EnterPositiveNumberMessage);
        final boolean positiveNoVotes = Validator.validatePositiveNumber(request, "noVotes", "noVotesErrorMessage",
                AddQuestionServletMessages.AddQuestionsServlet_EnterPositiveNumberMessage);
        final boolean positiveBlankVotes = Validator.validatePositiveNumber(request, "blankVotes",
                "blankVotesErrorMessage", AddQuestionServletMessages.AddQuestionsServlet_EnterPositiveNumberMessage);
        return positiveYesVotes && positiveNoVotes && positiveBlankVotes;
    }

    @SuppressWarnings({ "nls", "unchecked", "boxing", "PMD.AvoidDuplicateLiterals" })
    private static void addVoteToVoteBeansList(final HttpServletRequest request) {
        setCommenCounterAndBeanList(request);
        List<VoteBean> voteBeans = (List<VoteBean>) request.getSession().getAttribute("voteBeansList");
        if (voteBeans == null) {
            voteBeans = new ArrayList<>();
        }
        final VoteBean bean = new VoteBean();
        final int voteNumber = (int) request.getSession().getAttribute("voteCounter");
        bean.setVoteNumber(voteNumber);
        request.getSession().setAttribute("voteCounter", voteNumber + 1);
        bean.setQuestionNumber(Float.parseFloat(request.getParameter("voteQuestionNumber")));
        bean.setVoteSubject(request.getParameter("voteSubject"));
        bean.setVoteType(request.getParameter("voteType"));
        bean.setNumYesVotes(Integer.parseInt(request.getParameter("yesVotes")));
        bean.setNumNoVotes(Integer.parseInt(request.getParameter("noVotes")));
        bean.setNumBlankVotes(Integer.parseInt(request.getParameter("blankVotes")));
        if ("default".equals(request.getParameter("selectedVoteModerator"))) {
            bean.setModerator(request.getParameter("otherModeratorName"));
        }
        else {
            bean.setModerator(request.getParameter("selectedVoteModerator"));
        }
        if ("default".equals(request.getParameter("selectedVoteMember1"))) {
            bean.setMember1(request.getParameter("otherMember1Name"));
        }
        else {
            bean.setMember1(request.getParameter("selectedVoteMember1"));
        }
        if ("default".equals(request.getParameter("selectedVoteMember2"))) {
            bean.setMember2(request.getParameter("otherMember2Name"));
        }
        else {
            bean.setMember2(request.getParameter("selectedVoteMember2"));
        }
        voteBeans.add(bean);
        request.getSession().setAttribute("voteBeansList", voteBeans);
    }

    @SuppressWarnings({ "nls", "boxing" })
    private static void setCommenCounterAndBeanList(final HttpServletRequest request) {
        if (request.getSession().getAttribute("voteCounter") == null) {
            request.getSession().setAttribute("voteCounter", 1);
        }
        if (request.getSession().getAttribute("voteBeansList") == null) {
            final List<VoteBean> questionBeans = new ArrayList<>();
            request.getSession().setAttribute("voteBeansList", questionBeans);
        }
    }
}
