package com.izahariev.projects.pm.presentation.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.izahariev.projects.pm.factories.services.ServicesFactory;
import com.izahariev.projects.pm.presentation.string_externalization.LoginPageServletMessages;
import com.izahariev.projects.pm.services.interfaces.LoginServiceInterface;

/**
 * Servlet used to login into the Protocol Manager application.
 */
public class LoginPageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     * 
     * @see HttpServlet#HttpServlet()
     */
    public LoginPageServlet() {
        super();
    }

    /**
     * Forwards to the Login.jsp.
     * 
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        @SuppressWarnings("nls")
        final RequestDispatcher RequetsDispatcherObj = request.getRequestDispatcher("/Login.jsp");
        RequetsDispatcherObj.forward(request, response);
    }

    /**
     * Used for login into the system.
     * 
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @SuppressWarnings({ "nls", "boxing" })
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final LoginServiceInterface loginService = ServicesFactory.getLoginServices();
        final boolean result = loginService.checkUser(request.getParameter("username"),
                request.getParameter("password"));
        if (result) {
            request.getSession().setAttribute("userIsLogged", true);
            response.sendRedirect("/PM_Presentation/private/home");
        }
        else {
            request.setAttribute("errorMessage", LoginPageServletMessages.LoginPageServlet_WrongLoginDataMessage);
            doGet(request, response);
        }
    }

}
