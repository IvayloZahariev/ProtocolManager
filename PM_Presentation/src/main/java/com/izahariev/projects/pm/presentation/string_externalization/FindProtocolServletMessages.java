package com.izahariev.projects.pm.presentation.string_externalization;

import org.eclipse.osgi.util.NLS;

/**
 * Class used for the string externalization in the FindProtocolServlet.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings("PMD.LongVariable")
public final class FindProtocolServletMessages extends NLS {
    private static final String BUNDLE_NAME = "com.izahariev.projects.pm.presentation.string_externalization.findProtocolServletMessages"; //$NON-NLS-1$
    public static String FindProtocolServlet_EnterNumberMessage;
    public static String FindProtocolServlet_EnterPositiveNumberMessage;
    public static String FindProtocolServlet_NoProtocolsMessage;
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, FindProtocolServletMessages.class);
    }

    private FindProtocolServletMessages() {
        super();
    }
}
