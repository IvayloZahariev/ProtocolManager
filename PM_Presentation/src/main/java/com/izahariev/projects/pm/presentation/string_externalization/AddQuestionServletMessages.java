package com.izahariev.projects.pm.presentation.string_externalization;

import org.eclipse.osgi.util.NLS;

/**
 * Class used for the string externalization of the AddQuestionServlet.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings("PMD.LongVariable")
public final class AddQuestionServletMessages extends NLS {
    private static final String BUNDLE_NAME = "com.izahariev.projects.pm.presentation.string_externalization.addQuestionServletMessages"; //$NON-NLS-1$
    public static String AddQuestionsServlet_EmptyFieldMessage;
    public static String AddQuestionsServlet_SelectQuestionTypeMessage;
    public static String AddQuestionsServlet_EnterNumberMessage;
    public static String AddQuestionsServlet_NoQuestionsMessage;
    public static String AddQuestionsServlet_NoQuestionWithThisNumberMessage;
    public static String AddQuestionsServlet_SelectOrEnterOwnerNameMessage;
    public static String AddQuestionsServlet_NoCommentWithThisNumberMessage;
    public static String AddQuestionsServlet_SelectVoteTypeMessage;
    public static String AddQuestionsServlet_SelectModeratorMessage;
    public static String AddQuestionsServlet_SelectMember1Message;
    public static String AddQuestionsServlet_SelectMember2Message;
    public static String AddQuestionsServlet_EnterPositiveNumberMessage;
    public static String AddQuestionsServlet_NoVoteWithThisNumberMessage;
    public static String AddQuestionsServlet_NoQuestionsAddedMessage;
    public static String AddQuestionsServlet_WrongVotesNumberMessage;
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, AddQuestionServletMessages.class);
    }

    private AddQuestionServletMessages() {
        super();
    }
}
