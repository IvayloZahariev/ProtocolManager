/**
 * 
 */
package com.izahariev.projects.pm.presentation.utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.izahariev.projects.pm.beans.QuestionBean;
import com.izahariev.projects.pm.presentation.string_externalization.AddQuestionServletMessages;
import com.izahariev.projects.pm.presentation.validation.Validator;

/**
 * Class providing methods for question creation, removal and editing.
 * 
 * @author Ivaylo Zahariev
 *
 */
@SuppressWarnings("PMD.UseUtilityClass")
public class QuestionUtility {

    /**
     * Default constructor.
     */
    public QuestionUtility() {

    }

    /**
     * Attempts to add a new question to the questionBeansList. The parameters
     * for the creation of the question are taken from the servlet request.
     * 
     * @param request
     *            HttpServletRequest of the servlet which uses the method.
     * @param response
     *            HttpServletResponse of the servlet which uses the method.
     * @return True if the question was successfully added to the
     *         questionBeansList, otherwise false.
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings({ "nls", "PMD.AvoidDuplicateLiterals", "unchecked" })
    public static boolean addQuestion(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        if (checkAddQuestionFields(request)) {
            if (request.getSession().getAttribute("questionBeansList") == null) {
                final List<QuestionBean> questionBeans = new ArrayList<>();
                request.getSession().setAttribute("questionBeansList", questionBeans);
            }
            final QuestionBean questionBean = new QuestionBean();
            questionBean.setNumber(Float.parseFloat(request.getParameter("questionNumber")));
            questionBean.setType(request.getParameter("selectedQuestionType"));
            questionBean.setSpecification(request.getParameter("questionSpecification"));
            questionBean.setDecision(request.getParameter("questionDecision"));
            final List<QuestionBean> questionBeans = (List<QuestionBean>) request.getSession()
                    .getAttribute("questionBeansList");
            questionBeans.add(questionBean);
            request.getSession().setAttribute("questionBeansList", questionBeans);

            return true;
        }
        return false;
    }

    /**
     * Attempts to remove a question from the questionBeansList. The number of
     * the question which should be removed is taken from the servlet request.
     * 
     * @param request
     *            HttpServletRequest of the servlet which uses the method.
     */
    @SuppressWarnings({ "nls", "unchecked", "PMD.AvoidDuplicateLiterals" })
    public static void removeQuestion(final HttpServletRequest request) {
        if (Validator.validateTextBoxNotEmpty(request, "removeQuestionNumber", "removeQuestionErrorMessage",
                AddQuestionServletMessages.AddQuestionsServlet_EmptyFieldMessage)
                && Validator.validateTextBoxNumber(request, "removeQuestionNumber", "removeQuestionErrorMessage",
                        AddQuestionServletMessages.AddQuestionsServlet_EnterNumberMessage)) {
            final List<QuestionBean> questionBeans = (List<QuestionBean>) request.getSession()
                    .getAttribute("questionBeansList");
            if (questionBeans == null) {
                request.setAttribute("removeQuestionErrorMessage",
                        AddQuestionServletMessages.AddQuestionsServlet_NoQuestionsMessage);
            }
            else {
                QuestionBean beanToBeRemoved = null;
                for (final QuestionBean bean : questionBeans) {
                    if (bean.getNumber() == Float.parseFloat(request.getParameter("removeQuestionNumber"))) {
                        beanToBeRemoved = bean;
                    }
                }
                if (beanToBeRemoved == null) {
                    request.setAttribute("removeQuestionErrorMessage",
                            AddQuestionServletMessages.AddQuestionsServlet_NoQuestionWithThisNumberMessage);
                }
                else {
                    questionBeans.remove(beanToBeRemoved);
                    request.getSession().setAttribute("questionBeansList", questionBeans);
                }
            }
        }
    }

    @SuppressWarnings("nls")
    private static boolean checkAddQuestionFields(final HttpServletRequest request) {
        final boolean validQuestionType = Validator.validateDrpodownNotEmpty(request, "selectedQuestionType",
                "questionTypeErrorMessage", AddQuestionServletMessages.AddQuestionsServlet_SelectQuestionTypeMessage);
        final boolean validQuestionNumber = Validator.validateTextBoxNotEmpty(request, "questionNumber",
                "questionNumberErrorMessage", AddQuestionServletMessages.AddQuestionsServlet_EmptyFieldMessage)
                && Validator.validateTextBoxNumber(request, "questionNumber", "questionNumberErrorMessage",
                        AddQuestionServletMessages.AddQuestionsServlet_EnterNumberMessage);
        final boolean validQuestionSpecification = Validator.validateTextBoxNotEmpty(request, "questionSpecification",
                "questionSpecificationErrorMessage", AddQuestionServletMessages.AddQuestionsServlet_EmptyFieldMessage);
        final boolean validQuestionDecision = Validator.validateTextBoxNotEmpty(request, "questionDecision",
                "questionDecisionErrorMessage", AddQuestionServletMessages.AddQuestionsServlet_EmptyFieldMessage);
        return validQuestionType && validQuestionNumber && validQuestionSpecification && validQuestionDecision;
    }
}
