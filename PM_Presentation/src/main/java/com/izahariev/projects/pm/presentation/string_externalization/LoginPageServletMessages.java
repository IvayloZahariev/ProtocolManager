package com.izahariev.projects.pm.presentation.string_externalization;

import org.eclipse.osgi.util.NLS;

/**
 * Class used for the string externalization of the LoginPageServlet.
 * 
 * @author Ivaylo Zahariev
 *
 */
public final class LoginPageServletMessages extends NLS {
    private static final String BUNDLE_NAME = "com.izahariev.projects.pm.presentation.string_externalization.loginPageServletMessages"; //$NON-NLS-1$
    @SuppressWarnings("PMD.LongVariable")
    public static String LoginPageServlet_WrongLoginDataMessage;
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, LoginPageServletMessages.class);
    }

    private LoginPageServletMessages() {
        super();
    }
}
